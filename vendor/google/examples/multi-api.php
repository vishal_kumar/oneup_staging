<?php
/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

include_once __DIR__ . '/../vendor/autoload.php';
include_once __DIR__ .'/../vendor/google/apiclient-services/src/Google/Service/Google_Service_PlusPages.php';
include_once "templates/base.php";

echo pageHeader("User Query - Multiple APIs");


/*************************************************
 * Ensure you've downloaded your oauth credentials
 ************************************************/
if (!$oauth_credentials = getOAuthCredentialsFile()) {
  echo missingOAuth2CredentialsWarning();
  return;
}

/************************************************
 * The redirect URI is to the current page, e.g:
 * http://localhost:8080/multi-api.php
 ************************************************/
$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

$client = new Google_Client();
$client->setAuthConfig($oauth_credentials);
$client->setRedirectUri($redirect_uri);
$client->setAccessType("offline");
$client->addScope("https://www.googleapis.com/auth/plus.pages.manage");
$client->addScope("https://www.googleapis.com/auth/plus.stream.read");
$client->addScope("https://www.googleapis.com/auth/plus.stream.write");
$client->addScope("https://www.googleapis.com/auth/plus.circles.read");
$client->addScope("https://www.googleapis.com/auth/plus.circles.write");
$client->addScope("https://www.googleapis.com/auth/plus.media.readwrite");
$client->addScope("https://www.googleapis.com/auth/plus.me");
$client->addScope("https://www.googleapis.com/auth/plus.profiles.write");
$client->addScope("https://www.googleapis.com/auth/plus.collections.readonly");


// add "?logout" to the URL to remove a token from the session
if (isset($_REQUEST['logout'])) {
  unset($_SESSION['multi-api-token']);
}

/************************************************
 * If we have a code back from the OAuth 2.0 flow,
 * we need to exchange that with the
 * Google_Client::fetchAccessTokenWithAuthCode()
 * function. We store the resultant access token
 * bundle in the session, and redirect to ourself.
 ************************************************/
if (isset($_GET['code'])) {
  $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  $client->setAccessToken($token);
  $_SESSION['multi-api-token'] = $token;
  $plus_service = new Google_Service_PlusPages($client);
  print_r($_SESSION['multi-api-token']);
  var_dump($plus_service->people->listPeople("me", "pages"));

  
  // redirect back to the example
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}

$authUrl = $client->createAuthUrl();



?>

<div class="box">
  <div class="request">
  <a class="login" href="<?= $authUrl ?>">Connect Me!</a>
  </div>
</div>

<?= pageFooter(__FILE__) ?>

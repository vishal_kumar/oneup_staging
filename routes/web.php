<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@showLanding');


Auth::routes();

Route::get('/loginsuccess', 'HomeController@loginSuccess')->middleware('auth');;

Route::get('/firsttime', 'HomeController@firstTime');
Route::get('/quicktour', 'HomeController@firstTime');
Route::get('/category', 'CategoryController@showCategory')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/addcontent', 'HomeController@addContent');
Route::any('/signature', 'SettingsController@getSignature');

//Admin
Route::get('/s', 'HomeController@switchAccount');

Route::post('/getscheduledpostbyid', 'PostController@getScheduledPostById');

//Category Routes
Route::post('/getcategory', 'CategoryController@getCategory');
Route::post('/getcategoryandaccounts', 'CategoryController@getCategoryAndAccounts');
Route::post('/getinstagramaccounts', 'CategoryController@getInstagramAccounts');
Route::post('/getpostscount', 'CategoryController@getPostsCount');
Route::post('/updatecategoryname', 'CategoryController@updateCategoryName');
Route::post('/updatecategorycolor', 'CategoryController@updateCategoryColor');
Route::post('/removesocialaccountcategory', 'CategoryController@removeSocialAccount');
Route::post('/addsocialaccountcategory', 'CategoryController@addSocialAccount');
Route::post('/deletecategory', 'CategoryController@deleteCategory');
Route::post('/addcategory', 'CategoryController@addCategory');

//Add Content Routes
Route::get('/post/addsinglepost', 'PostController@addSinglePost')->middleware('auth');
Route::get('/post/addinstagramstory', 'PostController@addInstagramStory')->middleware('auth');
Route::get('/post/addbulkpost', 'PostController@addBulkPost')->middleware('auth');
Route::get('/post/getrss', 'PostController@getRSS')->middleware('auth');
Route::post('/post/getrssfeed', 'PostController@getRssFeed');
Route::post('/post/imagefromextension', 'PostController@getImageFromExtension')->middleware('auth');;
Route::post('/post/getimagefromdiscover', 'PostController@getImageFromDiscover')->middleware('auth');;
Route::post('/post/processcsv', 'PostController@processCsv');
Route::post('/post/postnow', 'PostController@postNow');
Route::post('/post/schedulepost', 'PostController@schedulePost');
Route::post('/post/addpost', 'PostController@addPostToQueue');
Route::post('/refreshqueue', 'PostController@refreshQueue');


//Schedule Routes
Route::get('/schedule', 'ScheduleController@scheduleView')->middleware('auth');
Route::post('/getschedule', 'ScheduleController@getSchedule');
Route::post('/createnewschedule', 'ScheduleController@createNewSchedule');
Route::post('/editschedule', 'ScheduleController@editSchedule');
Route::post('/deleteschedule', 'ScheduleController@deleteSchedule');

//Queue
Route::get('/queue', 'QueueController@queueView')->middleware('auth');
Route::get('/calendarview', 'QueueController@calendarView')->middleware('auth');
Route::post('/getcount', 'QueueController@getPostCounts');
Route::post('/getqueuecategory', 'QueueController@getCategoryAndAccounts');
Route::post('/getposts', 'QueueController@getPosts');
Route::post('/getpaginatedposts', 'QueueController@getPaginatedPosts');
Route::post('/publishedposts', 'QueueController@getPublishedPosts')->middleware('auth');
Route::post('/getpublishedpostsforanalytics', 'QueueController@getPublishedPostsForAnalytics');
Route::post('/failedposts', 'QueueController@getFailedPosts');
Route::post('/deletepost', 'QueueController@deletePost');
Route::post('deletefailedpost','QueueController@deleteFailedPost');
Route::post('/editpost', 'QueueController@editPost');
Route::post('/bulkdeletepost', 'QueueController@bulkDeletePost');
Route::post('/bulkdeletefailedpost', 'QueueController@bulkDeleteFailedPost');
Route::post('/getevents', 'QueueController@getEvents');
Route::post('/updateposttime', 'QueueController@updatePostCalendar');
Route::post('/cancelrecycle', 'QueueController@cancelRecycle');
Route::post('/convertrecycle', 'QueueController@convertRecycle');
Route::post('/updatecta', 'QueueController@updatePostCta');


//Price
Route::get('/price', 'SettingsController@price');
Route::get('/privacy', 'SettingsController@privacy');
Route::get('/terms', 'SettingsController@terms');
Route::get('/jobs', 'SettingsController@jobs');
Route::get('/team', 'SettingsController@team');
Route::get('/subscribe', 'SettingsController@subscribe')->middleware('auth');

//Analytics
Route::get('/analytics', 'AnalyticsController@analyticsView')->middleware('auth');

//Extension
Route::get('/post/getextension', 'HomeController@getExtension')->middleware('auth');

//Discover
Route::get('/post/discovercontent', 'PostController@discoverContent')->middleware('auth');
Route::post('/post/fetchpopularcontent', 'PostController@fetchPopularContent')->middleware('auth');


Route::post('/insertpost', 'HomeController@insertPost');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

//Account Controller
Route::get('/accounts','AccountController@accounts')->middleware('auth');
Route::get('/connectfacebook', 'AccountController@connectFacebook');
Route::post('/connectpinterest', 'AccountController@connectPinterest');
Route::get('/deleteaccount', 'AccountController@deleteAccount');

Route::post('/bulkdeleteaccounts', 'AccountController@bulkDeleteAccount');
Route::post('/getaccounts', 'AccountController@getAccounts');
Route::post('/getaccountsforanalytics', 'AccountController@getAccountsForAnalytics');
Route::get('/authbitly', 'AccountController@authBitly');
Route::get('/deletebitly', 'AccountController@deleteBitly');
Route::get('/switchbitly', 'AccountController@switchBitly');
Route::get('/getinstafeed', 'AccountController@getInstaFeed');

//Settings Controller
Route::get('/timezone', 'SettingsController@timezone')->middleware('auth');
Route::post('/updatetimezone', 'SettingsController@updateTimezone');
Route::get('/plan', 'SettingsController@plan');

//Pause Controller
Route::get('/pausequeue', 'QueueController@pauseQueueView')->middleware('auth');
Route::post('/updatecategorystate', 'CategoryController@updateCategoryState');

//Callbacks
Route::get('/facebookcallback', 'CallbackController@facebookCallback');
Route::get('/twitter/callback', 'CallbackController@twitterCallback');
Route::get('/google/callback', 'CallbackController@googleCallBack');
Route::get('/linkedin/callback', 'CallbackController@linkedInCallBack');
Route::get('/pinterestredirect', 'CallbackController@pinterestCallBack');

Route::post('/connectinstagram', 'AccountController@connectInstagram');
Route::post('connectinsta', 'AccountController@connectInstagramFromExt');
Route::post('/refreshinstagram', 'AccountController@refreshInstagram');
Route::post('/verifyinstagram', 'AccountController@verifyInstagram')->middleware('auth');;

//Automated feed
Route::post('/getsavedfeeds', 'PostController@getSavedFeeds');
Route::post('/addfeed', 'PostController@addFeed');
Route::post('/deletefeed', 'PostController@deleteFeed');


//Cron Controller
Route::get('/facebook', 'CronController@facebookCron');
Route::get('/facebookpaul', 'CronController@facebookCronPaul');
Route::get('/twitter', 'CronController@twitterCron');
Route::get('/twittervideo', 'CronController@twitterVideoCron');
Route::get('/instagram', 'CronController@instagramCron');
Route::get('/instagramnew', 'CronController@newInstagramCron');
Route::get('/instagramstory', 'CronController@instagramStoryCron');
Route::get('/instagramstorynew', 'CronController@newInstagramStory');
Route::get('/instagramstorytest', 'CronController@instagramStoryCronTest');
Route::get('/google', 'CronController@googleCron');
Route::get('/linkedin', 'CronController@linkedinCron');
Route::get('/pinterestcron', 'CronController@pinterestCron');
Route::get('/healthywomncron', 'CronController@pinterestCronHealthyWoman');
Route::get('/gmbcron', 'CronController@googleMyBusinessCron');

Route::get('googleanalytics', 'AnalyticsController@googleAnalytics');
Route::post('getinitialstate', 'AnalyticsController@getInitialState');
Route::post('toggletrackingstate', 'AnalyticsController@toggleTrackingState');
Route::post('updaterackingvariable', 'AnalyticsController@updateTrackingVariable');

Route::get('socialaccountutm', 'AnalyticsController@socialAccountUTM');
Route::post('toggleaccounttracking', 'AnalyticsController@toggleAccountTracking');
Route::post('updateaccounttracking', 'AnalyticsController@updateAccountTracking');

//Analytics Cron
Route::get('/facebookana', 'AnalyticsCronController@facebookCron');
Route::get('/twitterana', 'AnalyticsCronController@getTwitterAnalytics');
Route::get('/pinterestana', 'AnalyticsCronController@pinterestCron');

//RSS Cron
Route::get('/rsscron', 'CronController@rssCron');

//referral
Route::get('/trackreferral', 'HomeController@trackReferral');
Route::get('/sendcloud', 'HomeController@sendCloud');

//FreeTrial ExpiredMail
Route::get('/checktrial', 'HomeController@checkPlanExpiry');

//Team Member
Route::get('/inviteteam', 'HomeController@inviteTeam');
Route::post('/addteammember', 'HomeController@addTeamMember');
Route::post('/removeteammember', 'HomeController@removeTeamMember');

Route::get('/teamsignup', 'HomeController@teamSignUp');
Route::post('/teamregisterpost', 'HomeController@registerTeamMember');
Route::any('/teammemberlogin', 'HomeController@teamMemberLogin');

//Activate account
Route::post('/activateaccount', 'HomeController@activateAccount');

//Charles
Route::get('/getpostsbyusername', 'PostController@getPublishedPostByUsername');

//GMB Details
Route::get('/google-my-business', 'HomeController@gmbDetailsView');
Route::get('/oneup-vs-meet-edgar', 'HomeController@oneVsMeetView');
Route::get('/oneup-usecase', 'HomeController@oneupUseCase');

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('user', 'UserController@getAuthUser');
    Route::get('scheduledposts', 'UserController@getScheduledPosts');
    Route::get('publishedposts', 'UserController@getPublishedPosts');
    Route::get('failedposts', 'UserController@getFailedPosts');
    Route::get('getcategoryandaccounts', 'UserController@getCategoryAndAccounts');

    Route::post('postnow', 'UserController@postNow');
    Route::post('schedulepost', 'UserController@schedulePost');


});

Route::get('auth/login', 'UserController@login');

<?php //## NextScripts SNAP API 4.3.23
//================================Instagram=====================================
if (!class_exists('nxsAPI_IG')){class nxsAPI_IG{ var $ck = array(); var $agent=''; var $guid=''; var $phid='';  var $dId=''; var $tkn = ''; var $opNm = ''; var $debug = false; var $loc = ''; var $proxy = array(); var $u=''; var $p=''; var $sid='';
    function __construct() { $this->agent = 'Instagram 12.0.0.7.91 Android (21/5.0; 480dpi; 1080x1920; samsung; SM-G900P; kltespr; qcom; en_US)';
      $this->guid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',  mt_rand(0, 65535),  mt_rand(0, 65535),  mt_rand(0, 65535),  mt_rand(16384, 20479),  mt_rand(32768, 49151),  mt_rand(0, 65535),  mt_rand(0, 65535),  mt_rand(0, 65535));
      $this->dId = "android-".$this->guid;
    }
    function headers($ref, $org='', $type='GET', $aj=false){$hdrsArr = array(); $hdrsArr['User-Agent']='Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30';
      $hdrsArr['connection']='keep-alive'; $hdrsArr['Accept-Language']='en-US'; $hdrsArr['Accept-Encoding']='gzip, deflate'; if (!empty($ref)) $hdrsArr['Referer']=$ref;
      if($aj===true) $hdrsArr['X-Requested-With']='XMLHttpRequest'; if($type=='POST') $hdrsArr['Content-Type']='application/x-www-form-urlencoded';
      //$hdrsArr['X-IG-Capabilities']='3QI='; $hdrsArr['X-IG-Connection-Type']='WIFI';
    return $hdrsArr;}
    function doSig($data) { return hash_hmac('sha256', $data, 'b4946d296abf005163e72346a6d33dd083cadde638e6ad9c5eb92e381b35784a'); }
    function makeSQExtend( $imgSrc, $thumbFile, $thumbSize=1000 ){ $type = substr( $imgSrc , strrpos( $imgSrc , '.' )+1 );
      switch( $type ){ case 'jpg' : case 'jpeg' : $src = imagecreatefromjpeg( $imgSrc ); break; case 'png' : $src = imagecreatefrompng( $imgSrc ); break; case 'gif' : $src = imagecreatefromgif( $imgSrc ); break; }
      list($w, $h) = getimagesize($imgSrc); if ($w > $h)  $bgSide = $w; else { $bgSide = $h; } if ($thumbSize<$bgSide) $sqSize = $thumbSize; else $sqSize = $bgSide; //$width = imagesx( $src ); $height = imagesy( $src );
      if($w> $h) { $width_t=$sqSize; $height_t=round($h/$w*$sqSize); $off_y=ceil(($width_t-$height_t)/2); $off_x=0; }
        elseif($h> $w) { $height_t=$sqSize; $width_t=round($w/$h*$sqSize); $off_x=ceil(($height_t-$width_t)/2); $off_y=0; } else { $width_t=$height_t=$sqSize; $off_x=$off_y=0; }
      $new = imagecreatetruecolor( $sqSize , $sqSize ); $bg = imagecolorallocate ( $new, 255, 255, 255 ); imagefill ( $new, 0, 0, $bg ); imagecopyresampled( $new , $src , $off_x, $off_y, 0, 0, $width_t, $height_t, $w, $h );
      $res = imagejpeg( $new , $thumbFile, 100); @imagedestroy( $new ); @imagedestroy( $src );
    }
    function makeSQCrop( $imgSrc, $thumbFile, $thumbSize=1000 ){ list($width, $height) = getimagesize($imgSrc); $type = substr( $imgSrc , strrpos( $imgSrc , '.' )+1 );
      switch( $type ){ default : $src = imagecreatefromjpeg( $imgSrc ); break; case 'png' : $src = imagecreatefrompng( $imgSrc ); break; case 'gif' : $src = imagecreatefromgif( $imgSrc ); break; }
      if ($width > $height) { $y = 0; $x = ($width - $height) / 2;} else { $x = 0; $y = ($height - $width) / 2;} $minSide = min($width,$height);
      $thumb = imagecreatetruecolor($minSide, $minSide); imagecopyresampled($thumb, $src, 0, 0, $x, $y, $minSide, $minSide, $minSide, $minSide);
      unlink($imgSrc); imagejpeg($thumb,$thumbFile); @imagedestroy($src); @imagedestroy($thumb);
    }
    function makeJpg( $imgSrc, $thumbFile, $thumbSize=1000 ){ list($width, $height) = getimagesize($imgSrc); $type = substr( $imgSrc , strrpos( $imgSrc , '.' )+1 );
      switch( $type ){ case 'jpg' : case 'jpeg' : $src = imagecreatefromjpeg( $imgSrc ); break; case 'png' : $src = imagecreatefrompng( $imgSrc ); break; case 'gif' : $src = imagecreatefromgif( $imgSrc ); break; }
      unlink($imgSrc); imagejpeg($src,$thumbFile); @imagedestroy($src); @imagedestroy($thumb);
    }
    function setSession(){
      if (!empty($this->sid)) { if (empty($this->ck)) $this->ck = array(); if ($this->debug) echo "[FP] Setting Session...<br/>\r\n";
          foreach ($this->ck as $ci=>$cc) if ( $this->ck[$ci]->name=='sessionid') unset($this->ck[$ci]); $c = new NXS_Http_Cookie( array('name' => 'sessionid', 'value' => $this->sid) ); $this->ck[] = $c;
      }
    }
    function check($u=''){ $this->setSession(); $ck = $this->ck; if (!empty($ck) && is_array($ck)) { $hdrsArr = $this->headers('https://www.instagram.com/accounts/edit/','','GET',true); if ($this->debug) echo "[IG] Checking....<br/>\r\n";
        foreach ($ck as $ci=>$cc) { if ( $ck[$ci]->name=='sessionid' && empty($ck[$ci]->value)) unset($ck[$ci]); }
        $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, '', $this->proxy); $rep = nxs_remote_get('https://www.instagram.com/accounts/edit/?__a=1', $advSet); // if ($this->debug) prr($advSet);
        if (is_nxs_error($rep)) return false;  $contents = $rep['body']; // if ($this->debug) prr($rep); //die();
        if ($rep['response']['code']=='302' && $rep['headers']['location']=='https://www.instagram.com/challenge/?__a=1') return 'c'; if ($rep['response']['code']=='302')  return false;
        $jc = json_decode($contents, true); if (!empty($jc) && is_array($jc) && !empty($jc['form_data']) && is_array($jc['form_data']) && (!empty($jc['form_data']['username']) || !empty($jc['form_data']['email']))) {
           $this->prcCK($rep['cookies']); $usr = $jc['form_data']['username']; $usrEml = $jc['form_data']['email'];  if ($this->debug) echo "[IG] Logged as: ".$usr." (".$usrEml.")<br/>\r\n"; return true;
        } else  return false;
      } else return false;
    }
    function prcCK($ck){ if (empty($this->ck)) $this->ck = array(); foreach ($ck as $ci=>$cc) if (empty($ck[$ci]->value) || $ck[$ci]->value=='""') unset($ck[$ci]);
      foreach ($this->ck as $ci=>$cc) if (empty($this->ck[$ci]->value) || $this->ck[$ci]->value=='""') unset($this->ck[$ci]); foreach ($ck as $ci=>$cc) { if ( $ck[$ci]->name=='csrftoken') $this->tkn = $ck[$ci]->value;
      //if ( $ck[$ci]->name=='sessionid') $ck[$ci]->value = urlencode($cc->value);
      if ( $ck[$ci]->name=='checkpoint_step') unset($ck[$ci]); } $this->ck = nxs_MergeCookieArr($this->ck, $ck);
    }
    function bldBody($arr){ $body = "";
      foreach($arr as $b){ $body .= "--".str_replace('-','',$this->guid)."\r\n"; $body .= "Content-Disposition: ".$b["type"]."; name=\"".$b["name"]."\"";
        if(isset($b["filename"])) { $ext = pathinfo($b["filename"], PATHINFO_EXTENSION); $body .= "; filename=\"".substr(bin2hex($b["filename"]),0,18).".".$ext."\""; }
        if(isset($b["headers"]) && is_array($b["headers"])) foreach($b["headers"] as $header)$body.= "\r\n".$header; $body.= "\r\n\r\n".$b["data"]."\r\n";
      } $body .= "--".str_replace('-','',$this->guid)."--"; return $body;
    }
    function connect($u='',$p=''){ $badOut = 'Error: '; if (!empty($u)) $this->u = $u; if (!empty($p)) $this->p = $p; $u = $this->u; $p = $this->p; $chk = $this->check($u);
      if ($chk==='c') return "You've got a login checkpoint! Please login to Instagram from your phone and confirm the login or action before trying to post again.";
      //## Check if alrady IN
      if ($chk===true) { if ($this->debug) echo "[IG] Saved Data is OK;<br/>\r\n"; return false; } else  return $this->connectW();
    }
    function connectW($u='',$p=''){ $badOut = 'Error: '; if (!empty($u)) $this->u = $u; if (!empty($p)) $this->p = $p; $u = $this->u; $p = $this->p;
      if ($this->debug) echo "[IG] NO Saved Data; Logging in...<br/>\r\n";  $this->ck = array();
        $advSet = nxs_mkRemOptsArr( $this->headers('https://www.instagram.com/',''), '', '', $this->proxy); $rep = nxs_remote_get('https://www.instagram.com/', $advSet); $this->prcCK($rep['cookies']);  $flds = array('username'=>$u, '&password'=>$p);
        //## ACTUAL LOGIN
        $ck = $this->ck;  $hdrsArr =  $this->headers('https://www.instagram.com/', '', 'POST', true);  $hdrsArr['X-Instagram-AJAX'] = '1'; $hdrsArr['X-CSRFToken'] = $this->tkn; $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $flds, $this->proxy);
        unset($advSet['user-agent']);
        $rep = nxs_remote_post('https://www.instagram.com/accounts/login/ajax/', $advSet); $this->prcCK($rep['cookies']); $ck = $this->ck; //prr($advSet);
        if (is_nxs_error($rep)) {  $badOut = "|ERROR -02-".print_r($rep, true); return $badOut; } if (empty($rep['body'])) {  $badOut = "|ERROR -03-".print_r($rep, true); return $badOut; }  $obj = @json_decode($rep['body'], true); //prr($rep);
        if (empty($obj) || !is_array($obj) || empty($obj['status'])) {  $badOut = "|ERROR -04- ".print_r($rep, true); return $badOut; }
        if ($obj['status']!='ok' && !empty($obj['message']) && $obj['message']=='checkpoint_required' && !empty($obj['checkpoint_url'])) { $this->prcCK($rep['cookies']); $ck = $this->ck;
        if (stripos($obj['checkpoint_url'], 'integrity')!==false) {
            $hdrsArr =  $this->headers('https://www.instagram.com/','https://www.instagram.com/', '', true);  $hdrsArr['X-Instagram-AJAX'] = '1'; $hdrsArr['X-CSRFToken'] = $this->tkn; $cpURL = $obj['checkpoint_url'];
            $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $flds, $this->proxy); $rep = nxs_remote_get($cpURL, $advSet); // prr($advSet); prr($rep);
          } else {
            $flds = 'choice=1'; $hdrsArr =  $this->headers('https://www.instagram.com/','https://www.instagram.com/', $flds, true);  $hdrsArr['X-Instagram-AJAX'] = '1'; $hdrsArr['X-CSRFToken'] = $this->tkn; $cpURL = $obj['checkpoint_url'];
            $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $flds, $this->proxy); $rep = nxs_remote_post('https://www.instagram.com'.$cpURL, $advSet); //prr($advSet); prr($rep);
            $obj = @json_decode($rep['body'], true);  if ($obj['status']=='ok') { $this->prcCK($rep['cookies']); $ck = $this->ck;
              if (function_exists('nxs_getOption')) { $opVal = array(); $opNm = $this->opNm; $opVal = nxs_getOption($opNm); $opVal['ck'] = $ck;  $opVal['url'] = $cpURL; nxs_saveOption($opNm, $opVal); }
            } return 'cpt';
          }
        } if ($obj['status']!='ok' && !empty($obj['message']))  return "|ERROR -LOGIN- ".print_r($obj, true);
        if ( empty($obj['authenticated']) && ( empty($obj['logged_in_user']) || empty($obj['logged_in_user']['username']))) { $repM = $this->connectM(); if ($repM==false) return false; else return "|ERROR -05.1- ".print_r($repM, true).' | '.print_r($rep, true); }
        if ($obj['status']=='ok') { $this->prcCK($rep['cookies']); return false; } else return "|ERROR -LOGIN 2- ".print_r($obj, true);
    }
    function connectM($u='',$p=''){ if (!empty($u)) $this->u = $u; if (!empty($p)) $this->p = $p; $u = $this->u; $p = $this->p; $badOut = 'Error: '; // $this->debug = true;
        $flds = '{"device_id":"'.$this->dId.'","guid":"'.$this->guid.'","username":"'.$u.'","password":"'.$p.'","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'; $flds = 'signed_body='.$this->doSig($flds).'.'.urlencode($flds).'&ig_sig_key_version=4';
        //## ACTUAL LOGIN
        $hdrsArr = $this->headers('', '', 'POST'); $hdrsArr['User-Agent']=$this->agent;  $advSet = nxs_mkRemOptsArr($hdrsArr, '', $flds, $this->proxy); $rep = nxs_remote_post('https://i.instagram.com/api/v1/accounts/login/', $advSet); //prr($advSet); prr($rep);
        $obj = @json_decode($rep['body'], true); $this->prcCK($rep['cookies']); $ck = $this->ck; if ($obj['status']=='ok') return false;
        if ($obj['status']!='ok' && !empty($obj['message'])) return 'IG M Error (Message):'.print_r($obj['message'], true)." | ".(!empty($obj['error_type'])?print_r($obj['error_type'], true):'');
        if ($obj['status']!='ok' && !empty($obj['message']) && $obj['message']=='checkpoint_required' && !empty($obj['checkpoint_url'])) { $hdrsArr = $this->headers('https://www.instagram.com');
           $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, '', $this->proxy); $lrep = nxs_remote_get("https://i.instagram.com/integrity/checkpoint/?next=instagram%3A%2F%2Fcheckpoint%2Fdismiss", $advSet);// prr($lrep, '');
            $this->prcCK($lrep['cookies']); $ck = $this->ck;
           if (function_exists('nxs_getOption')) { $opVal = array(); $opNm = $this->opNm; $opVal = nxs_getOption($opNm); $opVal['ck'] = $ck; nxs_saveOption($opNm, $opVal); }
           return 'cnf';
        } return 'IG M Error:'.print_r($rep, true);
    }
    function checkCode($url, $code){ $ck = $this->ck; foreach ($ck as $ci=>$cc) { if ( $ck[$ci]->name=='sessionid' && empty($ck[$ci]->value)) unset($ck[$ci]); }   foreach ($ck as $ci=>$cc) { if ( $ck[$ci]->name=='csrftoken') $tkn = $ck[$ci]->value; }
      $flds = 'security_code='.$code; $hdrsArr = nxs_makeHeaders('https://www.instagram.com/','https://www.instagram.com/', $flds, true);  $hdrsArr['X-Instagram-AJAX'] = '1'; $hdrsArr['X-CSRFToken'] = $tkn;
      $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $flds, $this->proxy); $rep = nxs_remote_post('https://www.instagram.com'.$url, $advSet); //prr('https://www.instagram.com'.$url, 'Code CHECK'); prr($advSet); prr($rep);
      $obj = @json_decode($rep['body'], true);  if ($obj['status']=='ok') {
          $this->prcCK($rep['cookies']); $ck = $this->ck;  return $ck;
      } return false;
    }
    function post($msg, $imgURL, $style='E'){
      $ck = $this->ck;
      if ($this->debug) echo "[IG] Posting to ...".$imgURL."<br/>\r\n";
      $badOut = '';
      $msg = strip_tags($msg);
      $msg = str_replace("&", "and", $msg);
      $msg = str_replace(";", ",", $msg);

      if (empty($imgURL)) return 'No Image Provided. Can\'t post to Instagram without an image';


      $hdrsArr = '';

      $nxAPIVer = defined('NXSAPIVER')?NXSAPIVER:'NV';
      $nxAPIVer2 = defined('NextScripts_SNAP_Version')?NextScripts_SNAP_Version:'NV';
      $advSet = nxs_mkRemOptsArr($hdrsArr, '', '', $this->proxy);

      $imgData = file_get_contents($imgURL);


      //Upload starts here
      foreach ($ck as $c) { if ($c->name=='csrftoken') $xftkn = $c->value;  if ($c->name=='ds_user_id') $uid = $c->value;} $ddt = date("Y:m:d H:i:s");

      $octStreamArr = array(array('type' => 'form-data', 'name' => 'upload_id', 'data' => (time().'033')),array('type' => 'form-data','name' => 'photo','data' => $imgData,'filename' => 'pending_media_'.time().'003.jpg','headers' =>array("Content-type: image/jpeg")),array('type' => 'form-data', 'name' => 'media_type', 'data' => '1')); $data = $this->bldBody($octStreamArr);

      $hdrsArr = $this->headers('https://www.instagram.com/create/style/', '', 'POST', true);
      $hdrsArr['Content-Type']= 'multipart/form-data; boundary='.str_replace('-','',$this->guid);
      $hdrsArr['X-Instagram-AJAX']= '1';
      $hdrsArr['X-CSRFToken']= $this->tkn;
      $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $data, $this->proxy);// prr($hdrsArr); prr($ck); //prr($advSet);
      $advSet['usearray'] = '1';
      $rep = nxs_remote_post('https://www.instagram.com/create/upload/photo/', $advSet); //prr($rep); die();
      if (is_nxs_error($rep)) {  $badOut .= "|ERROR -02I- ".print_r($rep, true); return $badOut; }

      if ($rep['response']['code']=='302' && stripos($rep['headers']['location'], 'accounts/login')!==falsee) { $c = new NXS_Http_Cookie( array('name' => 'lg', 'value' => 'lg')); $ck = array($c);
        $this->ck = $ck; if (function_exists('nxs_getOption')) { $opVal = array(); $opNm = $this->opNm; $opVal = nxs_getOption($opNm); $opVal['ck'] = $ck;   nxs_saveOption($opNm, $opVal); }  return "Logged out. Please try to make test post.";
      }

      if (empty($rep['body'])) {  $badOut .= "|ERROR -03I- ". print_r($rep, true); return $badOut; } $obj = @json_decode($rep['body'], true);

      if (($obj['status']!='ok' && $obj['message']=='login_required')) {   if ($this->debug) {  prr($obj); echo "[IG] Login M<br/>\r\n";}
         $res = $this->connectM(); if ($res=='cnf') return "You've got checkpoint! Please login to Instagram from your phone and confirm the login or action before trying to post again."; $ck = $this->ck;
         $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $data, $this->proxy); $rep = nxs_remote_post('https://i.instagram.com/api/v1/upload/photo/', $advSet);  $obj = @json_decode($rep['body'], true);
      }

      if ($obj['status']!='ok' && stripos($obj['redirect_url'], '/accounts/login/')!==false) { $c = new NXS_Http_Cookie( array('name' => 'lg', 'value' => 'lg')); $ck = array($c);
        $this->ck = $ck; if (function_exists('nxs_getOption')) { $opVal = array(); $opNm = $this->opNm; $opVal = nxs_getOption($opNm); $opVal['ck'] = $ck;   nxs_saveOption($opNm, $opVal); }
        return "Logged out. Please try to post again";
      }
      if (empty($obj) || !is_array($obj) || empty($obj['status']) || $obj['status']!='ok'){ $badOut .= "|ERROR 04IG (".$nxAPIVer." [".$nxAPIVer2."]) - ".print_r($rep, true); return $badOut; }

      $hdrsArr = $this->headers('https://www.instagram.com/create/details/', '', 'POST', true);
      $data = 'upload_id='.$obj['upload_id'].'&caption='.$msg;
      $hdrsArr['X-Instagram-AJAX']= '1';
      $hdrsArr['X-CSRFToken']= $this->tkn;
      $hdrsArr['Content-Type']= 'application/x-www-form-urlencoded';
      $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $data, $this->proxy);//prr($advSet);
      $advSet['usearray'] = '1';
      $rep = nxs_remote_post('https://www.instagram.com/create/configure/', $advSet);
      if (is_nxs_error($rep)) {  $badOut .= "|ERROR -07I-".print_r($rep, true); return $badOut; }    if (empty($rep['body'])) {  $badOut .= "|ERROR -08I-".print_r($rep, true); return $badOut; }
      $obj = @json_decode($rep['body'], true);
      if (empty($obj) || !is_array($obj) || empty($obj['status'])){ $badOut .= "|ERROR -09I- ".print_r($rep, true).' | <br/> DATA:'.print_r($data, true); return $badOut; }
      if ($obj['status']!='ok' && $obj['message']=='checkpoint_required') { return "You got checkpoint! Please login to Instagram from your phone and confirm the login or action."; }
      if ($obj['status']!='ok' && !empty($obj['message'])) { $badOut .= "|ERROR -POST- ".print_r($obj, true); return $badOut; }
      if ($obj['status']=='ok') { return array("isPosted"=>"1", "postID"=>$obj['media']['code'], 'pDate'=>date('Y-m-d H:i:s'), "postURL"=>'https://www.instagram.com/p/'.$obj['media']['code'], 'msg'=>$badOut); } else {  $badOut .= print_r($rep, true)."|ERROR -XI- "; return $badOut; }
    }

    function postStory($msg, $imgURL, $style='E'){
      $ck = $this->ck;
      if ($this->debug) echo "[IG] Posting to ...".$imgURL."<br/>\r\n";
      $badOut = '';
      $msg = strip_tags($msg);
      $msg = str_replace("&", "and", $msg);
      $msg = str_replace(";", ",", $msg);

      if (empty($imgURL)) return 'No Image Provided. Can\'t post to Instagram without an image';
      //## Get image

      $hdrsArr = '';

      $nxAPIVer = defined('NXSAPIVER')?NXSAPIVER:'NV';
      $nxAPIVer2 = defined('NextScripts_SNAP_Version')?NextScripts_SNAP_Version:'NV';
      $advSet = nxs_mkRemOptsArr($hdrsArr, '', '', $this->proxy);

      $imgData = file_get_contents($imgURL);


      //Upload starts here
      foreach ($ck as $c) { if ($c->name=='csrftoken') $xftkn = $c->value;  if ($c->name=='ds_user_id') $uid = $c->value;} $ddt = date("Y:m:d H:i:s");

      $octStreamArr = array(array('type' => 'form-data', 'name' => 'upload_id', 'data' => (time().'033')),array('type' => 'form-data','name' => 'photo','data' => $imgData,'filename' => 'pending_media_'.time().'003.jpg','headers' =>array("Content-type: image/jpeg")),array('type' => 'form-data', 'name' => 'media_type', 'data' => '1')); $data = $this->bldBody($octStreamArr);

      $hdrsArr = $this->headers('https://www.instagram.com/create/style/', '', 'POST', true);
      $hdrsArr['Content-Type']= 'multipart/form-data; boundary='.str_replace('-','',$this->guid);
      $hdrsArr['X-Instagram-AJAX']= '1';
      $hdrsArr['X-CSRFToken']= $this->tkn;
      $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $data, $this->proxy);// prr($hdrsArr); prr($ck); //prr($advSet);
      $advSet['usearray'] = '1';
      $rep = nxs_remote_post('https://www.instagram.com/create/upload/photo/', $advSet); //prr($rep); die();
      if (is_nxs_error($rep)) {  $badOut .= "|ERROR -02I- ".print_r($rep, true); return $badOut; }

      if ($rep['response']['code']=='302' && stripos($rep['headers']['location'], 'accounts/login')!==falsee) { $c = new NXS_Http_Cookie( array('name' => 'lg', 'value' => 'lg')); $ck = array($c);
        $this->ck = $ck; if (function_exists('nxs_getOption')) { $opVal = array(); $opNm = $this->opNm; $opVal = nxs_getOption($opNm); $opVal['ck'] = $ck;   nxs_saveOption($opNm, $opVal); }  return "Logged out. Please try to make test post.";
      }

      if (empty($rep['body'])) {  $badOut .= "|ERROR -03I- ". print_r($rep, true); return $badOut; } $obj = @json_decode($rep['body'], true);

      if (($obj['status']!='ok' && $obj['message']=='login_required')) {   if ($this->debug) {  prr($obj); echo "[IG] Login M<br/>\r\n";}
         $res = $this->connectM(); if ($res=='cnf') return "You've got checkpoint! Please login to Instagram from your phone and confirm the login or action before trying to post again."; $ck = $this->ck;
         $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $data, $this->proxy); $rep = nxs_remote_post('https://i.instagram.com/api/v1/upload/photo/', $advSet);  $obj = @json_decode($rep['body'], true);
      }

      if ($obj['status']!='ok' && stripos($obj['redirect_url'], '/accounts/login/')!==false) { $c = new NXS_Http_Cookie( array('name' => 'lg', 'value' => 'lg')); $ck = array($c);
        $this->ck = $ck; if (function_exists('nxs_getOption')) { $opVal = array(); $opNm = $this->opNm; $opVal = nxs_getOption($opNm); $opVal['ck'] = $ck;   nxs_saveOption($opNm, $opVal); }
        return "Logged out. Please try to post again";
      }
      if (empty($obj) || !is_array($obj) || empty($obj['status']) || $obj['status']!='ok'){ $badOut .= "|ERROR 04IG (".$nxAPIVer." [".$nxAPIVer2."]) - ".print_r($rep, true); return $badOut; }

      $hdrsArr = $this->headers('https://www.instagram.com/create/details/', '', 'POST', true);
      $data = 'upload_id='.$obj['upload_id'].'&caption='.$msg;
      $hdrsArr['X-Instagram-AJAX']= '1';
      $hdrsArr['X-CSRFToken']= $this->tkn;
      $hdrsArr['Content-Type']= 'application/x-www-form-urlencoded';
      $advSet = nxs_mkRemOptsArr($hdrsArr, $ck, $data, $this->proxy);//prr($advSet);
      $advSet['usearray'] = '1';
      $rep = nxs_remote_post('https://www.instagram.com/create/configure_to_story/', $advSet);
      if (is_nxs_error($rep)) {  $badOut .= "|ERROR -07I-".print_r($rep, true); return $badOut; }    if (empty($rep['body'])) {  $badOut .= "|ERROR -08I-".print_r($rep, true); return $badOut; }
      $obj = @json_decode($rep['body'], true);
      if (empty($obj) || !is_array($obj) || empty($obj['status'])){ $badOut .= "|ERROR -09I- ".print_r($rep, true).' | <br/> DATA:'.print_r($data, true); return $badOut; }
      if ($obj['status']!='ok' && $obj['message']=='checkpoint_required') { return "You got checkpoint! Please login to Instagram from your phone and confirm the login or action."; }
      if ($obj['status']!='ok' && !empty($obj['message'])) { $badOut .= "|ERROR -POST- ".print_r($obj, true); return $badOut; }
      if ($obj['status']=='ok') { return array("isPosted"=>"1", "postID"=>$obj['media']['code'], 'pDate'=>date('Y-m-d H:i:s'), "postURL"=>'https://www.instagram.com/p/'.$obj['media']['code'], 'msg'=>$badOut); } else {  $badOut .= print_r($rep, true)."|ERROR -XI- "; return $badOut; }
    }
}}

?>

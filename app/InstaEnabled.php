<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstaEnabled extends Model
{
  protected $table = 'insta_enabled';
  public $timestamps = false;
}

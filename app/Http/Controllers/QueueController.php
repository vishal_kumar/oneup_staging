<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use Illuminate\Support\Facades\DB;
use App\Posts;
use App\PublishedPosts;
use App\Category;
use App\SocialAccounts;
use App\FailedPosts;
use Carbon\Carbon;
use JavaScript;
use Mixpanel;

class QueueController extends Controller
{
    public function queueView(){

        $user = Auth::user();

        $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
    		$mp->identify($user->email);
    		$mp->track("User visited Queue page");

        $numOfDays = app('App\Http\Controllers\PlanauthController')->giveRemaningDaysInTrial();

        if(app('App\Http\Controllers\PlanauthController')->checkAccountExpiry())
    	   return view('queueView')->with('timezone', $user->timezone)->with('numOfDays', $numOfDays);
        else
           return view('subscribeView');
    }

    public function pauseQueueView(){
        return view('pauseQueueView');
    }

    public function calendarView(){
        $user = Auth::user();

        $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
    		$mp->identify($user->email);
    		$mp->track("User visited Calendar View page");

        return view('calendarView')->with('timezone', $user->timezone);;
    }

    public function getPostCounts(){
      $user = Auth::user();
      $obj = new \stdClass();
      $obj->publishedCount = PublishedPosts::where('email', $user->email)->count();
      return json_encode($obj);
    }

    public function getCategoryAndAccounts(){
      $user = Auth::user();
      $obj = new \stdClass();
      $obj->category =  DB::select('select c.id, c.category_name, c.color,c.isPaused, GROUP_CONCAT(ca.social_network_name) as selected_network,GROUP_CONCAT(ca.social_network_type) as network_type,GROUP_CONCAT(ca.social_network_id) as network_id from category c left join category_account ca on c.id = ca.category_id  where email = "'.$user->email.'" GROUP BY category_name');
    	$obj->accounts = SocialAccounts::where('email', $user->email)->get();

      return json_encode($obj);
    }

    public function getPosts(){
      $user = Auth::user();
      $dt = Carbon::now('UTC');

    	$obj = new \stdClass();
    	$obj->scheduledPosts = Posts::where('email', $user->email)->where('isPublished', 0)->get();

    	return json_encode($obj);
    }

    public function getPaginatedPosts(Request $request){
        $user = Auth::user();
        $start_count = $request->input('start');
        $categoryId = $request->input('categoryId');
        $accountId = $request->input('accountId');
        $start_count = $start_count;


        $obj = new \stdClass();
        if($categoryId !=-1)
          $obj->scheduledPosts = DB::select("SELECT *, p.id as post_id, p.image_url as content_image from posts p
                                                  inner join category c
                                                  on p.category_id = c.id
                                                  inner join social_accounts sa
                                                  on p.social_network_id = sa.social_account_id
                                                  where p.email='".$user->email."'
                                                  and sa.email = '".$user->email."'
                                                  and p.category_id=".$categoryId."
                                                  and isPublished =0 and isPublished !=1 and isPublished !=3 and isPublished !=10
                                                  ORDER BY p.ts_gmt asc limit ".$start_count.",50
                                            ");
        else if($accountId !=-1)
          $obj->scheduledPosts = DB::select("SELECT *, p.id as post_id, p.image_url as content_image from posts p
                                                  inner join category c
                                                  on p.category_id = c.id
                                                  inner join social_accounts sa
                                                  on p.social_network_id = sa.social_account_id
                                                  where p.email='".$user->email."'
                                                  and sa.email = '".$user->email."'
                                                  and p.social_network_id='".$accountId."'
                                                  and isPublished =0 and isPublished !=1 and isPublished !=3 and isPublished !=10
                                                  ORDER BY p.ts_gmt asc limit ".$start_count.",50
                                            ");
        else
          $obj->scheduledPosts = DB::select("SELECT *, p.id as post_id, p.image_url as content_image from posts p
                                                  left join category c
                                                  on p.category_id = c.id
                                                  left join social_accounts sa
                                                  on p.social_network_id = sa.social_account_id
                                                  where p.email='".$user->email."'
                                                  and sa.email = '".$user->email."'
                                                  and isPublished =0 and isPublished !=1 and isPublished !=3 and isPublished !=10
                                                  ORDER BY p.ts_gmt asc limit ".$start_count.",50
                                            ");

        return json_encode($obj);

    }

    public function getPublishedPosts(){
        $user = Auth::user();

        $obj = new \stdClass();
        $obj->publishedPosts = DB::select("SELECT  pp.id, pp.post_id, pp.content,pp.image_url as content_image,
                                                pp.source_url, pp.social_network_type,
                                                pp.social_network_id,pp.social_network_name,pp.board_name,
                                                pp.times_shared, pp.isEvergreen,
                                                pp.ts_gmt,
                                                pp.likes,
                                                pp.comments,
                                                pp.share,
                                                c.id as category_id,
                                                c.category_name,c.color,sa.image_url
                                                from published_posts pp
                                                right join category c
                                                on pp.category_id = c.id
                                                inner join social_accounts sa
                                                on pp.social_network_id = sa.social_account_id
                                                where pp.email='".$user->email."'
                                                and sa.email = '".$user->email."'
                                                ORDER BY pp.id desc
                                                limit 200
                                          ");

        return json_encode($obj);
    }

    public function getFailedPosts(){
        $user = Auth::user();

        $obj = new \stdClass();
        if($user->email =='support@monsterfunder.com' || $user->email =='support@contentmo.com' || $user->email =='ken@gouldbarbers.co.uk')
          $obj->failedPosts = DB::select("SELECT  *,p.id as post_id,pp.id as id,pp.content,pp.image_url as content_image,
                                                  pp.source_url, pp.social_network_type,
                                                  pp.social_network_id,pp.social_network_name,pp.board_name,
                                                  pp.times_shared, pp.isEvergreen, pp.ts_gmt, pp.fail_reason,
                                                  c.category_name,c.color,sa.image_url
                                                  from failed_posts pp
                                                  inner join category c
                                                  on pp.category_id = c.id
                                                  inner join social_accounts sa
                                                  on pp.social_network_id = sa.social_account_id
                                                  inner join posts p
                                                  on pp.post_id = p.id
                                                  where pp.email='".$user->email."'
                                                  and sa.email = '".$user->email."'
                                                  ORDER BY pp.id desc");
        else
          $obj->failedPosts = DB::select("SELECT  *,p.id as post_id,pp.id as id, pp.content,pp.image_url as content_image,
                                                  pp.source_url, pp.social_network_type,
                                                  pp.social_network_id,pp.social_network_name,pp.board_name,
                                                  pp.times_shared, pp.isEvergreen, pp.ts_gmt, pp.fail_reason,
                                                  c.category_name,c.color,sa.image_url
                                                  from failed_posts pp
                                                  inner join category c
                                                  on pp.category_id = c.id
                                                  inner join social_accounts sa
                                                  on pp.social_network_id = sa.social_account_id
                                                  inner join posts p
                                                  on pp.post_id = p.id
                                                  where pp.email='".$user->email."'
                                                  and sa.email = '".$user->email."'
                                                  ORDER BY pp.id desc limit 100
                                            ");


        return json_encode($obj);

    }

    public function getPublishedPostsForAnalytics(){
      $user = Auth::user();

      $obj = new \stdClass();
      $obj->publishedPosts = DB::select("SELECT pp.id,pp.content,pp.image_url as content_image,
                                              pp.source_url, pp.social_network_type,
                                              pp.social_network_id,pp.social_network_name,pp.board_name,
                                              pp.times_shared, pp.isEvergreen,
                                              pp.ts_gmt,
                                              pp.likes,
                                              pp.comments,
                                              pp.share,
                                              pp.media_id,
                                              c.id as category_id,
                                              c.category_name,c.color,sa.image_url
                                              from published_posts pp
                                              right join category c
                                              on pp.category_id = c.id
                                              inner join social_accounts sa
                                              on pp.social_network_id = sa.social_account_id
                                              where pp.email='".$user->email."'
                                              and (pp.social_network_type = 0
                                              or pp.social_network_type = 1
                                              )
                                              ORDER BY pp.id desc
                                              limit 120
                                        ");

      $obj->publishedCount = PublishedPosts::where('email', $user->email)->count();
      return json_encode($obj);
    }


    public function editPost(Request $request){

        $user = Auth::user();

        $post_id = $request->input('post_id');
        $failed_post_id = $request->input('failed_post_id');
        $category_id = $request->input('category_id');
        $content = $request->input('content');
        $source_url = $request->input('source_url');
        $image_url = $request->input('image_url');
        $timestamp = $request->input('scheduledDate')." ".$request->input('scheduledTime');
        $dt = Carbon::createFromFormat('Y-m-d H:i', $timestamp, $user->timezone);

        if($failed_post_id != 0){
          DB::table('failed_posts')->where('id', $failed_post_id)
                                      ->delete();
        }

        Posts::where('id', $post_id)
                 ->update(['category_id' => $category_id,
                            'content'=>$content,
                            'source_url'=>$source_url,
                            'image_url' => $image_url,
                            'ts_gmt' =>$dt->timestamp,
                            'isPublished' => 0,
                            'date_time' => $dt->toDateTimeString()
                          ]);

        return  $dt->timestamp;
    }

    public function updatePostCta(Request $request){
      $user = Auth::user();

      $post_id = $request->input('post_id');
      $ctaType = $request->input('ctaType');
      $ctaUrl = $request->input('ctaUrl');

      Posts::where('id', $post_id)
               ->update(['extraParams'=> $ctaType,
                          'ctaUrl'=> $ctaUrl
                        ]);

    }

    public function deletePost(Request $request){
    	$post_id = $request->input('post_id');
      DB::table('posts')->where('id', $post_id)
                                  ->delete();

      DB::table('published_posts')->where('post_id', $post_id)
                                  ->delete();
      DB::table('failed_posts')->where('post_id', $post_id)
                                  ->delete();
    }

    public function deleteFailedPost(Request $request){
      $id = $request->input('post_id');
      DB::table('failed_posts')->where('id', $id)
                                  ->delete();
    }



    public function bulkDeletePost(Request $request){
        $posts_id = $request->input('posts_id');
        $posts_id = json_decode(json_encode($posts_id),true);

        for($i = 0 ; $i < count($posts_id); $i++){
           DB::table('posts')->where('id', $posts_id[$i])
                                    ->delete();
        }
        return 1;
    }

    public function bulkDeleteFailedPost(){

      $user = Auth::user();
      //delete from scheduled section
      DB::table('posts')->where('email', $user->email)->where('isPublished', 3)
                               ->delete();

      DB::table('failed_posts')->where('email', $user->email)->delete();

      return 1;
    }

    public function cancelRecycle(Request $request){
      $post_id = $request->input('post_id');
      Posts::where('id', $post_id)
               ->update(['recycleMode' => 0, 'isEvergreen' => 0]);
    }

    public function convertRecycle(Request $request){
      $post_id = $request->input('post_id');
      $recycle_mode = $request->input('recycle_mode');

      $isEvergreen = $recycle_mode ==0?0:1;
      $repeatTime = $request->input('repeatTime');
      $expiryDate = $request->input('expiryDate');

      $customFrequencyInterval = $request->input('customFrequencyInterval');
      $customFrequencyMode = $request->input('customFrequencyMode');

      Posts::where('id', $post_id)
               ->update(['recycleMode' => $recycle_mode,
                          'isEvergreen' => $isEvergreen,
                          'expiry_date'=> $expiryDate,
                          'expiry_after_share' => $repeatTime,
                          'customFrequencyInterval' => $customFrequencyInterval,
                          'customFrequencyMode' => $customFrequencyMode
                        ]);


    }

    public static function getEvents(Request $request){
        $user = Auth::user();
        $category_id = $request->input('categoryId');
        $social_networks = $request->input('socialNetworks');

        $data = array();
        $orClause = '';

        for($i = 0; $i < count($social_networks); $i++){
            if($i == 0)
                $orClause .= ' and (social_network_id = "'.$social_networks[$i].'"';
            else
                $orClause .= ' or social_network_id = "'.$social_networks[$i].'"';
        }
        if(count($social_networks) != 0)
          $orClause .= ')';

        if($category_id != "ALL")
          $data = DB::select("Select * from posts where isPublished=0 and email='".$user->email."' and category_id=".$category_id.$orClause." order by ts_gmt asc");
        else
          $data = DB::select("Select * from posts where isPublished=0 and email='".$user->email."'".$orClause." order by ts_gmt asc");

        $data = json_decode(json_encode($data), true);
        $arr= [];
        $user = Auth::user();

        for($i = 0; $i < count($data); $i++){
            $dt = Carbon::createFromTimestamp($data[$i]['ts_gmt']);
            $dt->setTimezone($user->timezone);
            $obj = [];
            $obj['id'] = $data[$i]['id'];
            $obj['title'] = $data[$i]['social_network_name'];
            $obj['start'] = $dt->toDateTimeString();
            $obj['time'] = $dt->format('h:i A');
            $obj['image_url'] = $data[$i]['image_url'];
            $obj['source_url'] = $data[$i]['source_url'];
            $obj['network_name'] = $data[$i]['social_network_name'];
            $obj['network_type'] = $data[$i]['social_network_type'];
            $obj['category_id'] = $data[$i]['category_id'];
            $obj['network_id'] = $data[$i]['social_network_id'];
            $obj['description'] = $data[$i]['content'];
            $obj['isEvergreen'] = $data[$i]['isEvergreen'];

            if($data[$i]['recycleMode'] == "6")
              $obj['recycleMode'] = "Daily";
            if($data[$i]['recycleMode'] == "1")
              $obj['recycleMode'] = "Weekly";
            if($data[$i]['recycleMode'] == "2")
              $obj['recycleMode'] = "Monthly";
            if($data[$i]['recycleMode'] == "3")
              $obj['recycleMode'] = "Once Every 3 Months";
            if($data[$i]['recycleMode'] == "4")
              $obj['recycleMode'] = "Once Every 6 Months";
            if($data[$i]['recycleMode'] == "5")
              $obj['recycleMode'] = "Once Every Year";

            if($data[$i]['isPublished']  == 0 && $data[$i]['email'] == $user->email)
              array_push($arr, $obj);
        }

        return $arr;
    }

    public function updatePostCalendar(Request $request){
         $user = Auth::user();
         $id = $request->input('post_id');
         $updated_datetime = $request->input('date_time');

         $dt = Carbon::createFromFormat('Y-m-d H:i', $updated_datetime, $user->timezone);

        Posts::where('id', $id)
                 ->update(['date_time' => $dt->toDateTimeString(),
                            'ts_gmt'=>$dt->timestamp
                          ]);
    }
}

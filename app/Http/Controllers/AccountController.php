<?php

namespace App\Http\Controllers;
include_once  public_path().'/../vendor/google/vendor/autoload.php';
//include_once '/Users/vishalkumar 1 2/Desktop/oneupapp/vendor/google/vendor/autoload.php';

use \App\Classes\APInsta;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Facebook\Facebook;
use Auth;
use Session;
use App\SocialAccounts;
use App\Boards;
use App\User;
use App\Category;
use App\CategoryAccount;
use App\InstaEnabled;
use Illuminate\Support\Facades\Redirect;

use \TwitterOAuth\Auth\SingleUserAuth;
use \TwitterOAuth\Serializer\ArraySerializer;
use \DirkGroenen\Pinterest\Pinterest;

use LinkedIn\Client;
use LinkedIn\Scope;

use Mixpanel;


class AccountController extends Controller
{
	public function accounts(){


    $user = Auth::user();

		$mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
		$mp->identify($user->email);
		$mp->track("User visited Accounts page");


		$fb_login_url = $this->connectFacebook();
    $twitter_login_url = $this->connectTwitter();
    $google_login_url = $this->connectGoogle();
    $linkedin_login_url = $this->connectLinkedIn();
    $pinterest_login_url = $this->connectPinterestOfficial();
    $bitly_username = $user->bitly_username;
    $bitly_enabled = $user->enable_bitly;
 		Session::put('email',$user->email);


		$accounts = SocialAccounts::where('email', $user->email)->get();

    $insta_account_count = SocialAccounts::where('email', $user->email)->where('social_network_type',2)->count();
    $insta_limit = $user->insta_limit;

		//InstaEnabled accounts
		$isInstaEnabled = $user->plan == NULL? false: true;

		$numOfDays = app('App\Http\Controllers\PlanauthController')->giveRemaningDaysInTrial();

    return view('accountView')
                      ->with('fb_url',$fb_login_url)
                      ->with('twitter_url', $twitter_login_url)
                      ->with('google_url', $google_login_url)
                      ->with('linkedin_url', $linkedin_login_url)
                      ->with('pinterest_url', $pinterest_login_url)
      							  ->with('accounts', $accounts)
                      ->with('insta_account_count', $insta_account_count)
                      ->with('bitly_username', $bitly_username)
                      ->with('bitly_enabled', $bitly_enabled)
                      ->with('insta_limit', $insta_limit)
                      ->with('email', $user->email)
											->with('instaEnabled', $isInstaEnabled)
											->with('numOfDays', $numOfDays)
                      ;

	}

  public function getAccounts(){
    $user = Auth::user();
    return SocialAccounts::where('email', $user->email)->get();
  }

	public function getAccountsForAnalytics(){
    $user = Auth::user();
    //return SocialAccounts::where('email', $user->email)->where('social_network_type',0)->where('social_network_type',1)->get();
		$accounts = DB::select("SELECT * from social_accounts where email='".$user->email."' and (social_network_type =0 or social_network_type=1)");
		return $accounts;
	}

  public function connectFacebook(){
  	session_start() ;
    $user = Auth::user();

    $fb = '';
		$fb = new \Facebook\Facebook([
			'app_id' => '574206716073416',
			'app_secret' => 'd4e39beee98f74ae0cbfb2090c57267e',
			'default_graph_version' => 'v3.0'
		]);

    $helper = $fb->getRedirectLoginHelper();
    $permissions = ['manage_pages', 'publish_pages', 'publish_to_groups'];
    return $helper->getLoginUrl('https://www.oneupapp.io/facebookcallback', $permissions);
  }

  public function connectTwitter(){

		$user = Auth::user();

		if($user->email == 'support@monsterfunder.com')
			$credentials = array(
	        'consumer_key' => '4j5OY1vzStOHNZ7ATvM9MDkBy',
	        'consumer_secret' => 'EzHk0bfCNNgdLYYeBFkOOr4k2DKu6NpBQrRm4W9QJsD6GEqHhY'
	    );
		else
	    $credentials = array(
	        'consumer_key' => 'TwSZCN3PnlM6iUAhkDu6Sysmd',
	        'consumer_secret' => 'Miw5WLucTv7qbEqmHaYYgbKMWVTnK1eJ4g5Af5ZH8k7LeEuWSt'
	    );

    $serializer = new ArraySerializer();
    $auth = new SingleUserAuth($credentials, $serializer);

    $url =  url('https://www.oneupapp.io');
    $oauth_callback = $url. "/twitter/callback";
    $params = array(
        'oauth_callback' => $oauth_callback,
    );
    $response = $auth->post('oauth/request_token', $params);
    Session::put('oauth_token',$response['oauth_token']);
    Session::put('oauth_secret',$response['oauth_token_secret']);
    $url = "https://api.twitter.com/oauth/authorize?oauth_token=".$response['oauth_token'];

    return $url;
  }

  public function connectInstagram(Request $request)
  {

    $user = Auth::user();
		$username = $request->input('username');
    $password = $request->input('password');

		$count = SocialAccounts::where('email', $user->email)->where('social_network_type', 2)->count();
		if($count >= $user->insta_limit)
      return "10";

		if($user->email != 'allowloudstore@gmail.com'){
			return $this->connectInstagramV2($username, $password);
		}

		$mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
		$mp->identify($user->email);
		$mp->track("Instagram connect Init", array("network_type" => "Instagram", "username" => $request->input('username')));



    $proxies = array("216.10.1.239:3199", "104.194.153.111:3199", "104.194.145.193:3199","172.86.112.237:3199","104.194.147.120:3199");
    $count = SocialAccounts::where('username', $username)->where('social_network_type', 2)->count();
    if($count > 0)
      return "0";
    $selected_proxy =  $proxies[mt_rand(0,4)];
    \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
    $ig = new \InstagramAPI\Instagram();
    $ig->setProxy($selected_proxy);

    try {
        $ig->login($username, $password);

        $userId = $ig->people->getUserIdForName($username);
        $response = $ig->people->getInfoById($userId);
        $user = new SocialAccounts();
        $user->username = $response->getUser()->getUsername();
        $user->password = $password;
        $user->full_name = $response->getUser()->getFullName();
        $user->social_network_type = 2;
        $user->social_account_id = $response->getUser()->getUsername()."(Instagram)";
        $user->access_token = -1;
        $user->access_token_secret  = -1;
        $user->email = Session::get('email');
        $user->image_url = $response->getUser()->getProfilePicUrl();
        $user->account_type = -1;
        $user->proxy = $selected_proxy;
        $user->save();

				$mp->track("Instagram connected succesfully", array("network_type" => "Instagram", "username" => $username));

        return 1;

    }catch(\InstagramAPI\Exception\InstagramException $e){
        if (preg_match('/checkpoint_required/',$e->getMessage()) || preg_match('/Challenge required/',$e->getMessage()))
         {
                $iresp = $e->getResponse();
                $challenge_path = $iresp->getChallenge()->getApiPath();
                $parts = explode("/", trim($challenge_path, "/"));
                $instagram_id = $parts[1];
                $challenge_id = $parts[2];

                //set session for setting username,password, instagram_id and challengeid
                Session::put("insta_username", $username);
                Session::put("insta_password", $password);
                Session::put("instagram_id", $instagram_id);
                Session::put("challenge_id", $challenge_id);
                Session::put('selected_proxy', $selected_proxy);

								//$mp->track("Instagram requested OTP");

                $ig->sendChallengeVerificationCode($instagram_id,$challenge_id,1,false);
                return "5";
         }
        else{
					$mp->track("Failed to send  OTP.");
          return "2";//incorrect password
				}
        exit(0);
    } catch (\Exception $e) {
			$mp->track("Incorrect password.");
      return "7";
    }
  }

  public function verifyInstagram(Request $request){
    $inputCode = $request->input('code');

		$user = Auth::user();

		$mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
		$mp->identify($user->email);


    \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
    $ig = new \InstagramAPI\Instagram();
    $ig->setProxy(Session::get('selected_proxy'));

    try {
        $ig->setUser(Session::get('insta_username'), Session::get('insta_password'));
        $resp = $ig->approveChallengeVerificationCode(
            Session::get('instagram_id'),
            Session::get('challenge_id'),
            $inputCode);
    } catch (\Exception $e) {
      if (preg_match('/Please check the code we sent you and try again/',$e->getMessage()))
      {
        return "6";
      }else
			  $mp->track("OTP sent but failed to verify.");
        return "7";
    }

    \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
    $ig = new \InstagramAPI\Instagram();
    $ig->setProxy(Session::get('selected_proxy'));
    try{
      $ig->login(Session::get('insta_username'), Session::get('insta_password'));
    }
    catch(\Exception $e){
      return "8";
    }
    $userId = $ig->people->getUserIdForName(Session::get('insta_username'));
    $response = $ig->people->getInfoById($userId);
    $user = new SocialAccounts();
    $user->username = $response->getUser()->getUsername();
    $user->password = Session::get('insta_password');
    $user->full_name = $response->getUser()->getFullName();
    $user->social_network_type = 2;
    $user->social_account_id = $response->getUser()->getUsername();
    $user->access_token = -1;
    $user->access_token_secret  = -1;
    $user->email = Session::get('email');
    $user->image_url = $response->getUser()->getProfilePicUrl();
    $user->account_type = -1;
    $user->proxy = Session::get('selected_proxy');
    $user->save();
		$mp->track("Instagram connected after OTP verification.");
    return 1;

  }

  public function connectInstagramV2($username, $password){
		$insta = new APInsta();
		$insta->setShowErrors(false);
		$sessionId = $insta->login($username, $password, true);
		if($sessionId == false){
			return "7";
		}

		//save users
		$user = new SocialAccounts();
		$user->username = $username;
		$user->password = $password;
		$user->full_name = $username;
		$user->social_network_type = 2;
		$user->social_account_id = $username;
		$user->access_token = $sessionId;
		$user->access_token_secret  = -1;
		$user->email = Session::get('email');
		$user->image_url = "https://www.oneupapp.io/img/user_account.png";
		$user->account_type = -1;
		$user->proxy = '';
		$user->save();
		return "1";
	}

	public function connectInstagramFromExt(Request $request){

		$currentUser = Auth::user();

		$count = SocialAccounts::where('email', $currentUser->email)->where('social_network_type', 2)->count();
		if($count >= $currentUser->insta_limit)
			return "You have connected the maximum number of Instagram accounts allowed on your plan or free trial (Max. 1 Instagram account during the free trial). <br><br> You can upgrade your OneUp plan <a href='/subscribe?upgrade=true'>here.</a>";

		$data = $request->input('data');
		$data = json_decode($data, TRUE);
		$items = $data['items'];

		$user = new SocialAccounts();
		$user->username = $items[0]['username']['name'];
		$user->password = '12345';
		$user->full_name = $items[0]['username']['name'];
		$user->social_network_type = 2;
		$user->social_account_id = $items[0]['username']['name']."_insta";
		$user->access_token = $items[0]['cookie'];
		$user->access_token_secret  = -1;
		$user->email = $currentUser->email;
		$user->image_url = $items[0]['username']['profilePicture'];
		$user->account_type = -1;
		$user->proxy = '';
		$user->save();
		$categories = Category::where('email', $currentUser->email)->get();
		for($m = 0; $m < count($categories); $m++){
			CategoryAccount::where('social_network_id', $items[0]['username']['name']."_insta")->where('category_id', $categories[$m]['id'])->delete();
			$item = new CategoryAccount();
			$item->category_id = $categories[$m]['id'];
			$item->social_network_id = $items[0]['username']['name']."_insta";
			$item->social_network_type = 2;
			$item->social_network_name = $items[0]['username']['name'];
			$item->isPaused = false;
			$item->save();
		}
		return Redirect::to('/accounts');
	}

  public function connectGoogle(){
    //google plus config
    $client = new \Google_Client();
    $client->setClientId("449053730827-f94nijtku0c3bd6lo75qcmp30oa3pjec.apps.googleusercontent.com");
    $client->setClientSecret("2gv63X20ldBNUwB6cs0MW8Ef");
    $client->setRedirectUri("https://www.oneupapp.io/google/callback");
    $client->setAccessType("offline");
    $client->setApprovalPrompt('force');

    $client->addScope("https://www.googleapis.com/auth/plus.pages.manage");
    $client->addScope("https://www.googleapis.com/auth/plus.stream.read");
    $client->addScope("https://www.googleapis.com/auth/plus.stream.write");
    $client->addScope("https://www.googleapis.com/auth/plus.media.readwrite");
    $client->addScope("https://www.googleapis.com/auth/plus.me");
    $client->addScope("https://www.googleapis.com/auth/plus.media.upload");
		$client->addScope("https://www.googleapis.com/auth/plus.business.manage");
    return $client->createAuthUrl();
  }

  public function connectLinkedIn(){
		$client = new Client(
	    '814m0mnb6vx3hn',
	    'azdJMGKolvKYbYJt'
	  );
		$scopes = [
		        Scope::READ_BASIC_PROFILE,
						'rw_organization_admin',
						'w_organization_social',
		        'w_member_social',
		    ];
		$client->setRedirectUrl('https://www.oneupapp.io/linkedin/callback');
		return $client->getLoginUrl($scopes);
    return \LinkedIn::getLoginUrl(array('redirect_uri'=>'http://127.0.0.1:8889/linkedin/callback'));
  }

  public function connectPinterest(Request $request){

		//Mixpanel

		$proxiesServer = ['http://104.197.180.101:3000', 'http://35.192.45.134:3000'];
		$this->sendAnalytics("Pinterest");

     $email = $request->input('email');
     $password = $request->input('password');
      $curl = curl_init();
      curl_setopt_array($curl, array(
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_URL => $proxiesServer[mt_rand(0,1)].'/data?email='.$email.'&password='.$password,
          CURLOPT_USERAGENT => 'Pinnix',
          CURLOPT_SSL_VERIFYPEER=>0,
     ));

     $resp = curl_exec($curl);
     $resp=json_decode($resp,true);
     $access_token=$resp['api']['data']['access_token'];
     if($access_token=="")
     {
          return -1;
     }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'https://api.pinterest.com/v3/users/me/?access_token='.$access_token,
        CURLOPT_USERAGENT => 'Sample',
        CURLOPT_SSL_VERIFYPEER=>0

    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $response=json_decode($response,true);
    $username = $response['data']['username'];
    $full_name= $response['data']['full_name'];
    $image_url= $response['data']['image_large_url'];

   $count = SocialAccounts::where('username', $username)->where('email','!=',Session::get('email'))->where('social_network_type',5)->count();

    if($count>0)
    return 0;

    SocialAccounts::where('username', $username)->where('social_network_type', 5)->delete();

    $user = new SocialAccounts();
    $user->username = $username;
    $user->password = $password;
    $user->full_name = $full_name;
    $user->social_network_type = 5;
    $user->social_account_id = $username;
    $user->access_token = $access_token;
    $user->access_token_secret  = -1;
    $user->email = Session::get('email');
    $user->image_url = $image_url;
    $user->account_type = -1;
    $user->proxy = 0;
    $user->save();

		$categories = Category::where('email', Session::get('email'))->get();
		for($m = 0; $m < count($categories); $m++){
			CategoryAccount::where('social_network_id', $username)->where('category_id', $categories[$m]['id'])->delete();
			$item = new CategoryAccount();
			$item->category_id = $categories[$m]['id'];
			$item->social_network_id = $username;
			$item->social_network_type = 5;
			$item->social_network_name = $username;
			$item->isPaused = false;
			$item->save();
		}
    //now save the board
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'https://api.pinterest.com/v3/users/me/boards/?access_token='.$access_token,
        CURLOPT_USERAGENT => 'Sample',
        CURLOPT_SSL_VERIFYPEER=>0

    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $response=json_decode($response,true);

    $boards_data = $response['data'];
    $boards = array();
    for($i=0;$i<count($boards_data);$i++)
    {
      $board = array('username' => $username, 'board_id' => $boards_data[$i]['id'], 'board_name' => $boards_data[$i]['name']);
      array_push($boards, $board);
    }

    Boards::where('username', $username)->delete();
    Boards::insert($boards);
    return 1;

  }

  public function connectPinterestOfficial(){
    $pinterest = new Pinterest(config('constants.APP_KEY'), config('constants.APP_SECRET'));
    $loginurl = $pinterest->auth->getLoginUrl("https://www.oneupapp.io/pinterestredirect", array('read_public','write_public'));

    return $loginurl;
  }

  public function authBitly(){
			//Mixpanel
			$this->sendAnalytics("Bitly");
      $code = $_REQUEST['code'];

      // Enter your Client ID, Client Secret and Redirect URI
      $client_id ="0c4d3211b844f39859c5a1ac8f02f42b51132cdf";
      $client_secret = "ca83ad9fd7b7285612f68aa21ef9d7473d2af5e1";
      $redirect_uri = "https://www.oneupapp.io/authbitly"; //The redirect_uri is just pointing back to the location of this file

      $uri = "https://api-ssl.bitly.com/oauth/access_token";

      //POST to the bitly authentication endpoint
      $params = array();
      $params['client_id'] = $client_id;
      $params['client_secret'] = $client_secret;
      $params['code'] = $code;
      $params['redirect_uri'] = $redirect_uri;

      $output = "";
      $params_string = "";
      foreach ($params as $key=>$value) { $params_string .= $key.'='.$value.'&'; }
      rtrim($params_string,'&');
      try {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $uri);
        curl_setopt($ch,CURLOPT_POST, count($params));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          $output = curl_exec($ch);
      } catch (Exception $e) {
      }

      parse_str($output);
      $user = Auth::user();
      User::where('email', $user->email)
            ->update(['bitly_username' => $login,
                      'bitly_token'=>$access_token,
                      'enable_bitly' => 1
                    ]);
      return Redirect::to('/accounts');

  }

  public function deleteBitly(){
      $user = Auth::user();
      User::where('email', $user->email)
                 ->update(['bitly_username' => NULL,
                            'bitly_token'=>NULL,
                            'enable_bitly' => 0
                          ]);
      return Redirect::to('/accounts');
  }

  public function switchBitly(){
      $user = Auth::user();
      User::where('email', $user->email)
                 ->update([
                            'enable_bitly' => !$user->enable_bitly
                          ]);
      return Redirect::to('/accounts');
  }


  public function deleteAccount(){
    $account_id = $_GET['id'];

		$this->_deleteAccounts($account_id);
    return Redirect::to('/accounts');
  }

	public function bulkDeleteAccount(Request $request){
		$accounts = $request->input('accounts');

		for($i = 0; $i < count($accounts); $i++){
			$this->_deleteAccounts($accounts[$i]);
		}
	}

	public function _deleteAccounts($account_id){
		$user = Auth::user();
		SocialAccounts::where('email', $user->email)->where('social_account_id', $account_id)->delete();

    //delete all category associated with this account
		DB::delete("delete category_account from category_account left join category on category_account.category_id = category.id where category_account.social_network_id='".$account_id."'");

    //delete all posts of this account
    DB::table('posts')->where('email', $user->email)->where('social_network_id', $account_id)
                                     ->delete();
	}

	public function sendAnalytics($social_network){
		$user = Auth::user();

		$mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
		$mp->identify($user->email);
		$mp->track("User connected an account", array("network_type" => $social_network));
	}

	public function getInstaFeed(){
		\InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
		 $ig = new \InstagramAPI\Instagram();
		 $ig->setProxy('104.194.147.120:3199');

		 try{
			 $ig->login("oneup7533", "Vis9334836674@");

		 }catch(\Exception $e){
			 echo "cannot logij";
			 echo $e->getMessage();
			 return;
		 }
		     $rankToken = \InstagramAPI\Signatures::generateUUID();
		     $response = $ig->timeline->getUserFeed("620846664");
				 echo "<pre>";
		     print_r($response);
				 echo "</pre>";
	}
}

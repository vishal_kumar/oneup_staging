<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class PlanauthController extends Controller
{
    public function checkAccountExpiry(){
    	$user = Auth::user();

    	if($user['subscription_ref'] != NULL)
    		return true;

    	$dt =  Carbon::parse($user['created_at']);

    	$currentDay = Carbon::now();

  		if($dt->diffInDays($currentDay) > 7)
  			return false;
  		else
  		    return true;
    }

    public function giveRemaningDaysInTrial(){
      $user = Auth::user();

      if($user['subscription_ref'] != NULL)
    		return 200;

      $dt =  Carbon::parse($user['created_at']);

      $currentDay = Carbon::now();

      return $dt->diffInDays($currentDay);
    }
}

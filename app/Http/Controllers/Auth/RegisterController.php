<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

use Session;
use Mixpanel;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/firsttime';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $mp = Mixpanel::getInstance("6ccd434f84fcf5c56543b75b547d7a7d");
        $mp->people->set($data['email'], array(
            '$first_name'       => $data['name'],
            '$last_name'        => "",
            '$email'            => $data['email'],
            '$phone'            => "000",
            'ip_address'        => \Request::ip()
          ), $ip = \Request::ip(), $ignore_time = false);
        $mp->identify($data['email']);
        $mp->track("User registered");

        $cat = new Category();
        $cat->email = $data['email'];
        $cat->category_name = "Category 1";
        $cat->color = "#7e3878";
        $cat->save();

        $email = $data['email'];
        $name = $data['name'];


        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'referred_by' => Session::get('referral'),
            'timezone' => $data['timezone'],
            'password' => bcrypt($data['password'])
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Schedule;
use App\ScheduleAccounts;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mixpanel;


class ScheduleController extends Controller
{
    public function scheduleView(){
      $user = Auth::user();

      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
  		$mp->identify($user->email);
  		$mp->track("User visited Accounts page");

      if(app('App\Http\Controllers\PlanauthController')->checkAccountExpiry())
          return view('scheduleView')->with('timezone', $user->timezone);
      else
          return view('subscribeView');
    }

    public function getSchedule(){
        $user = Auth::user();

        $arr = array();
        array_push($arr, DB::select("SELECT s.second_index,
                                                s.first_index,
            									s.id,
            									GROUP_CONCAT(sa.social_network_id) as social_network_id,
                                                GROUP_CONCAT(sc.social_network_type) as social_network_type,
                                                GROUP_CONCAT(sc.image_url) as social_network_img,
            									s.selected_time,
            									s.category_id,
                                                c.category_name,
                                                c.color
            									FROM schedule s
            									inner JOIN schedule_accounts sa
            									on s.id = sa.schedule_id
                                                inner JOIN category c
                                                on s.category_id = c.id
                                                inner JOIN social_accounts sc
                                                on sa.social_network_id = sc.social_account_id
            									where s.email = '".$user->email."'
            								    GROUP BY s.first_index,s.second_index,s.id"));

        $temp =  new \stdClass();
        $temp->schedule = $arr;
        return json_encode($temp);
    }

    public function createNewSchedule(Request $request){
        $user = Auth::user();

        $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
    		$mp->identify($user->email);
    		$mp->track("User created Schedule");


        $category_id = $request->input('category_id');
        $time_selected = $request->input('time_selected');
        $week_day = $request->input('week_day');
        $sdt ='';
        if($week_day == 1)
            $sdt = "2017-09-04";
        if($week_day == 2)
            $sdt = "2017-09-05";
        if($week_day == 3)
            $sdt = "2017-09-06";
        if($week_day == 4)
            $sdt = "2017-09-07";
        if($week_day == 5)
            $sdt = "2017-09-08";
        if($week_day == 6)
            $sdt = "2017-09-09";
        if($week_day == 0)
            $sdt = "2017-09-10";

         if($week_day == 7)
            $sdt = "2017-09-10";


        $timestamp = $sdt." ".$time_selected;
        $dt = Carbon::createFromFormat('Y-m-d H:i', $timestamp, $user->timezone);

        $dt->setTimezone('UTC');
        $time_selected_utc = $dt->format('H:i');

        $week_day_utc =  $dt->format('w');;
        $first_index = $request->input('first_index');
        $second_index = $request->input('second_index');
        $social_network_id = $request->input('social_network_id');

        //check for time slots if it exists
        for($i = 0 ; $i < count($social_network_id);$i++){
            $ts = DB::select("SELECT * from schedule s
                              INNER JOIN schedule_accounts sa
                              on s.id = sa.schedule_id
                              where s.category_id =". $category_id.
                              " and s.first_index=". $first_index.
                              " and s.week_day =". $week_day.
                              " and s.selected_time ='". $time_selected
                              ."' and sa.social_network_id='".$social_network_id[$i].
                              "'");

            if(count($ts) != 0){
                 $obj = new \stdClass();
                 $obj->status = 302;
                 $obj->errorMessage = "Some of Time Slot at selected time already exists. Please check.";
                 return json_encode($obj);
            }
        }

        if($week_day == 7){
            for ($j = 0;$j <7 ;$j++){
                $ts = new Schedule();
                $ts->email = $user->email;
                $ts->category_id = $category_id;
                $ts->week_day = $j;
                $ts->selected_time = $time_selected;
                $ts->first_index = $first_index;
                $ts->second_index = $j;

                if($j == 1)
                    $sdt = "2017-09-04";
                if($j == 2)
                    $sdt = "2017-09-05";
                if($j == 3)
                    $sdt = "2017-09-06";
                if($j == 4)
                    $sdt = "2017-09-07";
                if($j == 5)
                    $sdt = "2017-09-08";
                if($j == 6)
                    $sdt = "2017-09-09";
                if($j == 0)
                    $sdt = "2017-09-10";
                $timestamp = $sdt." ".$time_selected;
                $dt = Carbon::createFromFormat('Y-m-d H:i', $timestamp, $user->timezone);

                $dt->setTimezone('UTC');
                $ts->week_day_utc = $dt->format('w');

                $ts->selected_time_utc = $dt->format('H:i');

                $ts->save();

                for($i = 0; $i< count($social_network_id); $i++){
                    $obj = new ScheduleAccounts();
                    $obj->schedule_id = $ts->id;
                    $obj->social_network_id = $social_network_id[$i];
                    $obj->save();
                }
            }
        }else{
            //start inserting timeslots
            $ts = new Schedule();
            $ts->email = $user->email;
            $ts->category_id = $category_id;
            $ts->week_day = $week_day;
            $ts->week_day_utc = $week_day_utc;
            $ts->first_index = $first_index;
            $ts->second_index = $second_index;
            $ts->selected_time = $time_selected;
            $ts->selected_time_utc = $time_selected_utc;

            $ts->save();

            for($i = 0; $i< count($social_network_id); $i++){
                $obj = new ScheduleAccounts();
                $obj->schedule_id = $ts->id;
                $obj->social_network_id = $social_network_id[$i];
                $obj->save();
            }
       }
    }

    public function editSchedule(Request $request){
        $user = Auth::user();

        $schedule_id = $request->input('schedule_id');
        $category_id = $request->input('category_id');
        $time_selected = $request->input('time_selected');
        $week_day = $request->input('week_day');
        $first_index = $request->input('first_index');
        $second_index = $request->input('second_index');
        $social_network_id = $request->input('social_network_id');

        $sdt ='';
        if($week_day == 1)
            $sdt = "2017-09-04";
        if($week_day == 2)
            $sdt = "2017-09-05";
        if($week_day == 3)
            $sdt = "2017-09-06";
        if($week_day == 4)
            $sdt = "2017-09-07";
        if($week_day == 5)
            $sdt = "2017-09-08";
        if($week_day == 6)
            $sdt = "2017-09-09";
        if($week_day == 0)
            $sdt = "2017-09-10";

        $timestamp = $sdt." ".$time_selected;

        $dt = Carbon::createFromFormat('Y-m-d H:i', $timestamp, $user->timezone);
        $dt->setTimezone('UTC');

        $week_day_utc = $dt->format('w');
        $time_selected_utc = $dt->format('H:i');

        //update parent table
        Schedule::where('id', $schedule_id)
                 ->update([
                    'category_id' => $category_id,
                    'week_day' => $week_day,
                    'week_day_utc'=>$week_day_utc,
                    'first_index' => $first_index,
                    'second_index' => $second_index,
                    'selected_time' => $time_selected,
                    'selected_time_utc' =>$time_selected_utc
                    ]);

        DB::table('schedule_accounts')->where('schedule_id', $schedule_id)
                                      ->delete();

        for($i = 0; $i< count($social_network_id); $i++){
            $obj = new ScheduleAccounts();
            $obj->schedule_id = $schedule_id;
            $obj->social_network_id = $social_network_id[$i];
            $obj->save();
        }
    }

    public function deleteSchedule(Request $request){
        $schedule_id = $request->input('schedule_id');
        DB::table('schedule')->where('id', $schedule_id)
                                     ->delete();

        DB::table('schedule_accounts')->where('schedule_id', $schedule_id)
                                      ->delete();

    }
}

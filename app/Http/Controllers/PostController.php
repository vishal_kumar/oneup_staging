<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use League\Csv\Reader;

use Illuminate\Support\Facades\DB;
use App\Category;
use App\Posts;
use App\PublishedPosts;
use App\SocialAccounts;
use App\User;
use App\Schedule;
use App\Feeds;
use App\FeedAccounts;
use Carbon\Carbon;
use JavaScript;
use SimplePie;
use Mixpanel;
include(app_path() . '/functions/bitly.php');

class PostController extends Controller
{
    public function addSinglePost(){

        if(app('App\Http\Controllers\PlanauthController')->checkAccountExpiry())
        {
            $user = Auth::user();

            $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
        		$mp->identify($user->email);
        		$mp->track("User visited Single Post page");

            $queue_count = Posts::where('email',$user->email)->where('isPublished',0)->count();
            $plan_limit = $user->post_limit;

            $connected_account_count = SocialAccounts::where('email', $user->email)->count();
            $account_limit = $user->account_limit;

            if($queue_count >  $plan_limit)
              return view('limitExceed');
            else if($connected_account_count > $account_limit)
              return view('accountLimitExceeded')->with('accountLimit', $account_limit)->with('user', $user);
            else
              return view('singlePostView');
        }
        else
           return view('subscribeView');
    }

    public function addInstagramStory(){
      if(app('App\Http\Controllers\PlanauthController')->checkAccountExpiry())
      {
          $user = Auth::user();

          $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
          $mp->identify($user->email);
          $mp->track("User visited Instagram Story page");

          $queue_count = Posts::where('email',$user->email)->where('isPublished',0)->count();
          $plan_limit = $user->post_limit;

          if($queue_count >=  $plan_limit)
            return view('limitExceed');

          $connected_account_count = SocialAccounts::where('email', $user->email)->count();
          $account_limit = $user->account_limit;

          if($connected_account_count > $account_limit)
            return view('accountLimitExceeded')->with('accountLimit', $account_limit)->with('user', $user);;

          return view('instagramStoryView');

      }
      else
         return view('subscribeView');
    }

    public function addBulkPost(){
        JavaScript::put([
          'isExtension' => false,
          'extData' => []
        ]);
        if(app('App\Http\Controllers\PlanauthController')->checkAccountExpiry())
        {
            $user = Auth::user();

            $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
        		$mp->identify($user->email);
        		$mp->track("User visited Multiple Post page");

            $queue_count = Posts::where('email',$user->email)->where('isPublished',0)->count();
            $plan_limit = $user->post_limit;

            $connected_account_count = SocialAccounts::where('email', $user->email)->count();
            $account_limit = $user->account_limit;

            if($queue_count >  $plan_limit)
              return view('limitExceed');
            else if($connected_account_count > $account_limit)
              return view('accountLimitExceeded')->with('accountLimit', $account_limit)->with('user', $user);
            else
              return view('bulkPostView');
        }
        else
            return view('subscribeView');
    }

    public function addCsvPost(){
      $user = Auth::user();

      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
      $mp->identify($user->email);
      $mp->track("User visited CSV page");

      return view('csvPostView');
    }

    public function getImageFromExtension(Request $request){
      if(isset($_GET['mode'])){
        $json = $request->input();
        $html = $json['html'];
        $html = json_decode($html, true);
        $posts = $html['posts'];
        $items = [];
        for($i =0; $i < count($posts); $i++){
          $obj = array();
          $obj['description'] = urldecode($posts[$i]['description']);
          $obj['site_url'] = urldecode($posts[$i]['source']);
          $obj['image_url'] = urldecode($posts[$i]['imageUrl']);
          $obj['pinid'] = $posts[$i]['pinid'];
          array_push($items, $obj);
        }
      }else{
        $data = $request->get('data');
        $data = json_decode($data ,true);
        $items = $data['items'];
      }
      $user = Auth::user();

      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
      $mp->identify($user->email);
      $mp->track("User imported from Extension");


      $queue_count = Posts::where('email',$user->email)->where('isPublished',0)->count();
      $plan_limit = $user->post_limit;
      if($queue_count >=  $plan_limit)
        return view('limitExceed');

      $connected_account_count = SocialAccounts::where('email', $user->email)->count();
      $account_limit = $user->account_limit;

      if($connected_account_count > $account_limit)
        return view('accountLimitExceeded')->with('accountLimit', $account_limit)->with('user', $user);

      JavaScript::put([
        'isExtension' => true,
        'extData' => $items
      ]);

      return view('bulkPostView');
    }

    public function getRSS(){
      $user = Auth::user();

      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
      $mp->identify($user->email);
      $mp->track("User visited RSS page");

      return view('rssView');
    }

    public function getRssFeed(Request $request){
      $term = $request->input('term');
      $feed = new SimplePie();
      $feed->set_feed_url($term);
      $feed->set_autodiscovery_level( SIMPLEPIE_LOCATOR_ALL);
      $feed->enable_cache(false);
      $feed->init();
      $feed->handle_content_type();
      //return var_dump($feed->get_items());
      $feeds = array();
      if(count($feeds) == 0){
        $feed = new SimplePie();
        $feed->set_curl_options([CURLOPT_USERAGENT => ""]);
        $feed->set_feed_url($term);
        $feed->set_autodiscovery_level( SIMPLEPIE_LOCATOR_ALL);
        $feed->enable_cache(false);
        $feed->init();
        $feed->handle_content_type();
      }
      foreach($feed->get_items() as  $item){
        $temp = array();
        $temp['title'] = $item->get_title();
        $temp['source_url'] = $item->get_link();
        $temp['posted_on'] = $item->get_date();
        $temp['isSelected'] = true;
        array_push($feeds, $temp);
      }
      return $feeds;
      // for($i = 0; $i < count($feed_to_updated); $i++){
      //   foreach($feed->get_items() as  $item)
      //   {
      //     //check for already added
      //     $feed_count = FeedPost::where("title", $item->get_title())->where("source_url", $item->get_link())->where('primary_email', Session::get('primary_email'))->where('social_network_type', $feed_to_updated[$i]['social_network_type'])->where('social_network_name', $feed_to_updated[$i]['social_network_name'])->count();
      //     if($feed_count != 0)continue;
      //     $post = new FeedPost();
      //     $post->title = $item->get_title();
      //     $post->source_url = $item->get_link();
      //     $post->category_id = $feed_to_updated[$i]['category_id'];
      //     $post->feed_id = $feed_to_updated[$i]['id'];
      //     $post->posted_on = $item->get_date();
      //     $post->is_added = 0;
      //     $post->primary_email = Session::get('primary_email');
      //     $post->social_network_type = $feed_to_updated[$i]['social_network_type'];
      //     $post->social_network_name = $feed_to_updated[$i]['social_network_name'];
      //     $post->save();
      //   }
      // }
    }

    public function getSavedFeeds(){
      $user = Auth::user();

      $obj = new \stdClass();
      $obj->feeds = DB::select('select c.category_name, f.feed_url, f.repeat_mode,f.interval,f.id from feeds f left join category c on f.category_id = c.id  where f.email = "'.$user->email.'"');
      return json_encode($obj);
    }

    public function getScheduledPostById(Request $request){
      $postId = $request->input('postId');
      $isPublishPost = $request->input('isPublishedPosts');
      $post = '';

      if($isPublishPost == "true"){
        $post = PublishedPosts::where('id', $postId)->first();
      }else{
        $post = Posts::where('id', $postId)->first();
      }

      return $post;
    }

    public function addFeed(Request $request){
      $user = Auth::user();
      $obj = new \stdClass();

      //get current feeds added by user
      $connectedRssCount = Feeds::where('email', $user->email)->count();
      if($connectedRssCount >= $user->rss_limit){
        $obj->message = "exceeded";
        return json_encode($obj);
      }

      $data = $request->input('data');
      $accounts =  $request->input('accounts');

      //check feed is valid or not
      $feed = new SimplePie();
      $feed->set_curl_options([CURLOPT_USERAGENT => ""]);
      $feed->set_item_limit(1);
      $feed->set_feed_url($data['url']);
      $feed->enable_order_by_date(true);
      $feed->set_autodiscovery_level( SIMPLEPIE_LOCATOR_ALL);
      $feed->enable_cache(false);
      $feed->init();
      $feed->handle_content_type();

      $feeds_check = array();
      foreach($feed->get_items() as  $item){
        $temp = array();
        $temp['title'] = $item->get_title();
        array_push($feeds_check, $temp);
      }
      if(count($feeds_check)  == 0){
        $obj->message = "error";
        return json_encode($obj);
      }


      $db_feed = new Feeds();
      $db_feed->email = $user->email;
      $db_feed->feed_url = $data['url'];
      $db_feed->interval = $data['feedInterval'];
      $db_feed->repeat_mode = $data['repeatMode'];
      $db_feed->expiry_after = $data['repeatTimes'];
      $db_feed->category_id = $data['categoryId'];

      $db_feed->save();

      for($i = 0; $i < count($accounts); $i++){
        $feed_account = new FeedAccounts();
        $feed_account->feed_id = $db_feed->id;
        $feed_account->social_network_id = $accounts[$i];
        $feed_account->save();
      }

      //fetch feeds
      $highest = array();
      $now = Carbon::now($user->timezone);
      $count = 0;

      foreach($feed->get_items() as  $item){
        $dt = new Carbon($item->get_date(),'UTC');
        array_push($highest, $dt->timestamp);

        $count++;
        if($count > 10) continue;



        for($i = 0; $i < count($accounts); $i++){
          $social_network = SocialAccounts::where('social_account_id', $accounts[$i])->first();
          $post = new Posts();
          $post->email = $user->email;
          $post->category_id = $data['categoryId'];
          $post->content = htmlspecialchars_decode($item->get_title());
          $post->post_type = 0;
          $post->video_url = "NA";
          $post->image_url = $this->extractImageUrl($item->get_content());
          $post->source_url =  $item->get_link();
          $post->board_name = NULL;
          $post->board_id = NULL;
          $post->social_network_type = $social_network['social_network_type'];
          $post->social_network_id = $social_network['social_account_id'];
          $post->social_network_name =$social_network['full_name'];
          $post->ts_gmt = $now->timestamp;
          $post->expiry_date = "NA";
          $post->expiry_after_share = $data['repeatTimes'];
          $post->isEvergreen = $data['repeatTimes']==0?0:1;
          $post->isFixedSchedule  = 1;
          $post->date_time = $now->toDateTimeString();
          $post->isNewPost = 1;
          $post->recycleMode = $data['repeatMode'];
          $post->isRepin = 0;
          $post->pinId = 0;
          $post->save();
        }
        $now->addMinutes($data['feedInterval']);
      }
      $temp = max($highest);
      Feeds::where('id',$db_feed->id)->update(['last_feed_ts' => $temp]);

      $obj->message = "added";
      return json_encode($obj);

    }

    public function extractImageUrl($html){
      $doc = new \DOMDocument();
      @$doc->loadHTML($html);
      $imgs = $doc->getElementsbyTagName('img');
      if($imgs->item(0) != NULL)
        $res = $imgs->item(0)->getAttribute('src');
      else
        $res = "NA";

      return $res;
    }

    public function deleteFeed(Request $request){
      $id = $request->input('feedId');
      DB::table('feeds')->where('id', $id)
                                  ->delete();
    }

    public function getImageFromDiscover(Request $request){
      $user = Auth::user();

      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
      $mp->identify($user->email);
      $mp->track("User imported from Discover page");

      $queue_count = Posts::where('email',$user->email)->where('isPublished',0)->count();
      $plan_limit = $user->post_limit;
      if($queue_count >=  $plan_limit)
        return view('limitExceed');

      $data = $request->get('data');
      $data = json_decode($data ,true);

      JavaScript::put([
        'isExtension' => true,
        'extData' => $data
      ]);

      return view('bulkPostView');
    }

    public function getPublishedPostByUsername(){
      $username = $_GET['username'];

      $data = PublishedPosts::where('content', 'like', '%' .$username. '%')->take(5000)->orderBy('id', 'DESC')->get();

      return view('getPostByUsername')->with('data', $data);

    }

    public function postNow(Request $request){

        $user = Auth::user();
        $data =  $request->input('data');

        $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
        $mp->identify($user->email);
        $mp->track("User scheduled using Post Now", array("post_count" => count($data)));

        for($i = 0; $i<count($data); $i++){
          $src_url_before_utm = $data[$i]['source_url'];
          $new_post = $this->appendUtm($data[$i]);

          $dt = Carbon::now($user->timezone);

          $post = new Posts();
          $post->email = $user->email;
          $post->category_id = $new_post['category_id'];
          $shortUrl = $new_post['social_network_type']==5?$new_post['source_url']:$this->shortenUrl($new_post['source_url']);
          $post->content = str_replace($src_url_before_utm,$shortUrl,$new_post['content']);
          $post->post_type = $new_post['postType'];
          $post->video_url = $new_post['video_url'];
          $post->image_url = $new_post['image_url'];
          $post->source_url = $shortUrl;

          if(isset($new_post['board_name']))
            $post->board_name = $new_post['board_name'];
          else
            $post->board_name = NULL;
          if(isset($new_post['board_id']))
            $post->board_id =$new_post['board_id'];
          else
            $post->board_id = NULL;
          $post->social_network_type = $new_post['social_network_type'];
          $post->social_network_id = $new_post['social_network_id'];
          $post->social_network_name = $new_post['social_network_name'];
          $post->ts_gmt = $dt->timestamp;
          $post->expiry_date = $new_post['expiry_date'];
          $post->expiry_after_share = $new_post['expiry_after_share'];
          $post->isEvergreen = $new_post['isEvergreen'];
          $post->isFixedSchedule  = 1;
          $post->isNewPost = $new_post['isNewPost'];
          $post->recycleMode = $new_post['recycleMode'];
          $post->customFrequencyInterval = isset($new_post['customFrequencyInterval'])?$new_post['customFrequencyInterval']:null;
          $post->customFrequencyMode = isset($new_post['customFrequencyMode'])?$new_post['customFrequencyMode']:null;
          $post->extraParams = isset($new_post['extraParams'])?$new_post['extraParams']:null;
          $post->ctaUrl = isset($new_post['ctaUrl'])?$new_post['ctaUrl']:null;
          $post->date_time = $dt->toDateTimeString();
          $post->save();
        }
    }

    public function schedulePost(Request $request){
        $user = Auth::user();

        $data =  $request->input('data');

        $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
        $mp->identify($user->email);
        $mp->track("User scheduled using Schedule", array("post_count" => count($data)));


        if(count($data) > $user->post_limit){
          return -1;
        }
        for($i = 0; $i<count($data); $i++){
          $src_url_before_utm = $data[$i]['source_url'];

          $new_post = $this->appendUtm($data[$i]);
          $timestamp = $new_post['scheduledDate']." ".$new_post['scheduledTime'];
          $dt = Carbon::createFromFormat('Y-m-d H:i', $timestamp, $user->timezone);

          $post = new Posts();
          $post->email = $user->email;
          $post->category_id = $new_post['category_id'];

          $shortUrl = $new_post['social_network_type']==5?$new_post['source_url']:$this->shortenUrl($new_post['source_url']);
          $post->content = str_replace($src_url_before_utm,$shortUrl,$new_post['content']);
          $post->post_type = $new_post['postType'];
          $post->video_url = $new_post['video_url'];
          $post->image_url = $new_post['image_url'];
          $post->source_url = $shortUrl;
          if(isset($new_post['board_name']))
            $post->board_name = $new_post['board_name'];
          else
            $post->board_name = NULL;
          if(isset($new_post['board_id']))
            $post->board_id =$new_post['board_id'];
          else
            $post->board_id = NULL;
          $post->social_network_type = $new_post['social_network_type'];
          $post->social_network_id = $new_post['social_network_id'];
          $post->social_network_name = $new_post['social_network_name'];
          $post->ts_gmt = $dt->timestamp;
          $post->expiry_date = $new_post['expiry_date'];
          $post->expiry_after_share = $new_post['expiry_after_share'];
          $post->isEvergreen = $new_post['isEvergreen'];
          $post->isFixedSchedule  = 1;
          $post->date_time = $dt->toDateTimeString();
          $post->isNewPost = $new_post['isNewPost'];
          $post->recycleMode = $new_post['recycleMode'];
          $post->customFrequencyInterval = isset($new_post['customFrequencyInterval'])?$new_post['customFrequencyInterval']:null;
          $post->customFrequencyMode = isset($new_post['customFrequencyMode'])?$new_post['customFrequencyMode']:null;
          $post->extraParams = isset($new_post['extraParams'])?$new_post['extraParams']:null;
          $post->ctaUrl = isset($new_post['ctaUrl'])?$new_post['ctaUrl']:null;
          $post->isRepin = $new_post['isRepin'];
          $post->pinId = $new_post['repinId'];
          $post->save();
        }
    }

    public function addPostToQueue(Request $request){
      $user = Auth::user();


      $data =  $request->input('data');

      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
      $mp->identify($user->email);
      $mp->track("User added to Queue", array("post_count" => count($data)));

      if(count($data) > $user->post_limit){
        return -1;
      }
      $failed_post = 0;
      for($i = 0; $i < count($data); $i++){
         $post = $data[$i];
         $post['email'] = $user->email;
         $last_scheduled_post = Posts::where('email', $user->email)
                                    ->where('category_id', $post['category_id'])
                                    ->where('social_network_type', $post['social_network_type'])
                                    ->where('social_network_id', $post['social_network_id'])
                                    ->where('isPublished',0)
                                    ->orderBy('ts_gmt', 'desc')
                                    ->first();

        if(count($last_scheduled_post) == 0) {
            //no post for this queue
           $res = $this->createFirstPost($post);
           if($res == -1) $failed_post++;
        }
        else{
           $res = $this->addThisPost($last_scheduled_post['date_time'], $post);
            if($res == -1) $failed_post++;
        }
      }
      $obj = new \stdClass();
      $obj->error = $failed_post;
      return json_encode($obj);
    }

    public function refreshQueue(Request $request){
      $user = Auth::user();

      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
      $mp->identify($user->email);
      $mp->track("User refreshed Queue");

      //clear existing queue
      $temp_data = Posts::where('email', $user->email)->where('isFixedSchedule', 0)->get()->toArray();
      DB::table('posts')->where('email', $user->email)->where('isFixedSchedule', 0)
                                     ->delete();
      $data =  $request->input('data');
      $failed_post = 0;
      try{
        for($i = 0; $i < count($data); $i++){
           $post = $this->appendUtm($data[$i]);
           $post['email'] = $user->email;
           $last_scheduled_post = Posts::where('email', $user->email)
                                      ->where('category_id', $post['category_id'])
                                      ->where('social_network_type', $post['social_network_type'])
                                      ->where('social_network_id', $post['social_network_id'])
                                      ->where('isEvergreen', 1)
                                      ->orderBy('ts_gmt', 'desc')
                                      ->first();

          if(count($last_scheduled_post) == 0) {
              //no post for this queue
             $res = $this->createFirstPost($post);
             if($res == -1) $failed_post++;
          }
          else{
             $res = $this->addThisPost($last_scheduled_post['date_time'], $post);
              if($res == -1) $failed_post++;
          }
        }
        $obj = new \stdClass();
        $obj->error = $failed_post;
        return json_encode($obj);
      }catch(\Exception $e){
        Posts::insert($temp_data);
      }
    }

    public function processCsv(Request $request){
      $data = $request->input('data');
      $temp_name = public_path()."/csv/".time().".csv";
      file_put_contents($temp_name, fopen($data, 'r'));
      $reader = Reader::createFromPath($temp_name);
      $results = $reader->fetch();
      $posts = array();
      foreach ($results as $row) {
        array_push($posts, $row);
      }

      return $posts;

    }

    /**********************************************Internal Functions *******************************/
    public function createFirstPost($new_post){

        $user = Auth::user();

        $date = new \DateTime("now", new \DateTimeZone($user->timezone) );
        $dt = $date->format('Y-m-d H:i');

        $today = Carbon::createFromFormat('Y-m-d H:i', $dt);

        $day_of_week = $today->dayOfWeek;

        //get the schedule for this account and day of week
        $date_time = $this->getAllTimeSlotsFirstPost($new_post, $today, $day_of_week, $day_of_week);

        if($date_time == -1)
          return $date_time;

        $dt = Carbon::createFromFormat('Y-m-d H:i', $date_time, $user->timezone);

        $post = new Posts();
        $post->email = $user->email;
        $post->category_id = $new_post['category_id'];
        $shortUrl = $new_post['social_network_type']==5?$new_post['source_url']:$this->shortenUrl($new_post['source_url']);
        $post->content = str_replace($new_post['source_url'],$shortUrl,$new_post['content']);
        $post->post_type = $new_post['postType'];
        $post->video_url = $new_post['video_url'];
        $post->image_url = $new_post['image_url'];
        $post->source_url = $shortUrl;
        if(isset($new_post['board_name']))
          $post->board_name = $new_post['board_name'];
        else
          $post->board_name = NULL;
        if(isset($new_post['board_id']))
          $post->board_id =$new_post['board_id'];
        else
          $post->board_id = NULL;
        $post->social_network_type = $new_post['social_network_type'];
        $post->social_network_id = $new_post['social_network_id'];
        $post->social_network_name = $new_post['social_network_name'];
        $post->ts_gmt = $dt->timestamp;
        $post->expiry_date = $new_post['expiry_date'];
        $post->expiry_after_share = $new_post['expiry_after_share'];
        $post->isEvergreen = $new_post['isEvergreen'];
        $post->isFixedSchedule  = 0;
        $post->date_time = $dt->toDateTimeString();
        $post->isRepin = $new_post['isRepin'];
        $post->pinId = $new_post['repinId'];
        $post->save();

    }

    public function addThisPost($date, $new_post){

      $user = User::where('email', $new_post['email'])->first();

      $dt = Carbon::parse($date);
      $date_time = $this->getAllTimeSlots($new_post, $dt, $dt->dayOfWeek, $dt->dayOfWeek);
      if($date_time == -1)
        return $date_time;


      $dt = Carbon::createFromFormat('Y-m-d H:i', $date_time, $user->timezone);


      $post = new Posts();
      $post->email = $user->email;
      $post->category_id = $new_post['category_id'];
      $shortUrl = $new_post['social_network_type']==5?$new_post['source_url']:$this->shortenUrl($new_post['source_url']);
      $post->content = str_replace($new_post['source_url'],$shortUrl,$new_post['content']);
      $post->post_type = $new_post['postType'];
      $post->video_url = $new_post['video_url'];
      $post->image_url = $new_post['image_url'];
      $post->source_url = $shortUrl;
      if(isset($new_post['board_name']))
        $post->board_name = $new_post['board_name'];
      else
        $post->board_name = NULL;
      if(isset($new_post['board_id']))
        $post->board_id =$new_post['board_id'];
      else
        $post->board_id = NULL;
      $post->social_network_type = $new_post['social_network_type'];
      $post->social_network_id = $new_post['social_network_id'];
      $post->social_network_name = $new_post['social_network_name'];
      $post->ts_gmt = $dt->timestamp;
      $post->expiry_date = $new_post['expiry_date'];
      $post->expiry_after_share = $new_post['expiry_after_share'];
      $post->isEvergreen = $new_post['isEvergreen'];
      $post->isFixedSchedule  = 0;
      $post->date_time = $dt->toDateTimeString();
      $post->isRepin = $new_post['isRepin'];
      $post->pinId = $new_post['repinId'];
      $post->save();
    }

    public function getAllTimeSlotsFirstPost($post, $date, $originalDayofWeek, $dayOfWeek){
       $allTimeSlots = DB::select("select * from schedule s inner join schedule_accounts sa
                  on s.id = sa.schedule_id
                  where s.category_id=".$post['category_id']." and
                  s.week_day=".$dayOfWeek." and
                  s.selected_time>'".Carbon::parse($date)->format('H:i')."' and
                  sa.social_network_id='".$post['social_network_id']."' order by s.selected_time asc limit 1");

        $allTimeSlots = json_decode(json_encode($allTimeSlots),true);


        if(count($allTimeSlots)==0){
            $dayOfWeek++;
            $date->addDay();

            if($dayOfWeek == 7){
              $dayOfWeek = 0;
            }
            for($i = $dayOfWeek; ($originalDayofWeek - $dayOfWeek)!=0 ; $i++){
              $allTimeSlots = DB::select("select * from schedule s inner join schedule_accounts sa
                    on s.id = sa.schedule_id
                    where s.category_id=".$post['category_id']." and
                    s.week_day=".$dayOfWeek." and
                    sa.social_network_id='".$post['social_network_id']."' order by s.selected_time asc limit 1");

              $allTimeSlots = json_decode(json_encode($allTimeSlots),true);
              if(count($allTimeSlots) !=0 ) {$selected_time = $allTimeSlots[0]['selected_time'];break;}
              $dayOfWeek++;
              $date->addDay();

              if($dayOfWeek == 7){
                $dayOfWeek = 0;
              }
            }
            //we are out of loop, so look for time on same day
            if(count($allTimeSlots) == 0)
            {
               $sameDaySlot = DB::select("select * from schedule s inner join schedule_accounts sa
                 on s.id = sa.schedule_id
                where s.category_id=".$post['category_id']." and
                s.week_day=".$originalDayofWeek." and
                sa.social_network_id='".$post['social_network_id']."' limit 1");



               $sameDaySlot = json_decode(json_encode($sameDaySlot),true);

                if(count($sameDaySlot) != 0)
                {
                  $selected_time = $sameDaySlot[0]['selected_time'];
                  return $date->toDateString().' '.$selected_time;
                }
                else
                return -1;
            }
            else
            return $date->toDateString().' '.$selected_time;
        }
         $selected_time = $allTimeSlots[0]['selected_time'];
         return $date->toDateString().' '.$selected_time;
    }

    public function getAllTimeSlots($post, $date, $originalDayofWeek, $dayOfWeek){
        $selected_time = '';
        $prev_day_of_week = $dayOfWeek;

        $first_index = $this->getFirstIndex($date, $post['email']);
        $prev_first_index = $this->getFirstIndex($date, $post['email']);

        $allTimeSlots = DB::select("select * from schedule s inner join schedule_accounts sa
                  on s.id = sa.schedule_id
                  where s.category_id=".$post['category_id']." and
                  s.week_day=".$dayOfWeek." and
                  s.selected_time>'".Carbon::parse($date)->format('H:i')."' and
                  sa.social_network_id='".$post['social_network_id']."' order by s.selected_time asc limit 1");


        $allTimeSlots = json_decode(json_encode($allTimeSlots),true);

        if(count($allTimeSlots)==0){

            $first_index = 0;
            $dayOfWeek++;
            $date->addDay();

            if($dayOfWeek >= 7){
              $dayOfWeek = 0;
            }
            for($i = $dayOfWeek; ($originalDayofWeek - $dayOfWeek)!=0 ; $i++){

                 $allSlots = DB::select("select * from schedule where first_index>=".$first_index." and email='".$post['email']."'order by first_index asc");

                 $allSlots = json_decode(json_encode($allSlots), true);

                 for($k = 0; $k < count($allSlots); $k++){
                    $allTimeSlots = DB::select("select * from schedule s inner join schedule_accounts sa
                      on s.id = sa.schedule_id
                      where s.category_id=".$post['category_id']." and
                      s.week_day=".$dayOfWeek." and
                      s.first_index=".$allSlots[$k]['first_index']." and
                      sa.social_network_id='".$post['social_network_id']."' order by s.selected_time asc limit 1");

                    $allTimeSlots = json_decode(json_encode($allTimeSlots),true);

                    if(count($allTimeSlots) !=0 ) {$selected_time = $allTimeSlots[0]['selected_time'];break;}

                    $first_index++;
                }

                if(count($allTimeSlots) !=0 ) {$selected_time = $allTimeSlots[0]['selected_time'];break;}else{$first_index=23;}
                if($first_index >=23)
                {
                  $first_index = 0;
                  $dayOfWeek++;
                  $date->addDay();
                }

                if($dayOfWeek >= 7){
                  $dayOfWeek = 0;
                }
          }
            //we are out of loop, so look for time on same day
          if(count($allTimeSlots) == 0)
          {
             $sameDaySlot = DB::select("select * from schedule s inner join schedule_accounts sa
               on s.id = sa.schedule_id
              where s.category_id=".$post['category_id']." and
              s.week_day=".$originalDayofWeek." and
              s.selected_time>'".Carbon::parse($date)->format('H:i')."' and
              sa.social_network_id='".$post['social_network_id']."' limit 1");

             $sameDaySlot = json_decode(json_encode($sameDaySlot),true);

              if(count($sameDaySlot) != 0)
              {
                $selected_time = $sameDaySlot[0]['selected_time'];
                return $date->toDateString().' '.$selected_time;
              }

             $sameDaySlot = DB::select("select * from schedule s inner join schedule_accounts sa
               on s.id = sa.schedule_id
              where s.category_id=".$post['category_id']." and
              s.week_day=".$originalDayofWeek." and
              sa.social_network_id='".$post['social_network_id']."' limit 1");

             $sameDaySlot = json_decode(json_encode($sameDaySlot),true);
              if(count($sameDaySlot) != 0)
              {
                $selected_time = $sameDaySlot[0]['selected_time'];
                return $date->toDateString().' '.$selected_time;
              }else
              return -1;
          }
          else
            return $date->toDateString().' '.$selected_time;
        }

         $selected_time = $allTimeSlots[0]['selected_time'];
         return $date->toDateString().' '.$selected_time;

    }


    public function getFirstIndex($ts,$email){

      $user = User::where('email', $email)->first();

      $t_date = Carbon::createFromFormat('Y-m-d H:i:s', $ts)->timezone($user->timezone);
      $time_string = $t_date->toTimeString();

      if($time_string >= "00:00" && $time_string <"01:00")
        return 0;
      if($time_string >= "01:00" && $time_string <"02:00")
        return 1;
      if($time_string >= "02:00" && $time_string <"03:00")
        return 2;
      if($time_string >= "03:00" && $time_string <"04:00")
        return 3;
      if($time_string >= "04:00" && $time_string <"05:00")
        return 4;
      if($time_string >= "05:00" && $time_string <"06:00")
        return 5;
      if($time_string >= "06:00" && $time_string <"07:00")
        return 6;
      if($time_string >= "07:00" && $time_string <"08:00")
        return 7;
      if($time_string >= "08:00" && $time_string <"09:00")
        return 8;
      if($time_string >= "09:00" && $time_string <"10:00")
        return 9;
      if($time_string >= "10:00" && $time_string <"11:00")
        return 10;
      if($time_string >= "11:00" && $time_string <"12:00")
        return 11;
      if($time_string >= "12:00" && $time_string <"13:00")
        return 12;
      if($time_string >= "13:00" && $time_string <"14:00")
        return 13;
      if($time_string >= "14:00" && $time_string <"15:00")
        return 14;
      if($time_string >= "15:00" && $time_string <"16:00")
        return 15;
      if($time_string >= "16:00" && $time_string <"17:00")
        return 16;
      if($time_string >= "17:00" && $time_string <"18:00")
        return 17;
      if($time_string >= "18:00" && $time_string <"19:00")
        return 18;
      if($time_string >= "19:00" && $time_string <"20:00")
        return 19;
      if($time_string >= "20:00" && $time_string <"21:00")
        return 20;
      if($time_string >= "21:00" && $time_string <"22:00")
        return 21;
      if($time_string >= "22:00" && $time_string <"23:00")
        return 22;
      if($time_string >= "22:00" && $time_string <"00:00")
        return 23;
    }

    public function appendUtm($post){
      $user = Auth::user();
      if($post['source_url'] == "NA") return $post;

      if($user->account_tracking == 1){
        $account = SocialAccounts::where('email', $user->email)
                                  ->where('social_network_type', $post['social_network_type'])
                                  ->where('social_account_id', $post['social_network_id'])
                                  ->first();

        if($account['utmValue'] != NULL)
          $post['source_url'] =  $post['source_url']."?".$account['utmValue'];
        return $post;
      }
      return $post;
    }
    public function shortenUrl($longUrl){

      $user = Auth::user();
      if($longUrl == "NA") return $longUrl;
      if($user->bitly_username == NULL) return $longUrl;
      if($user->enable_bitly == 0) return $longUrl;

      if (preg_match('#^http#', $longUrl) === 1) {
      }else{
        $longUrl = "http://".$longUrl;
      }

      $params = array();
      $params['access_token'] = $user->bitly_token;
      $params['longUrl'] = $longUrl;
      $params['domain'] = 'bit.ly';
      $results = bitly_get('shorten', $params);
      if($results['status_code'] == 200)
       return $results['data']['url'];
      else
      return $longUrl;
    }
}

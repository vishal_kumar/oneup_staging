<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\SocialAccounts;
use Mixpanel;

class AnalyticsController extends Controller
{
    public function analyticsView(){
    	$user = Auth::user();

      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
      $mp->identify($user->email);
      $mp->track("User visited Analytics page");

    	return view('analyticsView')->with('timezone', $user->timezone);;
    }

    public function googleAnalytics(){
    	return view('googleanalyticsView');
    }

    public function socialAccountUTM(){
      $user = Auth::user();
      return view('socialaccountutmView');
    }

    public function getInitialState(){
    	$user = Auth::user();
    	$obj = new \stdClass();

    	$obj->trackingStatus = $user->account_tracking;

      $social_accounts = SocialAccounts::where('email', $user->email)
                                        ->get();
      for($i = 0; $i < count($social_accounts); $i++){
        if($social_accounts[$i]['utmValue'] == NULL){
          switch ($social_accounts[$i]['social_network_type']) {
            case 0:
              $social_accounts[$i]['utmValue'] = 'utm_source='.$social_accounts[$i]['username'].'&utm_medium=Facebook';
              SocialAccounts::where('social_account_id', $social_accounts[$i]['social_account_id'])->update(['utmValue'=>$social_accounts[$i]['utmValue']]);
              break;

            case 1:
              $social_accounts[$i]['utmValue'] = 'utm_source='.$social_accounts[$i]['username'].'&utm_medium=Twitter';
              SocialAccounts::where('social_account_id', $social_accounts[$i]['social_account_id'])->update(['utmValue'=>$social_accounts[$i]['utmValue']]);
              break;

            case 2:
              $social_accounts[$i]['utmValue'] = 'utm_source='.$social_accounts[$i]['username'].'&utm_medium=Instagram';
              SocialAccounts::where('social_account_id', $social_accounts[$i]['social_account_id'])->update(['utmValue'=>$social_accounts[$i]['utmValue']]);
              break;

            case 3:
              $social_accounts[$i]['utmValue'] = 'utm_source='.$social_accounts[$i]['username'].'&utm_medium=Google';
              SocialAccounts::where('social_account_id', $social_accounts[$i]['social_account_id'])->update(['utmValue'=>$social_accounts[$i]['utmValue']]);
              break;

            case 4:
              $social_accounts[$i]['utmValue'] = 'utm_source='.$social_accounts[$i]['username'].'&utm_medium=LinkedIn';
              SocialAccounts::where('social_account_id', $social_accounts[$i]['social_account_id'])->update(['utmValue'=>$social_accounts[$i]['utmValue']]);
              break;

            case 5:
              $social_accounts[$i]['utmValue'] = 'utm_source='.$social_accounts[$i]['username'].'&utm_medium=Pinterest';
              SocialAccounts::where('social_account_id', $social_accounts[$i]['social_account_id'])->update(['utmValue'=>$social_accounts[$i]['utmValue']]);
              break;

            case 6:
            $social_accounts[$i]['utmValue'] = 'utm_source='.$social_accounts[$i]['username'].'&utm_medium=gmb';
            SocialAccounts::where('social_account_id', $social_accounts[$i]['social_account_id'])->update(['utmValue'=>$social_accounts[$i]['utmValue']]);
            break;

          }
        }
      }
      $obj->socialAccounts = $social_accounts;
    	return json_encode($obj);
    }

    public function toggleTrackingState(){
    	$user = Auth::user();

 		  User::where('email', $user->email)
    	         ->update(['account_tracking' => !$user->account_tracking]);
    }

    public function toggleAccountTracking(){
      $user = Auth::user();
      $currentValue = 0;

      if($user->account_tracking == 0)
        $currentValue = 1;

 		  User::where('email', $user->email)
    	         ->update(['account_tracking' => $currentValue]);
    }

    public function updateTrackingVariable(Request $request){
    	$user = Auth::user();

    	$utmMedium = $request->input('utmMedium');
    	$utmFacebook = $request->input('utmFacebook');
    	$utmTwitter = $request->input('utmTwitter');
    	$utmInstagram = $request->input('utmInstagram');
    	$utmGoogle = $request->input('utmGoogle');
    	$utmLinked = $request->input('utmLinked');
    	$utmPinterest = $request->input('utmPinterest');

 		  User::where('email', $user->email)
    	         ->update(['utm_medium' => $utmMedium,
    	         			'utm_facebook' => $utmFacebook,
    	         			'utm_twitter' => $utmTwitter,
    	         			'utm_instagram' => $utmInstagram,
    	         			'utm_google' => $utmGoogle,
    	         			'utm_linkedin' => $utmLinked,
    	         			'utm_pinterest' => $utmPinterest
    	         	]);

    }

    public function updateAccountTracking(Request $request){
      $user = Auth::user();

      $accounts = $request->input('accounts');

      for($i = 0; $i < count($accounts); $i++){
        SocialAccounts::where('email', $user->email)->where('social_account_id', $accounts[$i]['social_account_id'])
      	         ->update(['utmValue' => str_replace(" ","_",$accounts[$i]['utmValue'])
      	         	]);
      }

    }
}

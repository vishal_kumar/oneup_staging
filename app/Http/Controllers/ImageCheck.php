<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageCheck extends Controller
{
    public function returnValidLink($url){
        if ($this->check_url($url))
          return $url;
        else
           return "https://www.oneupapp.io/img/user_account.png";
    }

    public function check_url($url) {
      $headers = @get_headers( $url);
      $headers = (is_array($headers)) ? implode( "\n ", $headers) : $headers;

      return (bool)preg_match('#^HTTP/.*\s+[(200|301|302)]+\s#i', $headers);
    }
}

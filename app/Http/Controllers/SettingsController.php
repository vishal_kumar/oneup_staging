<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use EddTurtle\DirectUpload\Signature;



class SettingsController extends Controller
{
    public function timezone(){
        $user = Auth::user();
    	return view('timezoneView')->with('timezone', $user->timezone);
    }

    public function updateTimezone(Request $request){
    	$user = Auth::user();
        $new_tz = $request->input('timezone');

    	User::where('email', $user->email)
    	         ->update(['timezone' => $new_tz]);

    }

    public function plan(){
        return view('planView');
    }

    public function price(){
        return view('priceView');
    }

    public function subscribe(){
        return view('subscribeView');
    }

    public function privacy(){
        return view('privacyView');
    }

    public function terms(){
      return view('termsView');
    }

    public function jobs(){
        return view('jobsView');
    }

    public function team(){
      return view('teamView');
    }

    public function getSignature(){
      $opts = array('acl' => 'public-read-write');

    	$upload = new Signature(
    	    "AKIAIXQEFFMOGMVEHBVQ",
    	    "gJxEeSlk0NLBXjkfL6txClE2QY7A8F77D0pLqkqs",
    	    "datasetone",
    	    "ap-south-1",
    	    $opts
    	);

    	return json_encode([
    		'signature' => $upload->getFormInputs(),
    		'postEndpoint' => $upload->getFormUrl()
    	]);
    }
}

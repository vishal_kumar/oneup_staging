<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use JWTAuth;
use App\User;
use App\Boards;
use App\Posts;
use App\SocialAccounts;
use JWTAuthException;

class UserController extends Controller
{
  public function login(Request $request){

      $credentials = $request->only('email', 'password');
      $token = null;
      try {
         if (!$token = JWTAuth::attempt($credentials)) {
          $error = array("code" => 422, "message"=>"invalid_email_or_password");
          return response()->json($error);
         }
      } catch (JWTAuthException $e) {
          $error = array("code" => 500, "message"=>"failed_to_create_token");
          return response()->json($error);
      }
      return response()->json(compact('token'));
  }

  public function getAuthUser(Request $request){
      $user = JWTAuth::toUser($request->token);
      return response()->json(['result' => $user]);
  }

  public function getScheduledPosts(Request $request){
     $user = JWTAuth::toUser($request->token);
     $start = $request->start;
     $obj = new \stdClass();
     $obj->scheduledPosts = DB::select("SELECT *, p.id as post_id, p.image_url as content_image from posts p
                                               inner join category c
                         on p.category_id = c.id
                                               inner join social_accounts sa
                                               on p.social_network_id = sa.social_account_id
                         where p.email='".$user->email."'
                                               and isPublished =0 and isPublished !=1 and isPublished !=3 and isPublished !=10
                                               ORDER BY p.ts_gmt asc LIMIT ".$start.", 50
                       ");


    return json_encode($obj);
  }

  public function getPublishedPosts(Request $request){

    $user = JWTAuth::toUser($request->token);
    $start = $request->start;
    $obj = new \stdClass();

    $obj->publishedPosts = DB::select("SELECT *, p.id as post_id, p.image_url as content_image from posts p
                                              inner join category c
                        on p.category_id = c.id
                                              inner join social_accounts sa
                                              on p.social_network_id = sa.social_account_id
                        where p.email='".$user->email."'
                                              and isPublished =1 and isPublished !=0 and isPublished !=3 and isPublished !=10
                                              ORDER BY p.ts_gmt desc LIMIT ".$start.", 50
                      ");

    return json_encode($obj);
  }

  public function getFailedPosts(Request $request){

    $user = JWTAuth::toUser($request->token);
    $start = $request->start;
    $obj = new \stdClass();

    $obj->failedPosts = DB::select("SELECT *, p.id as post_id, p.image_url as content_image from posts p
                                              inner join category c
                        on p.category_id = c.id
                                              inner join social_accounts sa
                                              on p.social_network_id = sa.social_account_id
                        where p.email='".$user->email."'
                                              and isPublished =3 and isPublished !=0 and isPublished !=1 and isPublished !=10
                                              ORDER BY p.id desc LIMIT ".$start.", 50
                      ");

    return json_encode($obj);
  }

  public function getCategoryAndAccounts(Request $request){
    $user = JWTAuth::toUser($request->token);

    $obj = new \stdClass();
    $obj->categories = DB::select('select c.id, c.category_name, c.color, GROUP_CONCAT(ca.social_network_name) as selected_network,GROUP_CONCAT(ca.social_network_type) as network_type,GROUP_CONCAT(ca.social_network_id) as network_id from category c left join category_account ca on c.id = ca.category_id  where email = "'.$user->email.'" and ca.social_network_type!=5 GROUP BY category_name');
    $accounts = SocialAccounts::where('email',$user->email)->get();

    // for($i = 0 ; $i < count($accounts); $i++){
    //   if($accounts[$i]['social_network_type'] ==5)
    //    $accounts[$i]['boards'] = Boards::select('username AS username','board_name AS text','board_id AS value')->where('username', $accounts[$i]['username'])->get();
    // }
    $obj->accounts = $accounts;
    return json_encode($obj);
  }

  // Post scheduling

  public function postNow(Request $request){

  }

  public function schedulePost(Request $request){
    $user = JWTAuth::toUser($request->token);
    $data =  $request->input('data');

    for($i = 0; $i<count($data); $i++){
      $new_post = $data[$i];

      $timestamp = $new_post['scheduledDate']." ".$new_post['scheduledTime'];
      $dt = Carbon::createFromFormat('Y-m-d H:i', $timestamp, $user->timezone);

      $post = new Posts();
      $post->email = $user->email;
      $post->category_id = $new_post['category_id'];
      $shortUrl = $new_post['source_url'];
      $post->content = str_replace($new_post['source_url'],$shortUrl,$new_post['content']);
      $post->post_type = $new_post['postType'];
      $post->video_url = $new_post['video_url'];
      $post->image_url = $new_post['image_url'];
      $post->source_url = $shortUrl;

      if(isset($new_post['board_name']))
        $post->board_name = $new_post['board_name'];
      else
        $post->board_name = NULL;
      if(isset($new_post['board_id']))
        $post->board_id =$new_post['board_id'];
      else
        $post->board_id = NULL;
      $post->social_network_type = $new_post['social_network_type'];
      $post->social_network_id = $new_post['social_network_id'];
      $post->social_network_name = $new_post['social_network_name'];
      $post->ts_gmt = $dt->timestamp;
      $post->expiry_date = $new_post['expiry_date'];
      $post->expiry_after_share = $new_post['expiry_after_share'];
      $post->isEvergreen = $new_post['isEvergreen'];
      $post->isFixedSchedule  = 1;
      $post->date_time = $dt->toDateTimeString();
      $post->save();
    }
  }


}

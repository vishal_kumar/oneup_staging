<?php

namespace App\Http\Controllers;

include_once public_path().'/../vendor/google/vendor/autoload.php';
include_once public_path().'/../vendor/google/vendor/google/apiclient-services/src/Google/Service/Google_Service_PlusPages.php';
include_once public_path().'/../vendor/google/vendor/google/apiclient-services/src/Google/Service/MyBusiness.php';

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Facebook\Facebook;
use \TwitterOAuth\Auth\SingleUserAuth;
use \TwitterOAuth\Serializer\ArraySerializer;
use \DirkGroenen\Pinterest\Pinterest;
use App\DumpModal;
use LinkedIn\Client;


use Mixpanel;

use App\SocialAccounts;
use App\Boards;
use App\Category;
use App\CategoryAccount;
use Session;

//Profile - type 0
//Pages - tpye 1
//groups type 2

class CallbackController extends Controller
{
    public function facebookCallback(){
    	session_start();
    	$fb = '';

      $fb = new \Facebook\Facebook([
        'app_id' => '574206716073416',
        'app_secret' => 'd4e39beee98f74ae0cbfb2090c57267e',
        'default_graph_version' => 'v2.5'
      ]);

    $accountsFoundArr = [];
		$helper = $fb->getRedirectLoginHelper();

		$_SESSION['FBRLH_state']=$_GET['state'];
		try {
		  $accessToken = $helper->getAccessToken();
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}
		if (isset($accessToken)) {
		  // Logged in!
		   //save this profile in fb accounts

		  // Get all pages managed by user
		  $response = $fb->get('/me/accounts?limit=300&fields=name,access_token,location{city,state,region_id}', $accessToken);
		  $res = json_decode($response->getBody(), true);


      $categories = Category::where('email', Session::get('email'))->get();

		  for($i = 0; $i < count($res['data']); $i++)
		  {
  		   $isExist = SocialAccounts::where('email', Session::get('email'))->where('social_account_id', $res['data'][$i]['id'])->delete();
  			 $user = new SocialAccounts();
         if(isset($res['data'][$i]['location']['city']))
  			    $user->username = str_replace(","," ",$res['data'][$i]['name'])." (".$res['data'][$i]['location']['city'].")";
         else
            $user->username = str_replace(","," ",$res['data'][$i]['name']);

         if(isset($res['data'][$i]['location']['city']))
            $user->full_name = str_replace(","," ",$res['data'][$i]['name'])." (".$res['data'][$i]['location']['city'].")";
         else
  			    $user->full_name = str_replace(","," ",$res['data'][$i]['name']);

        $accountFound = [];
        $accountFound['accountType'] = "Page";
        $accountFound['accountName'] =  $user->full_name;
        $accountFound['social_network_id'] = $res['data'][$i]['id'];
        $accountFound['image_url'] = 'http://graph.facebook.com/'.$res['data'][$i]['id'].'/picture';
        $accountFound['social_network_type'] = 0;
        $accountFound['isAlreadyConnected'] = ($isExist == 0?false:true);
        array_push($accountsFoundArr, $accountFound);

  			 $user->social_network_type = 0;
  			 $user->social_account_id = $res['data'][$i]['id'];
  			 $user->access_token = $res['data'][$i]['access_token'];
  			 $user->email = Session::get('email');
  		   $user->image_url = app('App\Http\Controllers\ImageCheck')->returnValidLink('http://graph.facebook.com/'.$res['data'][$i]['id'].'/picture');
  			 $user->account_type = 1;
  			 $user->save();
         if($isExist == 0){
           for($j = 0; $j < count($categories); $j++){
             CategoryAccount::where('social_network_id', $res['data'][$i]['id'])->where('category_id', $categories[$j]['id'])->delete();
             $item = new CategoryAccount();
           	 $item->category_id = $categories[$j]['id'];
           	 $item->social_network_id = $res['data'][$i]['id'];
           	 $item->social_network_type = 0;
           	 $item->social_network_name = $user->full_name;
           	 $item->isPaused = false;
           	 $item->save();
           }
         }
		   }//end of for

		  //get groups
		  $response = $fb->get('/me/groups?limit=200&admin_only=true', $accessToken);
		  $res = json_decode($response->getBody(), true);
		  for($i = 0; $i < count($res['data']); $i++)
		  {

			   $isExist = SocialAccounts::where('email', Session::get('email'))->where('social_account_id', $res['data'][$i]['id'])->delete();

			   $user = new SocialAccounts();
			   $user->username = str_replace(","," ",$res['data'][$i]['name']);
			   $user->full_name = str_replace(","," ",$res['data'][$i]['name']);
			   $user->social_network_type = 0;
			   $user->social_account_id = $res['data'][$i]['id'];
			   $user->access_token = $accessToken;
			   $user->email = Session::get('email');
		   	 $user->image_url = app('App\Http\Controllers\ImageCheck')->returnValidLink('http://graph.facebook.com/'.$res['data'][$i]['id'].'/picture');
			   $user->account_type = 2;
			   $user->save();
         $accountFound = [];
         $accountFound['accountType'] = "Group";
         $accountFound['accountName'] =  $user->full_name;
         $accountFound['social_network_id'] = $res['data'][$i]['id'];
         $accountFound['image_url'] = 'http://graph.facebook.com/'.$res['data'][$i]['id'].'/picture';
         $accountFound['social_network_type'] = 0;
         $accountFound['isAlreadyConnected'] = ($isExist == 0?false:true);
         array_push($accountsFoundArr, $accountFound);

         if($isExist == 0){
           for($j = 0; $j < count($categories); $j++){
             CategoryAccount::where('social_network_id', $res['data'][$i]['id'])->where('category_id', $categories[$j]['id'])->delete();
             $item = new CategoryAccount();
           	 $item->category_id = $categories[$j]['id'];
           	 $item->social_network_id = $res['data'][$i]['id'];
           	 $item->social_network_type = 0;
           	 $item->social_network_name = str_replace(","," ",$res['data'][$i]['name']);
           	 $item->isPaused = false;
           	 $item->save();
           }
         }
		    }
	    }
      $this->sendAnalytics("Facebook");
	    return view('accountSelectionCheck')->with('accounts',$accountsFoundArr);
    }

    public function twitterCallback(){
	    if(!isset($_GET['oauth_token']))
		   return Redirect::to('/accounts');

      if(Session::get('email') == 'support@monsterfunder.com')
        $credentials = array(
            'consumer_key' => '4j5OY1vzStOHNZ7ATvM9MDkBy',
            'consumer_secret' => 'EzHk0bfCNNgdLYYeBFkOOr4k2DKu6NpBQrRm4W9QJsD6GEqHhY',
            'oauth_token' => $_GET['oauth_token'],
            'oauth_token_secret' => Session::get('oauth_secret')
        );
      else
    		$credentials = array(
    		    'consumer_key' => 'TwSZCN3PnlM6iUAhkDu6Sysmd',
    		    'consumer_secret' => 'Miw5WLucTv7qbEqmHaYYgbKMWVTnK1eJ4g5Af5ZH8k7LeEuWSt',
    		    'oauth_token' => $_GET['oauth_token'],
    		    'oauth_token_secret' => Session::get('oauth_secret')
    		);

  		$serializer = new ArraySerializer();
  		$auth = new SingleUserAuth($credentials, $serializer);

  		$new_access_token = $_GET['oauth_token'];
  		$oath_verifier =  $_GET['oauth_verifier'];
  		$params = array(
  		   'oauth_verifier' => $oath_verifier
  		);

		  $response = $auth->post('oauth/access_token', $params);
	    $name = $response['screen_name'];
	    $access_token = $response['oauth_token'];
	    $access_token_secret = $response['oauth_token_secret'];


	    //now fetch other details
	    $oauth_token = Session::get('active_twitter_access_token');
	    $oauth_token_secret = Session::get('active_twitter_access_secret'); //'DOjrSC84qcSWne6z96m9THqmR6w1sfofp331usKt30SuA'

      if(Session::get('email') == 'support@monsterfunder.com')
        $credentials = array(
            'consumer_key' => '4j5OY1vzStOHNZ7ATvM9MDkBy',
            'consumer_secret' => 'EzHk0bfCNNgdLYYeBFkOOr4k2DKu6NpBQrRm4W9QJsD6GEqHhY',
            'oauth_token' => $oauth_token,
            'oauth_token_secret' => $oauth_token_secret,
        );
      else
    		$credentials = array(
    		    'consumer_key' => 'J6cl7OyCCk366QjCxY7zgbcZA',
    		    'consumer_secret' => '6HvdmHFGXHy7miI0nLrExB61myWYs6dyAmDcKbC7d7aJ3eVT70',
    		    'oauth_token' => $oauth_token,
    		    'oauth_token_secret' => $oauth_token_secret,
    		);


  		$auth = new SingleUserAuth($credentials, new ArraySerializer());

  		$params = array(
  		   'screen_name' => $name
  		);
		  $response = $auth->get('users/show', $params);
	    $full_name = $response['name'];
	    $img_url = $response['profile_image_url'];

		  $isExist = SocialAccounts::where('email', Session::get('email'))->where('username', $name)->where('social_network_type', 1)->delete();

	    $user = new SocialAccounts();
	    $user->username = $name;
	    $user->full_name = $full_name;
	    $user->social_network_type = 1;
	    $user->social_account_id = $name."_twitter";
	    $user->access_token = $access_token;
	    $user->access_token_secret  = $access_token_secret;
	    $user->email = Session::get('email');
   	  $user->image_url = $img_url;
	    $user->account_type = -1;
	    $user->save();
      $categories = Category::where('email', Session::get('email'))->get();
      if($isExist == 0){
        for($j = 0; $j < count($categories); $j++){
          CategoryAccount::where('social_network_id', $name."_twitter")->where('category_id', $categories[$j]['id'])->delete();
          $item = new CategoryAccount();
          $item->category_id = $categories[$j]['id'];
          $item->social_network_id = $name."_twitter";;
          $item->social_network_type = 1;
          $item->social_network_name = $name;
          $item->isPaused = false;
          $item->save();
        }
      }

      $this->sendAnalytics("Twitter");
	    return Redirect::to('/accounts');
    }

    public function googleCallBack(){

      if(!isset($_GET['code'])){
        return "Some error occured while connecting your accounts. Please retry. Make sure you are giving all the required permissions.";
      }
		  $client = new \Google_Client();
		  $client->setClientId("449053730827-f94nijtku0c3bd6lo75qcmp30oa3pjec.apps.googleusercontent.com");
	    $client->setClientSecret("2gv63X20ldBNUwB6cs0MW8Ef");
	    $client->setRedirectUri("https://www.oneupapp.io/google/callback");
	    $client->setAccessType("offline");
      $client->setApprovalPrompt('force');
	  	$token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
	  	$client->setAccessToken($token);
	  	$_SESSION['api-token'] = $token;

      $gmb = new \Google_Service_MyBusiness($client);
      $accountsFoundArr = [];

      $results = $gmb->accounts->listAccounts();
      $temp = $results;
      while($temp['nextPageToken'] != NULL){
        $opt['pageToken'] = $results['nextPageToken'];
        $temp = $gmb->accounts->listAccounts($opt);
        $results = array_merge($results, $temp);
      }
      $locations = $gmb->accounts_locations->listAccountsLocations($results['modelData']['accounts']['0']['name']);

      foreach ($locations as $location) {
        $account_check = SocialAccounts::where('email', Session::get('email'))->where('social_account_id', $location->name)->delete();
	    	$user = new SocialAccounts();

        if(isset($location->address->locality))
	    	  $user->username = str_replace(","," ",$location->locationName)." (".str_replace(","," ", $location->address->locality).")";
        else
          $user->username = str_replace(","," ",$location->locationName);

        if(isset($location->address->locality))
	    	  $user->full_name = str_replace(","," ",$location->locationName)." (".str_replace(","," ", $location->address->locality).")";
        else
          $user->full_name = str_replace(","," ",$location->locationName);
	    	$user->social_account_id = $location->name;
	    	$user->social_network_type = 6;
        $user->image_url = "https://www.oneupapp.io/img/user_account.png";
	    	$user->email = Session::get('email');
	    	$user->access_token = $_SESSION['api-token']['access_token'];
        if(isset($_SESSION['api-token']['refresh_token']))
    		$user->access_token_secret = $_SESSION['api-token']['refresh_token'];
        else
        {
           //get refresh token for this account and save it
           $data = SocialAccounts::where('email', Session::get('email'))->whereNotNull('access_token_secret')->first();
           $user->access_token_secret = $data['access_token_secret'];
        }
	    	$user->save();

        $accountFound = [];
        $accountFound['accountType'] = "Location";
        $accountFound['accountName'] =  $user->full_name;
        $accountFound['social_network_id'] = $location->name;
        $accountFound['image_url'] = "https://www.oneupapp.io/img/user_account.png";
        $accountFound['social_network_type'] = 6;
        $accountFound['isAlreadyConnected'] = ($account_check == 0?false:true);
        array_push($accountsFoundArr, $accountFound);

        if($account_check == 0){
          $categories = Category::where('email', Session::get('email'))->get();
          for($j = 0; $j < count($categories); $j++){
            CategoryAccount::where('social_network_id', $location->name)->where('category_id', $categories[$j]['id'])->delete();
            $item = new CategoryAccount();
            $item->category_id = $categories[$j]['id'];
            $item->social_network_id = $location->name;
            $item->social_network_type = 6;
            if(isset($location->address->locality))
              $item->social_network_name = str_replace(","," ",$location->locationName)." (".str_replace(","," ", $location->address->locality).")";
            else
              $item->social_network_name = str_replace(","," ",$location->locationName);
            $item->isPaused = false;
            $item->save();
          }
        }
      }

      //get location group
      $opt['filter'] = 'type=LOCATION_GROUP';
      $results = $gmb->accounts->listAccounts($opt);
      $temp = $results;
      while($temp['nextPageToken'] != NULL){
        $opt['pageToken'] = $results['nextPageToken'];
        $temp = $gmb->accounts->listAccounts($opt);
        $results = array_merge($results, $temp);
      }

      if(!isset($results['modelData']['accounts'])) {
        $this->sendAnalytics("Google");
        return view('accountSelectionCheck')->with('accounts',$accountsFoundArr);
      }

      for($i = 0; $i < count($results['modelData']['accounts']); $i++){

        $locations = $gmb->accounts_locations->listAccountsLocations($results['modelData']['accounts'][$i]['name']);

        foreach ($locations as $location) {
          $account_check = SocialAccounts::where('email', Session::get('email'))->where('social_account_id', $location->name)->delete();
  	    	$user = new SocialAccounts();

          if(isset($location->address->locality))
  	    	  $user->username = str_replace(","," ",$location->locationName)." (".str_replace(","," ", $location->address->locality).")";
          else
            $user->username = str_replace(","," ",$location->locationName);

          if(isset($location->address->locality))
  	    	  $user->full_name = str_replace(","," ",$location->locationName)." (".str_replace(","," ", $location->address->locality).")";
          else
            $user->full_name = str_replace(","," ",$location->locationName);
  	    	$user->social_account_id = $location->name;
  	    	$user->social_network_type = 6;
          $user->image_url = "https://www.oneupapp.io/img/user_account.png";
  	    	$user->email = Session::get('email');
  	    	$user->access_token = $_SESSION['api-token']['access_token'];
          if(isset($_SESSION['api-token']['refresh_token']))
      		$user->access_token_secret = $_SESSION['api-token']['refresh_token'];
          else
          {
             //get refresh token for this account and save it
             $data = SocialAccounts::where('email', Session::get('email'))->whereNotNull('access_token_secret')->first();
             $user->access_token_secret = $data['access_token_secret'];
          }
  	    	$user->save();

          $accountFound = [];
          $accountFound['accountType'] = "Location";
          $accountFound['accountName'] =  $user->full_name;
          $accountFound['social_network_id'] = $location->name;
          $accountFound['image_url'] = "https://www.oneupapp.io/img/user_account.png";
          $accountFound['social_network_type'] = 6;
          $accountFound['isAlreadyConnected'] = ($account_check == 0?false:true);
          array_push($accountsFoundArr, $accountFound);

          if($account_check == 0){
            $categories = Category::where('email', Session::get('email'))->get();
            for($j = 0; $j < count($categories); $j++){
              CategoryAccount::where('social_network_id', $location->name)->where('category_id', $categories[$j]['id'])->delete();
              $item = new CategoryAccount();
              $item->category_id = $categories[$j]['id'];
              $item->social_network_id = $location->name;
              $item->social_network_type = 6;
              if(isset($location->address->locality))
                $item->social_network_name = str_replace(","," ",$location->locationName)." (".str_replace(","," ", $location->address->locality).")";
              else
                $item->social_network_name = str_replace(","," ",$location->locationName);
              $item->isPaused = false;
              $item->save();
            }
          }
        }
      }

      $this->sendAnalytics("Google");
  		return view('accountSelectionCheck')->with('accounts',$accountsFoundArr);
    }

    public function linkedInCallBack(){
      if (isset($_GET['code'])) {
        $client = new Client(
          '814m0mnb6vx3hn',
          'azdJMGKolvKYbYJt'
        );
        $client->setApiHeaders([
          'Content-Type' => 'application/json',
          'x-li-format' => 'json',
          'X-Restli-Protocol-Version' => '2.0.0', // use protocol v2
        ]);
        $client->setApiRoot('https://api.linkedin.com/v2/');
        $client->setRedirectUrl('https://www.oneupapp.io/linkedin/callback');
        $accessToken = $client->getAccessToken($_GET['code']);
        $pages = $client->get(
          'organizationalEntityAcls?q=roleAssignee&role=ADMINISTRATOR&projection=(elements*(*,organizationalTarget~(localizedName)))'
        );

        $categories = Category::where('email', Session::get('email'))->get();

        for($i = 0; $i < count($pages['elements']); $i++){
          $obj = $pages['elements'][$i];
          $id = substr($obj['organizationalTarget'], (strripos($obj['organizationalTarget'], ":")+1));

          $imgUrl = "NA";

          try{
            $imgAddress = $client->get(
              'organizations/'.$id.'?projection=(logoV2(original~:playableStreams,cropped~:playableStreams,cropInfo))'
            );
            if(count($imgAddress) != 0){
              if(isset($imgAddress['logoV2']['original~']['elements'][0]['identifiers'][0]['identifier']))
                $imgUrl = $imgAddress['logoV2']['original~']['elements'][0]['identifiers'][0]['identifier'];
            }
          }catch(\Exception $e){

          }

          $account_check = SocialAccounts::where('email', Session::get('email'))->where('social_account_id', $obj['organizationalTarget'])->delete();
          $user = new SocialAccounts();
          $user->username = $obj['organizationalTarget~']['localizedName'];
          $user->full_name = $obj['organizationalTarget~']['localizedName'];
          $user->social_account_id = $obj['organizationalTarget'];
          $user->social_network_type = 4;
          $user->account_type = 1;
          $user->image_url = $imgUrl;
          $user->email = Session::get('email');
          $user->access_token = $accessToken;
          $user->save();
          if($account_check == 0){
            for($j = 0; $j < count($categories); $j++){
              CategoryAccount::where('social_network_id', $obj['organizationalTarget'])->where('category_id', $categories[$j]['id'])->delete();
              $item = new CategoryAccount();
              $item->category_id = $categories[$j]['id'];
              $item->social_network_id = $obj['organizationalTarget'];
              $item->social_network_type = 4;
              $item->social_network_name = $obj['organizationalTarget~']['localizedName'];
              $item->isPaused = false;
              $item->save();
            }
          }
        }

        $profile = $client->get(
          'me?projection=(id,firstName, lastName,profilePicture(displayImage~:playableStreams))'
        );

        $firstName = '';
        $lastName = '';

        $temp = $profile['firstName']['localized'];
        foreach($temp as $key => $value){
          $firstName = $value;
        }

        $temp = $profile['lastName']['localized'];
        foreach($temp as $key => $value){
          $lastName = $value;
        }

        $account_check = SocialAccounts::where('email', Session::get('email'))->where('social_account_id', "urn:li:person:".$profile['id'])->delete();

      	$user = new SocialAccounts();
      	$user->username = $firstName ." ". $lastName;
      	$user->full_name = $firstName ." ". $lastName;
      	$user->social_account_id = "urn:li:person:".$profile['id'];
      	$user->social_network_type = 4;
        $user->account_type = 0;
      	$user->image_url = isset($profile['profilePicture']['displayImage~']['elements'][0]['identifiers'][0]['identifier'])?$profile['profilePicture']['displayImage~']['elements'][0]['identifiers'][0]['identifier']:"NA";
  		  $user->email = Session::get('email');
  	    $user->access_token = $accessToken;
  	    $user->save();
        if($account_check == 0){
          for($j = 0; $j < count($categories); $j++){
            CategoryAccount::where('social_network_id', "urn:li:person:".$profile['id'])->where('category_id', $categories[$j]['id'])->delete();
            $item = new CategoryAccount();
            $item->category_id = $categories[$j]['id'];
            $item->social_network_id = "urn:li:person:".$profile['id'];
            $item->social_network_type = 4;
            $item->social_network_name = $firstName ." ". $lastName;
            $item->isPaused = false;
            $item->save();
          }
        }
      }
      $this->sendAnalytics("LinkedIn");
	    return Redirect::to('/accounts');

    }

    public function pinterestCallBack(){
      $pinterest = new Pinterest(config('constants.APP_KEY'), config('constants.APP_SECRET'));
      if(isset($_GET["code"])){
          $token = $pinterest->auth->getOAuthToken($_GET["code"]);
          $pinterest->auth->setOAuthToken($token->access_token);
          $me = $pinterest->users->me();
          echo $me;
      }
    }

    public function sendAnalytics($social_network){

  		$mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
  		$mp->identify(Session::get('email'));
      $event = "User connected ".$social_network." account";
  		$mp->track($event, array("network_type" => $social_network));
  	}
}

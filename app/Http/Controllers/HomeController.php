<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Posts;
use Auth;
use App\User;
use App\TeamMember;
use App\Schedule;
use App\Category;
use Carbon\Carbon;
use Session;
use Mixpanel;
use Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function switchAccount(Request $request){
      $email = $request->input('email');
      $user =  User::where('email', $email)->first();
      Auth::loginUsingId($user->id);
      return redirect('/queue');
    }

    public function loginSuccess(){
      $user = Auth::user();
      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
      $mp->people->set($user->email, array(
          '$first_name'       => $user->name,
          '$last_name'        => "",
          '$email'            => $user->email,
          '$phone'            => "000"
        ), $ip = 0, $ignore_time = false);

      $mp->identify($user->email);
      $mp->track("User Logged In");
      return redirect('/queue');
    }

    public function showLanding(){
      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));

      if (Auth::check()) {
        $user = Auth::user();
        $mp->people->set($user->email, array(
            '$first_name'       => $user->name,
            '$last_name'        => "",
            '$email'            => $user->email,
            '$phone'            => "000"
          ), $ip = 0, $ignore_time = false);

        $mp->identify($user->email);
        $mp->track("User Logged in Via Cookies");

          if(app('App\Http\Controllers\PlanauthController')->checkAccountExpiry())
             return redirect('/queue');
            else
               return view('subscribeView');
      }
      else{
          if(isset($_GET['referral'])){
              $mp->track("Referral New User Visit");
              Session::put("referral",$_GET['referral']);
          }else{
              $mp->track("New User Visit");
              Session::put("referral", "none");
          }
          return view('welcome');
      }
    }

    public function firstTime(){
        return view('firsttime');
    }

    public function getExtension(){
        return view('extensionView');
    }

    public function trackReferral(){
        $user = Auth::user();
        $users_referred = User::where('referred_by', 'allowloud')->get();

        return view('trackReferral')->with('users', $users_referred);
    }

    public function checkPlanExpiry(){

      $users = User::where('subscription_ref', NULL)->where('is_informed', '!=', 3)->get();

      //$users = User::where('email', 'support@oneupapp.io')->get();
      for($i = 0; $i < count($users); $i++){
        $user = $users[$i];
        $dt =  Carbon::parse($user['created_at']);
      	$currentDay = Carbon::now();

        if($dt->diffInDays($currentDay) > 12) {
          if($user['is_informed'] == 0 || $user['is_informed'] == 2){
            $count = $user['is_informed'];
            $count++;
            User::where('email', $user['email'])
                     ->update(['is_informed'=> $count]);

          }else if($user['is_informed'] == 1){
            $count = $user['is_informed'];
            $count++;
            User::where('email', $user['email'])
                     ->update(['is_informed'=> $count]);

            //disable the Posts
            Posts::where('email', $user['email'])->where('isPublished',0)->update(['isPublished'=> 20]);
          }

        }

      }

    }

    public function inviteTeam(Request $request){
      if ($request->session()->has('isAdmin')) {
        return view('inviteTeam')->with('isAuthorised', false);
      }
      $user = Auth::user();

      $teamMembers = TeamMember::where('invitedBy', $user->email)->get();
      return view('inviteTeam')->with('isAuthorised', true)->with('teamMembers', $teamMembers);

    }

    public function addTeamMember(Request $request){
      $email = $request->input('email');

      $count = TeamMember::where('invitedTo', $email)->count();
      if($count > 0) return -1;

      $count = User::where('email', $email)->count();
      if($count > 0) return -1;

      $user = Auth::user();
      $data = [];
      $data['name'] = $user->name;
      $data['hash'] = base64_encode($email);

      //send mail here
      Mail::send('mails/cloudbill',$data, function($message) use($email){
        $message->to($email)->subject('You have been invited to join OneUp');
        $message->from('support@oneupapp.io', 'OneUp App');
      });

      $teamMember = new TeamMember();
      $teamMember->invitedBy = $user->email;
      $teamMember->invitedTo = $email;
      $teamMember->status = "Not Joined";
      $teamMember->save();

      return 1;
    }

    public function removeTeamMember(Request $request){
      $user = Auth::user();

      $email = $request->input('email');
      DB::table('teamMember')->where('invitedBy', $user->email)
                             ->where('invitedTo', $email)
                             ->delete();
    }

    public function teamSignUp(){
      $email = base64_decode($_GET['id']);
      return view('auth/teamRegister')->with('email', $email);
    }

    public function registerTeamMember(Request $request){
      $name = $request->input('name');
      $email = $request->input('email');
      $password = md5($request->input('password'));

      TeamMember::where('invitedTo', $email)->update([
        'name' => $name,
        'password' => $password,
        'status' => 'JOINED'
      ]);

      return redirect('/teammemberlogin');
    }

    public function teamMemberLogin(Request $request){
      $params = $request->all();

      if(count($params) != 0){
        $email = $params['email'];
        $pass = md5($params['password']);

        $user = TeamMember::where('invitedTo', $email)->where('password', $pass)->first();
        if(count($user) >= 1){
          $adminUser =  User::where('email', $user['invitedBy'])->first();
          $request->session()->put('isAdmin', 'false');
          Auth::loginUsingId($adminUser->id);
          return redirect('/queue');
        }
        return view('auth/teamLogin')->with('error', 'These credentials do not match our records.');
      }
      return view('auth/teamLogin');
    }

    public function gmbDetailsView(){
      return view('gmbMoreView');
    }

    public function oneVsMeetView(){
      return view('oneVsMeetView');
    }

    public function oneupUseCase(){
      return view('oneupUseCase');
    }

    public function activateAccount(Request $request){
      $subscription_email = $request->input('SubscriptionReferrer');
      $subscription_ref = $request->input('SubscriptionReference');
      $plan= $request->input('SubscriptionPlan');
      $login_email=$request->input('SubscriptionReferrer');

      $postLimit = 25;
      $instaLimit = 1;
      $accountLimit = 3;
      $rssLimit = 1;

      if( stripos( $plan, 'starter' ) !== false ){
        $postLimit = 150;
      }else if(stripos( $plan, 'individual' ) !== false){
        $postLimit = 1000;
        $instaLimit = 3;
        $accountLimit = 10;
        $rssLimit = 3;
      }else if(stripos( $plan, 'growth' ) !== false){
        $postLimit = 2000;
        $instaLimit = 6;
        $accountLimit = 30;
        $rssLimit = 5;
      }else if(stripos( $plan, 'business' ) !== false){
        $postLimit = 3000;
        $instaLimit = 10;
        $accountLimit = 100;
        $rssLimit = 8;
      }

      User::where('email', $login_email)->update([
        'subscription_ref' => $subscription_ref,
        'plan' => $plan,
        'post_limit' => $postLimit,
        'insta_limit' => $instaLimit,
        'account_limit' => $accountLimit,
        'rss_limit' => $rssLimit
      ]);
      $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
      $mp->identify($login_email);
      $mp->track("User upgraded to plan ". $plan);

      header("HTTP/1.0 404 Not Found");
    }

    public function onlineReader(){

        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL,"'https://apis.voicebase.com/v3/media/4d6185b1-b42c-4a4e-9568-c2a894e72196/transcript");
        // curl_setopt($ch, CURLOPT_POST, 1);

        // $headers = [
        //     'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0ZmU0NzMzNi1kNTU0LTRjMjgtYmNmOC00OGM2YTM1MTIyYjIiLCJ1c2VySWQiOiJhdXRoMHw1YTcwMjQyZDNlNTEyMDZlNWFmNjBmZTgiLCJvcmdhbml6YXRpb25JZCI6ImVlNzMwNTdjLTg0ZTItZTY5ZS03MDcyLWJjNmUyYTMzNDdjMSIsImVwaGVtZXJhbCI6ZmFsc2UsImlhdCI6MTUxNzY2MDU5ODY1NywiaXNzIjoiaHR0cDovL3d3dy52b2ljZWJhc2UuY29tIn0.lY9WlbOqqxf5Pofs1IWbZgXYmvTK3DKTgchnFSw4ci4'
        // ];

        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // $resp = curl_exec($ch);
        // $resp=json_decode($resp,true);
        // return var_dump($resp);

        $data = '{"confidence":0.1301582526352993,"words":[{"p":0,"c":0.983,"s":229,"e":640,"w":"Private"},{"p":1,"c":0.909,"s":650,"e":1040,"w":"equity"},{"p":2,"c":0.981,"s":1050,"e":1530,"w":"today"},{"p":3,"c":0.987,"s":1540,"e":2060,"w":"embraces"},{"p":4,"c":0.917,"s":2070,"e":2120,"w":"a"},{"p":5,"c":0.923,"s":2129,"e":2610,"w":"vast"},{"p":6,"c":0.867,"s":2620,"e":2879,"w":"range"},{"p":7,"c":0.966,"s":2889,"e":2970,"w":"of"},{"p":8,"c":0.982,"s":2980,"e":3520,"w":"investment"},{"p":9,"c":0.98,"s":3530,"e":4190,"w":"strategies"},{"p":10,"c":0.947,"s":4200,"e":4360,"w":"and"},{"p":11,"c":0.836,"s":4370,"e":5100,"w":"regions"},{"p":12,"c":0.167,"s":5779,"e":6310,"w":"L.P.S."},{"p":13,"c":0.957,"s":6319,"e":6529,"w":"can"},{"p":14,"c":0.958,"s":6540,"e":6740,"w":"opt"},{"p":15,"c":0.667,"s":6750,"e":6950,"w":"for"},{"p":16,"c":0.704,"s":6960,"e":7540,"w":"mainstream"},{"p":17,"c":0.418,"s":8000,"e":8400,"w":"funds"},{"p":18,"c":0.857,"s":8410,"e":8570,"w":"or"},{"p":19,"c":0.935,"s":8580,"e":8760,"w":"more"},{"p":20,"c":0.979,"s":8769,"e":9450,"w":"specialized"},{"p":21,"c":0.983,"s":9460,"e":9900,"w":"ones"},{"p":22,"c":0.64,"s":9960,"e":10410,"w":"anywhere"},{"p":23,"c":0.857,"s":10420,"e":10510,"w":"in"},{"p":24,"c":0.83,"s":10519,"e":10590,"w":"the"},{"p":25,"c":0.937,"s":10599,"e":11130,"w":"world"},{"p":26,"c":0.071,"s":11840,"e":11960,"w":"they"},{"p":27,"c":0.944,"s":11969,"e":12130,"w":"can"},{"p":28,"c":0.959,"s":12139,"e":12519,"w":"invest"},{"p":29,"c":0.917,"s":12530,"e":12620,"w":"in"},{"p":30,"c":0.525,"s":12630,"e":12809,"w":"new"},{"p":31,"c":0.842,"s":12820,"e":13360,"w":"funds"},{"p":32,"c":0.879,"s":13400,"e":13549,"w":"or"},{"p":33,"c":0.302,"s":13559,"e":13790,"w":"buy"},{"p":34,"c":0.979,"s":13799,"e":14139,"w":"funds"},{"p":35,"c":0.968,"s":14150,"e":14509,"w":"stakes"},{"p":36,"c":0.9,"s":14519,"e":14620,"w":"on"},{"p":37,"c":0.917,"s":14630,"e":14700,"w":"the"},{"p":38,"c":0.95,"s":14710,"e":15269,"w":"secondary"},{"p":39,"c":0.955,"s":15280,"e":15870,"w":"market"},{"p":40,"c":0.061,"s":16609,"e":16740,"w":"they"},{"p":41,"c":0.938,"s":16750,"e":16930,"w":"can"},{"p":42,"c":0.412,"s":16940,"e":17289,"w":"Cohen"},{"p":43,"c":0.973,"s":17300,"e":17670,"w":"Vesta"},{"p":44,"c":0.96,"s":17680,"e":17949,"w":"long"},{"p":45,"c":0.0,"s":17949,"e":17949,"m":"punc","w":"."},{"p":46,"c":0.946,"s":19609,"e":19800,"w":"And"},{"p":47,"c":0.85,"s":19810,"e":20100,"w":"even"},{"p":48,"c":0.979,"s":20109,"e":20500,"w":"invest"},{"p":49,"c":0.5,"s":20510,"e":21020,"w":"directly"},{"p":50,"c":0.983,"s":21029,"e":21170,"w":"in"},{"p":51,"c":0.736,"s":21180,"e":21480,"w":"private"},{"p":52,"c":0.98,"s":21490,"e":22210,"w":"companies"},{"p":53,"c":0.0,"s":22210,"e":22210,"m":"punc","w":"."}]}';
        $temp = (array) json_decode($data);

        return view('onlineReader')->with('data',$temp['words']);
    }
}

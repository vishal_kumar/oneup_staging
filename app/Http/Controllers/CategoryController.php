<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\CategoryAccount;
use App\SocialAccounts;
use App\Posts;
use App\PublishedPosts;
use App\FailedPosts;
use App\InstaEnabled;
use App\Boards;
use Mixpanel;



class CategoryController extends Controller
{

    public function showCategory(){

        $user = Auth::user();
        $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
        $mp->identify($user->email);
        $mp->track("User visited Category page");

        if(app('App\Http\Controllers\PlanauthController')->checkAccountExpiry())
            return view('categoryView');
        else
            return view('subscribeView');
    }

    public function getCategory(){
        $user = Auth::user();
        $obj = new \stdClass();
        $obj->categories = DB::select('select c.id, c.category_name, c.color, c.isPaused, GROUP_CONCAT(ca.social_network_name) as selected_network,GROUP_CONCAT(ca.social_network_type) as network_type,GROUP_CONCAT(ca.social_network_id) as network_id from category c left join category_account ca on c.id = ca.category_id  where email = "'.$user->email.'" GROUP BY category_name');
        return json_encode($obj);
    }

    public function getCategoryAndAccounts(){
        $user = Auth::user();

        $obj = new \stdClass();
        $obj->categories = DB::select('select c.id, c.category_name, c.color, GROUP_CONCAT(ca.social_network_name) as selected_network,GROUP_CONCAT(ca.social_network_type) as network_type,GROUP_CONCAT(ca.social_network_id) as network_id from category c left join category_account ca on c.id = ca.category_id  where email = "'.$user->email.'" GROUP BY category_name');
        $accounts = SocialAccounts::where('email',$user->email)->get();

        for($i = 0 ; $i < count($accounts); $i++){
          if($accounts[$i]['social_network_type'] ==5)
           $accounts[$i]['boards'] = Boards::select('username AS username','board_name AS text','board_id AS value')->where('username', $accounts[$i]['username'])->get();
        }
        $obj->accounts = $accounts;
        $obj->email = $user->email;
        return json_encode($obj);
    }

    public function getInstagramAccounts(){
      $user = Auth::user();

      $obj = new \stdClass();
      $accounts = SocialAccounts::where('email',$user->email)->where('social_network_type',2)->get();

      $obj->accounts = $accounts;
      $obj->instaEnabled = 1;
      return json_encode($obj);
    }

    public function getPostsCount(Request $request){
        $category_id = $request->input('category_id');

        $obj = new \stdClass();
        $obj->normalPostCount = Posts::where('category_id', $category_id)->where('isEvergreen',0)->where('isPublished',0)->count();
        $obj->evergreenPostCount = Posts::where('category_id', $category_id)->where('isEvergreen',1)->where('isPublished',0)->count();

        return json_encode($obj);
    }

    public function updateCategoryName(Request $request){
        $user = Auth::user();

    	$old_name = $request->input('old_name');
    	$updated_name = $request->input('new_name');

    	Category::where('id', $old_name)->where('email',$user->email)
    	         ->update(['category_name' => $updated_name]);

    }

    public function updateCategoryColor(Request $request){
        $user = Auth::user();

    	$current_name = $request->input('current_name');
    	$new_color = $request->input('new_color');

    	Category::where('category_name', $current_name)->where('email',$user->email)
    	         ->update(['color' => $new_color]);
    }

    public function addSocialAccount(Request $request){
        $user = Auth::user();

    	$category_id = $request->input('category_id');
    	$social_network_id = $request->input('social_network_id');
    	$social_network_name = $request->input('social_network_name');
    	$social_network_type = $request->input('social_network_type');

    	$item = new CategoryAccount();
    	$item->category_id = $category_id;
    	$item->social_network_id = $social_network_id;
    	$item->social_network_type = $social_network_type;
    	$item->social_network_name = $social_network_name;
    	$item->isPaused = false;
    	$item->save();

        $obj = new \stdClass();
        $obj->categories = DB::select('select c.id, c.category_name, c.color, GROUP_CONCAT(ca.social_network_name) as selected_network,GROUP_CONCAT(ca.social_network_type) as network_type,GROUP_CONCAT(ca.social_network_id) as network_id from category c left join category_account ca on c.id = ca.category_id  where email = "'.$user->email.'" GROUP BY category_name');
        return json_encode($obj);


    }

    public function removeSocialAccount(Request $request){
        $user = Auth::user();

    	$category_id = $request->input('category_id');
    	$network_id = $request->input('network_id');

    	DB::table('category_account')->where('category_id', $category_id)
    						 ->where('social_network_id', $network_id)
    						 ->delete();

        $obj = new \stdClass();
        $obj->categories = DB::select('select c.id, c.category_name, c.color, GROUP_CONCAT(ca.social_network_name) as selected_network,GROUP_CONCAT(ca.social_network_type) as network_type,GROUP_CONCAT(ca.social_network_id) as network_id from category c left join category_account ca on c.id = ca.category_id  where email = "'.$user->email.'" GROUP BY category_name');
        return json_encode($obj);
    }

    public function deleteCategory(Request $request){
        $user = Auth::user();

        $category_id = $request->input('category_id');
        DB::table('category')->where('id', $category_id)
                             ->delete();

        DB::table('category_account')->where('category_id', $category_id)
                                     ->delete();

        //Delete all posts with this category
        DB::table('posts')->where('category_id',$category_id)->delete();

        DB::table('published_posts')->where('category_id',$category_id)->delete();
        DB::table('failed_posts')->where('category_id',$category_id)->delete();

        //delete all schedule
        DB::table('schedule')->where('category_id',$category_id)->delete();



        $obj = new \stdClass();
        $obj->categories = DB::select('select c.id, c.category_name, c.color, GROUP_CONCAT(ca.social_network_name) as selected_network,GROUP_CONCAT(ca.social_network_type) as network_type,GROUP_CONCAT(ca.social_network_id) as network_id from category c left join category_account ca on c.id = ca.category_id  where email = "'.$user->email.'" GROUP BY category_name');
        $obj->accounts = SocialAccounts::where('email',$user->email)->get();
        return json_encode($obj);
    }

    public function addCategory(Request $request){
        $user = Auth::user();

        $mp = Mixpanel::getInstance(config('constants.MIXPANEL_KEY'));
        $mp->identify($user->email);
        $mp->track("User created Category");

        $category_name = $request->input('category_name');
        $category_color = $request->input('category_color');

        $cat = new Category();
        $cat->email = $user->email;
        $cat->category_name = $category_name;
        $cat->color = $category_color;
        $cat->save();

        $obj = new \stdClass();
        $obj->categories = DB::select('select c.id, c.category_name, c.color, GROUP_CONCAT(ca.social_network_name) as selected_network,GROUP_CONCAT(ca.social_network_type) as network_type,GROUP_CONCAT(ca.social_network_id) as network_id from category c left join category_account ca on c.id = ca.category_id  where email = "'.$user->email.'" GROUP BY category_name');
        $obj->accounts = SocialAccounts::where('email',$user->email)->get();
        return json_encode($obj);
    }

    public function updateCategoryState(Request $request){
        $category_id = $request->input('category_id');
        $updated_state = $request->input('updated_state');
        if($updated_state == 0){
            Posts::where('category_id', $category_id)->where('isPublished',0)
                                                     ->update(['isPaused' =>0]);
        }else{
            Posts::where('category_id', $category_id)->where('isPublished',0)
                                                     ->update(['isPaused' =>1]);
        }
        Category::where('id', $category_id)
                 ->update(['isPaused' => $updated_state]);
    }
}

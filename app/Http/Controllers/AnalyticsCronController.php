<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PublishedPosts;
use App\SocialAccounts;
use Carbon\Carbon;


use Abraham\TwitterOAuth\TwitterOAuth;


class AnalyticsCronController extends Controller
{
	/********************************* Facebook *****************************************************************************************/
    public function facebookCron(){
    	 $users = PublishedPosts::groupBy('email')->select('email')->get();
    	 for($j=0; $j < count($users); $j++){
	    	$posts = PublishedPosts::where('email',$users[$j]['email'])->where('social_network_type',0)->take(100)->get();

	    	for($i = 0; $i < count($posts);$i++){
	    		$acc = SocialAccounts::where('social_account_id',$posts[$i]['social_network_id'])->first();
	    		$pos = strpos($posts[$i]['media_id'], '_');
				echo $posts[$i]['id']."<br>";
				if($pos){
					$this->makeRequestWithShare($posts[$i]['media_id'],$acc['access_token'] , $posts[$i]['id']);
				}else{
					$this->makeRequestWithoutShare($posts[$i]['media_id'],$acc['access_token'] , $posts[$i]['id']);
				}
	    	}
    	 }
    }

	function makeRequestWithoutShare($media_id, $access_token, $postId){
		$url = 'https://graph.facebook.com/'.$media_id.'?fields=likes.summary(true),comments.summary(true)&access_token='.$access_token;
		$this->makeCurlRequest($url, $postId, false);
		sleep(1);
	}

	function makeRequestWithShare($media_id, $access_token, $postId){
		$url = 'https://graph.facebook.com/'.$media_id.'?fields=likes.summary(true),comments.summary(true),shares&access_token='.$access_token;
		$this->makeCurlRequest($url, $postId, true);
		sleep(1);
	}

	function makeCurlRequest($url, $postId, $isShare){

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    $response  = curl_exec($ch);
	    curl_close($ch);
	    $response=json_decode($response,true);

	    if($isShare){
	    	$like_count = (isset($response['likes']['summary']['total_count'])? $response['likes']['summary']['total_count']:0);
	    	$comment_count = (isset($response['comments']['summary']['total_count'])? $response['comments']['summary']['total_count']:0);
	    	$share_count = (isset($response['shares']['count'])?$response['shares']['count']:0);

            PublishedPosts::where('id', $postId)
                 ->update(['likes' => $like_count, 'comments' => $comment_count, 'share'=>$share_count]);

	    }else{
	    	$like_count = (isset($response['likes']['summary']['total_count'])? $response['likes']['summary']['total_count']:0);
	    	$comment_count = (isset($response['comments']['summary']['total_count'])? $response['comments']['summary']['total_count']:0);

            PublishedPosts::where('id', $postId)
                 ->update(['likes' => $like_count, 'comments' => $comment_count]);

	    }
	}


/****************************************** Pinterest *****************************************************************************************************************/
    public function pinterestCron(){
    	$users = PublishedPosts::groupBy('email')->select('email')->get();
    	for($j=0; $j < count($users); $j++){
	    	$posts = PublishedPosts::where('email',$users[$j]['email'])->where('social_network_type',5)->take(100)->get();

	    	for($i = 0; $i < count($posts);$i++){
	    		$acc = SocialAccounts::where('social_account_id',$posts[$i]['social_network_id'])->where('social_network_type', 5)->first();
	    		$url = "https://api.pinterest.com/v3/pins/".$posts[$i]['media_id']."/?access_token=".$acc['access_token'];
				$this->getPinterestAnalytics($url, $posts[$i]['id']);
	    	}
    	}
    }

    public function getPinterestAnalytics($url, $postId){
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    $response  = curl_exec($ch);
	    curl_close($ch);
	    $response=json_decode($response,true);
	    if($response['status'] == "success")
        	PublishedPosts::where('id', $postId)
                 ->update(['likes' => $response['data']['like_count'], 'comments' =>$response['data']['comment_count'], 'share'=>$response['data']['repin_count']]);
    }


    /*********************************************** TwitterCron **************************************************************************************************/
    public function getTwitterAnalytics(){

      $users = PublishedPosts::groupBy('email')->select('email')->get();
      $ts = Carbon::now('UTC')->timestamp;
      $minTs = $ts - 1296000;
      for($j=0; $j < count($users); $j++){
  	    	$posts = PublishedPosts::where('email',$users[$j]['email'])->where('ts_gmt', '>=', $minTs)->where('social_network_type',1)->take(100)->get();
          for($i = 0; $i < count($posts);$i++){
  	    		$acc = SocialAccounts::where('social_account_id',$posts[$i]['social_network_id'])->first();
            $connection = new TwitterOAuth('TwSZCN3PnlM6iUAhkDu6Sysmd', 'Miw5WLucTv7qbEqmHaYYgbKMWVTnK1eJ4g5Af5ZH8k7LeEuWSt', $acc['access_token'], $acc['access_token_secret']);
            $connection->setTimeouts(90, 90);
            try{
              $statuses = $connection->get("statuses/show/".$posts[$i]['media_id'], []);
              $result = json_decode(json_encode($statuses), true);
              if(isset($result['favorite_count']))
                PublishedPosts::where('id', $posts[$i]['id'])
                     ->update(['likes' => $result['favorite_count'], 'share' =>$result['retweet_count']]);
            }catch(Exception $e){

            }
          }
      }

    }

}

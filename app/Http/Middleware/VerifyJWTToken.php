<?php


namespace App\Http\Middleware;
use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $user = JWTAuth::toUser($request->input('token'));
        }catch (JWTException $e) {
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                $error = array("code" => $e->getStatusCode(), "message"=>"Invalid Token. Login again.");
                return response()->json($error);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                $error = array("code" => $e->getStatusCode(), "message"=>"Invalid Token. Login again.");
                return response()->json($error);
            }else{
                $error = array("code" => $e->getStatusCode(), "message"=>"No token. Access Denied");
                return response()->json($error);
            }
        }
       return $next($request);
    }
}

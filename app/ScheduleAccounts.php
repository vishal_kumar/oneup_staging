<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleAccounts extends Model
{
    protected $table = 'schedule_accounts';
}

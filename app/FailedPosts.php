<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FailedPosts extends Model
{
    protected $table = 'failed_posts';
}

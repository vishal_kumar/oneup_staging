<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublishedPosts extends Model
{
    protected $table = 'published_posts';
}

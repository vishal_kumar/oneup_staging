<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedAccounts extends Model
{
  protected $table = 'feed_accounts';
  public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DumpModal extends Model
{
  protected $table = 'dump';
  public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialAccounts extends Model
{
    protected $table = 'social_accounts';
    public $timestamps = false;
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategoryAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_account', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->text('social_network_id');
            $table->integer('social_network_type');
            $table->string('social_network_name');
            $table->integer('isPaused');
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

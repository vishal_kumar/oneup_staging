<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->integer('category_id');
            $table->text('content')->nullable();
            $table->text('image_url')->nullable();
            $table->text('source_url')->nullable();
            $table->integer('social_network_type');
            $table->string('social_network_id');
            $table->string('ts_gmt')->nullable();
            $table->string('expiry_date')->nullable();
            $table->integer('expiry_after_share')->nullable();
            $table->integer('times_shared')->nullable();
            $table->string('date_time')->nullable();
            $table->integer('isFixedSchedule')->nullable();
            $table->integer('isEvergreen')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

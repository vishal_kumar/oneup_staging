
<link rel="shortcut icon"  href="{{URL::asset('img/favicon.ico')}}">

<nav class="navbar" role="navigation" aria-label="dropdown navigation" style="border-bottom: 1px solid #EAEAEA">
  <div class="navbar-brand">
    <a href="/queue">
      <img src="{{URL::asset('img/logo_final_wide.png')}}" style="height:56px;margin-left:30px;" alt="Welcome to OneUp app" width="112" height="28">
    </a>
    <span class="navbar-burger burger" data-target="navMenu">
      <span></span>
      <span></span>
      <span></span>
    </span>
  </div>
  <div class="navbar-menu">
  	<div class="navbar-start">
      <div class="navbar-item {{Request::is('accounts')?'active':''}}">
        <a href="/accounts" style="color:{{Request::is('accounts')?'#fff':'#4a4a4a'}}">
          Accounts
        </a>
      </div>
      <div class="navbar-item {{Request::is('category')?'active':''}}">
        <a href="/category" style="color:{{Request::is('category')?'#fff':'#4a4a4a'}}">
          Category
        </a>
      </div>
      <!--
      <div class="navbar-item {{Request::is('schedule')?'active':''}}">
        <a href="/schedule" style="color:{{Request::is('schedule')?'#fff':'#4a4a4a'}}">
          Schedule
        </a>
      </div>
    -->
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link  is-active" style="color:#4a4a4a">
          Queue
        </a>
        <div class="navbar-dropdown">
          <a  class="navbar-item {{Request::is('/queue')?'active':''}}" href="/queue">
            Normal View
          </a>
          <a  class="navbar-item {{Request::is('/calendarview')?'active':''}}" href="/calendarview">
            Calendar View
          </a>
        </div>
      </div>
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link  is-active" style="color:#4a4a4a">
          Analytics
        </a>
        <div class="navbar-dropdown">
          <!--
          <a class="navbar-item {{Request::is('/analytics')?'active':''}}" href="/analytics">
            Posts Engagement
          </a>
        -->
          <!--
          <a class="navbar-item" href="/googleanalytics">
            Google Analytics Tracking
          </a>
        -->
          <a class="navbar-item" href="/socialaccountutm">
            UTM Parameters
          </a>
        </a>
      </div>
      </div>
      <div class="navbar-item has-dropdown is-hoverable">
        <a href="/post/addsinglepost" class="navbar-link  is-active schedule" style="background-color:rgb(84,193,180);color:#fff;">
          Schedule Post
        </a>
        <div class="navbar-dropdown">
          <a class="navbar-item {{Request::is('post/addsinglepost')?'active':''}}" href="/post/addsinglepost">
            Schedule a post
          </a>
          <a class="navbar-item {{Request::is('post/addbulkpost')?'active':''}}" href="/post/addbulkpost">
            Bulk upload posts
          </a>
          <hr class="dropdown-divider">
          <a class="navbar-item {{Request::is('/post/addinstagramstory')?'active':''}}" href="/post/addinstagramstory">
            Schedule Instagram Story
          </a>
          <hr class="dropdown-divider">
          <a class="navbar-item {{Request::is('/post/getextension')?'active':''}}" href="/post/getextension">
            Schedule images from any site
          </a>
          <hr class="dropdown-divider">
          <a class="navbar-item {{Request::is('/post/getrss')?'active':''}}" href="/post/getrss">
            Auto-post from RSS feeds
          </a>
          <!--
          <hr class="dropdown-divider">
          <a class="navbar-item {{Request::is('/post/discovercontent')?'active':''}}" href="/post/discovercontent">
            Discover new content
          </a>
        -->
        </div>
      </div>
  	</div>
    <div class="navbar-end" style="margin-right:30px">
      <div class="navbar-item has-dropdown is-hoverable">
        @if(isset($numOfDays) && $numOfDays < 7)
         <a class="navbar-link  is-active" href="">
           @if((7 - $numOfDays) > 1)
           <p style="color:#188EF5">Free Trial: {{7 - $numOfDays}} days left</p>
           @else
           <p style="color:#188EF5">Free Trial: {{7 - $numOfDays}} day left</p>
           @endif
         </a>
         <div class="navbar-dropdown">
           <a class="navbar-item" style="color:black" href="/subscribe?upgrade=true">
             Upgrade Plan
           </a>
         </div>
        @endif
      </div>

    </div>
  	<div class="navbar-end" style="margin-right:30px">
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link  is-active" href="">
          Settings
        </a>
        <div class="navbar-dropdown">
          <a class="navbar-item" href="/plan">
            Current Plan
          </a>
          <a class="navbar-item" target="_blank" href="/subscribe?upgrade=true">
            Upgrade Plan
          </a>
          <a class="navbar-item" href="/inviteteam">
            Invite Team
          </a>
          <a class="navbar-item" href="/timezone">
            Timezone
          </a>
          <a class="navbar-item" href="https://oneup.crisp.help/en-us/article/oneup-video-tutorials-10w1sqz/" target="blank">
            Video Tutorials
          </a>
          <a class="navbar-item" target="_blank" href="/subscribe?upgrade=true">
            Pricing and FAQ
          </a>
          <a class="navbar-item" href="/logout">
            Log Out
          </a>
        </div>
      </div>
  	</div>
  </div>

  <!-- mobile menu -->
  <div id="navMenu" class="navbar-menu navbar-dropdown">
    <div class="navbar-end">
      <a href="/accounts" class="navbar-item ">Accounts</a>
      <a href="/category" class="navbar-item">Category</a>
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link nav-dropdown">
          Queue <i class="fa fa-angle-down" aria-hidden="true"></i>
        </a>

        <div class="navbar-dropdown is-hidden-mobile">
          <a href="/queue"  class="navbar-item">
            Normal View
          </a>
          <hr class="dropdown-divider">
          <a class="navbar-item">
             Calender View (avalaible on desktop)
          </a>
        </div>
      </div>

      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link nav-dropdown">
          Schedule Post <i class="fa fa-angle-down" aria-hidden="true"></i>
        </a>

        <div class="navbar-dropdown is-hidden-mobile">
          <a href="/post/addsinglepost" class="navbar-item">
            Schedule a Post
          </a>
          <hr class="dropdown-divider">
          <a class="navbar-item">
             Bulk upload (avalaible on desktop)
          </a>
          <a class="navbar-item">
             Schedule Instagram Story (avalaible on desktop)
          </a>
          <a class="navbar-item">
             Schedule images from any site (avalaible on desktop)
          </a>
          <a class="navbar-item">
             Auto-post from RSS feeds (avalaible on desktop)
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>


<script>
  (function() {
  var burger = document.querySelector('.burger');
  var nav = document.querySelector('#'+burger.dataset.target);
 
  burger.addEventListener('click', function(){
    burger.classList.toggle('is-active');
    nav.classList.toggle('is-active');
  });

  document.querySelectorAll('.navbar-link').forEach(function(navbarLink){
    navbarLink.addEventListener('click', function(){
      navbarLink.nextElementSibling.classList.toggle('is-hidden-mobile');
    })
  });
})();
</script>


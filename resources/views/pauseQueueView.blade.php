<html>
<head>
	<title>Pause/Resume Queue - OneUp App</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
	
	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">	
</head>
<body>
	@include('layouts.partials.nav');
	<div id="app" class="container" style="margin-top:80px">     
		<pausequeue></pausequeue>	
	</div>
</body>
<script src="{{URL::asset('js/app.js')}}"></script>
</html>

<html>
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-78962458-1');
  </script>

  <title>OneUp - Privacy Policy</title>
  <link rel="stylesheet" type="text/css" href="css/semantic.css">
  <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
  <meta name="viewport" content="width=device-width, initial-scale=1" />

</head>
<body>

  <div class="ui grid">
      <div class="three wide column" style="height:80px; background-color:#188EF5;border-bottom: 2px solid #c8c8c8">
          <a href="/">
            <img class="ui circular image" src="img/oneup.jpeg" style="width:55px; height:55px;margin-top:5px;margin-left:15%">
          </a>
      </div>
      <div class="thirteen wide column" style="height:80px; background-color:#188EF5;border-bottom: 2px solid #c8c8c8">
          <a href="/"><p style="float:right;margin-right:2%; color:#fff;line-height:60px;">Go Back</p></a>
      </div>
  </div>
  <div class="ui grid centered">
     <div class="fifteen wide column" style="min-height:700px">
          <h1>Privacy Policy</h1>
          <p style="font-size:1.2em">1. The terms OneUp or "we" or "us" or "our" or any grammatical variations of the preceding words refer to OneUp Inc. The terms "you" or "your" refer to the client/customer or to users of any of the interfaces (including but not limited to websites and emails) provided by OneUp. </p>

          <p style="font-size:1.2em">2. OneUp’s Privacy Policy shall apply to the information obtained via our website(s), through emails, through information obtained through third parties and shall govern our behavior related to all services offered by us. But this document shall not curtail OneUp’s right to publish privacy policies specifically corresponding to one or more services. </p>

          <p style="font-size:1.2em">3. Queries regarding our Privacy Policy can be sent through any mode of communication specified by us in any of our interfaces. </p>

          <h2>Information Collection and Usage</h2>
          <p style="font-size:1.2em">1. Information provided by you. You shall be required to give certain information while accessing our website, communicating with us through other means, making use of our services. Owing to the nature of our service it shall be mandatory on your part to provide some specific details. Not providing us with certain information might act to your detriment. We might choose not to provide you our offering in case you fail or refuse to provide us with the information we need. The information includes but is not limited to your name, your email id(s), phone number(s), user id for other websites and corresponding passwords, credit card details and any other information required to provide you with our assistance efficiently. </p>

          <p style="font-size:1.2em">2. Automatic Information Collection. OneUp or third party service providers might collect some information every time you make use of any of our websites. This information might be collected without your prior permission. We might collect certain information, commonly known as Traffic Data and Information of the visitors of our web pages which include inter alia the IP address, Operating System and browser type of your device being used for communication. We might also save data including the phone numbers or email ids used by you to communicate with us. On certain occasions we might use JavaScript or similar software to collect information about your usage of our websites such as the duration of the session, response times etc. when you access our web pages. We might choose to take appropriate measures to receive information regarding the delivery of our e-mails or information regarding when you accessed our e- mail(s). </p>

          <h2>Security of Information</h2>
          <p style="font-size:1.2em">OneUp makes use of sound practices like data encryption, installing firewall etc to ensure that customer information is not misused or accessed without authority. Steps shall be undertaken by OneUp to ensure that customer information is not altered or destroyed to the detriment of its clients. OneUp shall not be held liable for loss of data or any unauthorized access to the client information stored by us. </p>

          <h2>Other websites and Servers</h2>
          <p style="font-size:1.2em"> Third party web sites may use certain technology to send the content of our web site, to process payments for the purchase of services on our web site etc. In addition, third parties may host the servers that deliver our hosted services to our clients. They may automatically receive your IP address when you visit our website or communicate with us through other means. We do not have access to or control over cookies or other features that third parties may have, and the information practices of these third party web sites or servers are not covered by this Privacy Policy.</p>

          <h2>Changes to the Privacy Policy</h2>
          <p style="font-size:1.2em"> We might make suitable amendments to its Privacy Policy. We shall endeavor to make our users/clients aware of these amendments by. It is expected that users of our web pages or our clients read our Privacy Policy regularly to make themselves aware of any amendments made to our Privacy Policy. OneUp shall not be liable for any delay in the in publication of the amended Privacy Policy. </p>
      </div>
  </div>

</body>
</html>

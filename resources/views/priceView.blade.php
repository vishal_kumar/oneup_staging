<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>OneUp: Pricing</title>
    <meta name="description" content="Start your free trial today." />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link
      rel="shortcut icon"
      type="image/png"
      href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"
    />
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script
      async
      src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"
    ></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());

      gtag('config', 'UA-78962458-1');
    </script>
    <script type="text/javascript">
      window.$crisp = [];
      window.CRISP_WEBSITE_ID = 'ea53bead-ca28-42ee-9b50-908fe4f414cb';
      (function() {
        d = document;
        s = d.createElement('script');
        s.src = 'https://client.crisp.chat/l.js';
        s.async = 1;
        d.getElementsByTagName('head')[0].appendChild(s);
      })();
    </script>

    <!-- Hotjar Tracking Code for https://www.oneupapp.io -->
    <script>
      (function(h, o, t, j, a, r) {
        h.hj =
          h.hj ||
          function() {
            (h.hj.q = h.hj.q || []).push(arguments);
          };
        h._hjSettings = { hjid: 959054, hjsv: 6 };
        a = o.getElementsByTagName('head')[0];
        r = o.createElement('script');
        r.async = 1;
        r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
        a.appendChild(r);
      })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>

    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/semantic.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/responsive_pricing.css')}}" />

    <!-- Google Tag Manager -->
    <script>
      (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s),
          dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-5T76958');
    </script>
    <!-- End Google Tag Manager -->

    <script src="js/jquery.min.js"></script>
    <script>
      const monthly = [8, 15, 30, 99];
      const yearly = [4, 10, 20, 79];

      function setAnnual(e) {
        document.querySelector('.monthly').classList.remove('blue')
        e.currentTarget.classList.add('blue');
        document.querySelectorAll('.value').forEach(function(e, i) {
          e.innerText = yearly[i];
        })
      }
      function setMonthly(e) {
        document.querySelector('.annual').classList.remove('blue')
        e.currentTarget.classList.add('blue');
        document.querySelectorAll('.value').forEach(function(e, i) {
          e.innerText = monthly[i];
        })
      }
      document.addEventListener('DOMContentLoaded', function () {
        document.querySelector('.annual').addEventListener('click', setAnnual);
        document.querySelector('.monthly').addEventListener('click', setMonthly);
      });
    </script>
  </head>
  <body>
    <noscript
      ><iframe
        src="https://www.googletagmanager.com/ns.html?id=GTM-5T76958"
        height="0"
        width="0"
        style="display:none;visibility:hidden"
      ></iframe
    ></noscript>
    <div class="header">
      <a href="/">
        <img
          class="header-image"
          src="{{URL::asset('img/oneup.jpeg')}}"
        />
      </a>

      <a class="back" href="/">
        Go Back
      </a>
    </div>


    <div class="title">
      <h1>Grow Like Never Before With OneUp</h1>
      <p class="lead"">
        7 day FREE trial. No credit card required. Cancel anytime.
      </p>
    </div>


    <div class="ui grid centered">
      <div class="tweleve wide column" style="text-align: center">
        <div class="ui large buttons">
          <button class="ui button blue annual">Annual</button>
          <div class="or"></div>
          <button class="ui button monthly">Monthly</button>
        </div>
        <br /><br />
        <p style="font-size: 15px;">Save BIG with Annual plans.</p>
      </div>
    </div>

    <!-- Annual block -->
    <div class="annual_block">
      <div class="pricing-column">
        <div class="pricing-column-lead">
          <h3>STARTER</h3>
          <h1>$<span class="value">4</span> / mo</h1>
          <a href="/register"
            ><button class="ui button blue">Try For Free</button></a
          >
        </div>

        <div class="pricing-section">
          <p>3 social profiles</p>
          <b>150 Scheduled Posts</b>
        </div>

        <div class="pricing-section pricing-section-list">
          <p>
            Facebook, Instagram, Twitter, Pinterest, LinkedIn, and Google My Business
            <br /><br />Maximum of 1 Instagram account
          </p>
          <p>Set posts to repeat automatically</p>
          <p>Chrome extension for easy image scheduling</p>
          <p>Bulk uploading</p>
          <p>Import posts via CSV file</p>
          <p>Automate posts from RSS feeds (1 feed max)</p>
        </div>
      </div>

      <div class="pricing-column">
        <div class="pricing-column-lead">
          <h3>INDIVIDUAL</h3>
          <h1>$<span class="value">10</span> / mo</h1>
          <a href="/register"
            ><button class="ui button blue">Try For Free</button></a
          >
        </div>

        <div class="pricing-section">
          <p>1 additional team member</p>
          <p>10 social profiles</p>
          <b>1000 Scheduled Posts</b>
        </div>

        <div class="pricing-section pricing-section-list">
          <p>
            Facebook, Instagram, Twitter, Pinterest, LinkedIn, and Google My Business
            <br /><br />Maximum of 3 Instagram accounts
          </p>
          <p>Set posts to repeat automatically</p>
          <p>Chrome extension for easy image scheduling</p>
          <p>Bulk uploading</p>
          <p>Import posts via CSV file</p>
          <p>Automate posts from RSS feeds (3 feeds max)</p>
        </div>
      </div>

      <div class="pricing-column">
        <div class="pricing-column-lead">
          <h3>GROWTH</h3>
          <h1>$<span class="value">20</span> / mo</h1>
          <a href="/register"
            ><button class="ui button blue">Try For Free</button></a
          >
        </div>

        <div class="pricing-section">
          <p>3 additional team members</p>
          <p>30 social profiles</p>
          <b>2000 Scheduled Posts</b>
        </div>

        <div class="pricing-section pricing-section-list">
          <p>
            Facebook, Instagram, Twitter, Pinterest, LinkedIn, and Google My Business
            <br /><br />Maximum of 6 Instagram Accounts
          </p>
          <p>Set posts to repeat automatically</p>
          <p>Chrome extension for easy image scheduling</p>
          <p style="font-size:18px; text-align:left;">Bulk uploading</p>
          <p style="font-size:18px; text-align:left;">
            Import posts via CSV file
          </p>
          <p style="font-size:18px; text-align:left;">Automate posts from RSS feeds (5 feeds max)</p>
        </div>
      </div>
    </div>

    <div class="ui grid centered">
      <div style="width:80%;height:1px;background-color:#000"></div>
      <div style="width:80%;min-height:70px;display:flex; align-items:center; justify-content:center;margin-top:10px;margin-bottom:10px">
        <p><b>Need more? Try our Business plan - $<span class="value">79</span>/month</b> | 100 social profiles | Unlimited scheduled posts | 10 Instagram accounts | 4 additional users <a href="/register"
          style="margin-left:10px"><button class="ui button blue">Try For Free</button></a
        ></p>
      </div>
      <div style="width:80%;height:1px;background-color:#000;"></div>
    </div>

    </div>
    <div class="convinced" style="margin-top:25px">
      <div class="title">
        <h1>Still not convinced?</h1>
        <p class="lead">
          <b
            >Check out how OneUp compares to the other social scheduling
            tools.</b
          >
        </p>
      </div>
      <table id="comparetable" class="blueshine">
        <tr>
          <td class="blank"></td>
          <th>OneUp</th>
          <th>Buffer</th>
          <th>MeetEdgar</th>
        </tr>

        <tr>
          <td class="rowTitle" style="font-size:16px">
            Schedule posts to be recycled at specific time intervals
          </td>

          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td><img src="{{URL::asset('img/addRedX.png')}}" alt="icon" /></td>
          <td><img src="{{URL::asset('img/addRedX.png')}}" alt="icon" /></td>
        </tr>
        <tr>
          <td class="rowTitle" style="font-size:16px">
            Facebook, Twitter, & LinkedIn
          </td>

          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
        </tr>
        <tr>
          <td class="rowTitle" style="font-size:16px">Instagram</td>

          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td><img src="{{URL::asset('img/addRedX.png')}}" alt="icon" /></td>
        </tr>
        <tr>
          <td class="rowTitle" style="font-size:16px">Pinterest</td>

          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td>Only on Buffer’s $15/month and higher plans</td>
          <td><img src="{{URL::asset('img/addRedX.png')}}" alt="icon" /></td>
        </tr>
        <tr>
          <td class="rowTitle" style="font-size:16px">Google My Business</td>

          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td>
            <img src="{{URL::asset('img/addRedX.png')}}" alt="icon" />
          </td>
          <td><img src="{{URL::asset('img/addRedX.png')}}" alt="icon" /></td>
        </tr>
        <tr>
          <td class="rowTitle" style="font-size:16px">
            Ability to create a custom posting schedule
          </td>

          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td><img src="{{URL::asset('img/addRedX.png')}}" alt="icon" /></td>
        </tr>
        <tr>
          <td class="rowTitle" style="font-size:16px">
            Chrome browser extension for easy scheduling while browsing the
            web
          </td>

          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
        </tr>
        <tr>
          <td class="rowTitle" style="font-size:16px">
            Ability to sort updates into categories
          </td>

          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td><img src="{{URL::asset('img/addRedX.png')}}" alt="icon" /></td>
          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
        </tr>
        <tr>
          <td class="rowTitle" style="font-size:16px">Emoji support</td>

          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td>
            <img src="{{URL::asset('img/addcheck.png')}}" alt="icon" />
          </td>
          <td><img src="{{URL::asset('img/addRedX.png')}}" alt="icon" /></td>
        </tr>
      </table>
    </div>



    <div class="faq">
      <h1>Frequently Asked Questions</h1>
      <div class="eight wide column" style="text-align:left">
        <p style="font-size:22px; color:#323a44;">
          Do you support Instagram and Instagram Stories?
        </p>
        <p style="font-size:18px; color:#5b6675"><b>Yes, OneUp supports Instagram and Instagram Stories with direct publishing. No need for an app or push notifications.</b></p>

        <br /><br />
        <p style="font-size:22px; color:#323a44;">
          Does OneUp support scheduled posts to Google My Business?
        </p>
        <p style="font-size:18px; color:#5b6675"><b>Yes. You can also add Call-To-Action buttons in your Google My Business posts when scheduling.</b></p>

        <br /><br />
        <p style="font-size:22px; color:#323a44;">
          Can OneUp schedule posts to Facebook Groups?
        </p>
        <p style="font-size:18px; color:#5b6675"><b>Yes, but only if you are the admin of that group.</b></p>

        <br /><br />

        <p style="font-size:22px; color:#323a44;">
          What does "number of scheduled posts" mean?
        </p>
        <p style="font-size:18px; color:#5b6675">
          <b
            >Depending on your plan, you'll be able to schedule a certain number
            of posts in advance. This is not a weekly or monthly limit, it's the
            number of posts you can store in your queue at any one time.<br /><br />
            Let’s say you are on the Starter Plan and already have 150 posts in
            your queue. You will not have the option to add any more posts.
            However, as soon as one of your posts gets published, you will be
            able to top your queue back up to 150.</b
          >
        </p>

        <br /><br />
        <p style="font-size:22px; color:#323a44;">
          What counts as a social profile?
        </p>
        <p style="font-size:18px; color:#5b6675">
          <b>Each Facebook Page, Facebook Group, LinkedIn Profile, LinkedIn Page, Twitter account, Pinterest account, Instagram account, and Google My Business location that are connected to OneUp count as 1 social profile.
            <br><br>
            For example, if you connected 3 Facebook Pages and 2 Twitter accounts, and a Google My Business account with 5 locations, that would count as 10 total social profiles. Each Google My Business location counts as a social profile, even if all of them are connected to the same GMB account.</b>
        </p>

        <br /><br />

        <p style="font-size:22px; color:#323a44;">
          Do you offer any enterprise plan?
        </p>
        <p style="font-size:18px; color:#5b6675">
          <b
            >Yes, if you need more than 100 accounts, 10 Instagram accounts, or 4 team members, contact us in the chat or at
            <span style="color:#563c7d">support@oneupapp.io</span> for a custom
            plan.
          </b>
        </p>

        <br /><br />
        <p style="font-size:22px; color:#323a44;">
          What currency will I be billed in?
        </p>
        <p style="font-size:18px; color:#5b6675">
          <b
            >OneUp bills in USD. For foreign transactions, the currency
            conversion will happen after the charge and will be performed by
            your credit card provider.</b
          >
        </p>

        <br /><br />

        <p style="font-size:22px; color:#323a44;">
          Do you offer a free trial?
        </p>
        <p style="font-size:18px; color:#5b6675">
          <b
            >All of our paid plans include a 7 day free trial - no credit card
            necessary.</b
          >
        </p>

        <br /><br />

        <p style="font-size:22px; color:#323a44;">
          What's your refund policy?
        </p>
        <p style="font-size:18px; color:#5b6675">
          <b
            >Click
            <a
              href="https://oneup.crisp.help/en-us/article/what-is-oneups-cancellation-and-refund-policy-1a5a79f/?1555419261656"
              target="blank"
              >here</a
            >
            to learn about our refund policy.</b
          >
        </p>
      </div>
    </div>
  </body>
</html>

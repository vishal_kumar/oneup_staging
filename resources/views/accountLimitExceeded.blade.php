<html>
<head>
	<title>Please upgrade your limit - OneUp App</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>

	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
.navbar { position: fixed !important; top: 0; left: 0; right: 0; z-index: 1030;}
.active{background: #188EF5;color:#fff;}
#mypanel{
    width:50px;
    height:50px;
    border:1px solid #EAEAEA;
    margin-left:15px;
    background-position:center;
    background-size: 100%;
    cursor: pointer;
    margin-bottom: 20px;
}
#sn-icon{
    display:block;
    float:right;
    width:20px;
    height:20px;
    margin-top: -10px;
    margin-right: -10px;
    background-position:center;
    background-size: 100%;
}
</style>
<body>
	@include('layouts.partials.nav');
    <p style="margin-top:100px;margin-left:20px">You have exceeded the maximum social account limit allowed under your plan.<br><a href="https://www.oneupapp.io/subscribe?upgrade=true">Click here to upgrade your account.</a>

		@if($user->plan != NULL)
    	<p style="margin-top:50px;margin-left:20px;font-size:14px;color:#696969">*Current Account Limit: {{$accountLimit}}</p>
		@else
			<p style="margin-top:50px;margin-left:20px;font-size:14px;color:#696969">*Free Trial Account Limit: {{$accountLimit}}</p>
		@endif

</body>

</html>

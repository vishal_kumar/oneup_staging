<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-78962458-1');
	</script>
		
	<title>Create your Schedule - OneUp App</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>

	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
.navbar { position: fixed !important; top: 0; left: 0; right: 0; z-index: 1030;}
.active{background: #188EF5;color:#fff;}
#mypanel{
    width:50px;
    height:50px;
    border:1px solid #EAEAEA;
    margin-left:15px;
    background-position:center;
    background-size: 100%;
    cursor: pointer;
    margin-bottom :20px;
}
#sn-icon{
    display:block;
    float:right;
    width:24px;
    height:24px;
    margin-top: -10px;
    margin-right: -10px;
    background-position:center;
    background-size: 100%;
}
#smallpanel{
    float: left;
    width:30px;
    height:30px;
    border:1px solid #EAEAEA;
    margin-left:10px;
    background-position:center;
    background-size: 100%;
    cursor: pointer;
    margin-top:7px;
    margin-bottom: 7px;

}
#sn-icon-small{
    float:right;
    width:16px;
    height:16px;
    margin-top: -10px;
    margin-right: -10px;
    background-position:center;
    background-size: 100%;
}
</style>
<body>
	@include('layouts.partials.nav');
	<div id="app" class="container" style="margin-top:80px">
	   <schedule usertz="{{$timezone}}"></schedule>
    </div>
</body>
<script src="{{URL::asset('js/app.js')}}"></script>
</html>

<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-78962458-1');
	</script>
	<!-- Hotjar Tracking Code for https://www.oneupapp.io -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:959054,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

	<title>Get Chrome Extension - OneUp App</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
.navbar { position: fixed !important; top: 0; left: 0; right: 0; z-index: 1030;}
.active{background: #188EF5;color:#fff;}
</style>

<body>
	@include('layouts.partials.nav');
	<div class="container" style="margin-top:80px">
	  <div class="columns">
        <div class="column is-12" style="min-height:200px;text-align:center">
           <img src="{{URL::asset('img/chrome.png')}}" style="width:128px;height:128px"/>
           <p style="margin-top:20px;font-size:20px">Best <b>Chrome Extension</b> in the market.</p>
           <a href="https://chrome.google.com/webstore/detail/oneup/nfihappcoloadkogdfpoaeefhikofiib?hl=en" target="blank" class="button is-info  is-medium" style="margin-top:40px;background-color:#46B2F3">Get Chrome Extension</a>
           <a style="color:grey"><p class="start_tutorial" style="margin-top:10px" style="cursor: pointer;">See how it works!</p></a>
        </div>
	  </div>
      <div class="columns" style="text-align:center;margin-top:20px;border-top:1px solid #eee">
          <div class="column is-4">
            <img src="{{URL::asset('img/video-icon.png')}}" style="width:100px"/>
            <p>Pick images from any site in an instant - and schedule them in bulk! </p>
          </div>
          <div class="column is-4">
            <img src="{{URL::asset('img/pin-icon.png')}}" style="width:100px"/>
            <p>Schedule pins directly from Pinterest with a single click. </p>
          </div>
          <div class="column is-4">
            <img src="{{URL::asset('img/camera-icon.png')}}" style="width:100px"/>
            <p> Schedule videos from Youtube, Vimeo and Dailymotion. </p>
          </div>
       </div>
	</div>
	<div class="modal">
	  <div class="modal-background"></div>
	  <div class="modal-card">
	    <header class="modal-card-head">
	      <p class="modal-card-title"></p>
	      <button class="delete" aria-label="close"></button>
	    </header>
	    <section class="modal-card-body">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/ZI9LMd1g7jM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	    </section>
	  </div>
	</div>
</body>
<script src="{{URL::asset('js/app.js?v=1.2.6')}}"></script>
<script type="text/javascript">
	$(document).ready(function(e){
		$(".start_tutorial").click(function(e){
			$(".modal").addClass("is-active");
		});

		$(".delete").click(function(e){
			$(".modal").removeClass("is-active");
		});
	});
</script>

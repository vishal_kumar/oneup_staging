
<head>
  <script>!function(t) {
      var e,
      o=t.url,
      n=document.getElementsByTagName("head")[0],
      a=document.createElement("script");

      a.type="text/javascript",
      a.async=!0,
      a.src=o,
      e=function() {
          ONPHstart(t);
      }
      ,
      a.readyState?a.onreadystatechange=function() {
          "loaded"!=a.readyState&&"complete"!=a.readyState||(a.onreadystatechange=null, e())
      }
      :a.onload=function() {
          e()
      }
      ,
      n.appendChild(a)
  }

  ( {
      on_title: "Welcome to Healthy Advice", on_description: "Health Is Our Greatest Wealth", link: "http://pinterest.com/pin/create/button/?url=https://www.oneupapp.io&media=https://res.cloudinary.com/dgkqns6fw/image/upload/v1538037410/sqxkt6ebqyhzhibypdml.jpg&description=mytest", button_text: "JOIN", position: "top-right", design: "kitty", display_from: true, from_title: "Hello Hunter!", from_description: "You have a special discount: 20% off. Just use the code below at checkout.", from_discount: "HUNTER20", url: "{{URL::asset('js/modal.js')}}"
  }

  );
  </script>
</head>

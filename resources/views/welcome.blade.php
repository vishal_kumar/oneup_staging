<html>
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script
      async
      src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"
    ></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'UA-78962458-1');
    </script>

    <script type="text/javascript">
      window.$crisp = [];
      window.CRISP_WEBSITE_ID = 'ea53bead-ca28-42ee-9b50-908fe4f414cb';
      (function() {
        d = document;
        s = d.createElement('script');
        s.src = 'https://client.crisp.chat/l.js';
        s.async = 1;
        d.getElementsByTagName('head')[0].appendChild(s);
      })();
    </script>

    <!-- Hotjar Tracking Code for https://www.oneupapp.io -->

    <script>
      (function(h, o, t, j, a, r) {
        h.hj =
          h.hj ||
          function() {
            (h.hj.q = h.hj.q || []).push(arguments);
          };
        h._hjSettings = { hjid: 959054, hjsv: 6 };
        a = o.getElementsByTagName('head')[0];
        r = o.createElement('script');
        r.async = 1;
        r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
        a.appendChild(r);
      })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>

    <title>OneUp: Schedule your Google My Business and social media posts</title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta
      name="description"
      content="Schedule and automatically repeat your posts on Facebook, Twitter, Instagram, Google My Business, Pinterest, and LinkedIn. Schedule images with our Chrome extension, bulk upload post, and auto-post via RSS feeds"
    />

    <link
      rel="shortcut icon"
      type="image/png"
      href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"
    />

    <link rel="stylesheet" href="{{URL::asset('css/app_landing.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('css/responsive_landing.css')}}" />

    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    />

    <meta property="og:image" content="https://www.oneupapp.io/img/hero.png" />
    <meta name="twitter:image" content="https://www.oneupapp.io/img/hero.png">
    <meta
      property="og:image:secure_url"
      content="https://www.oneupapp.io/img/hero.png"
    />

    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.oneupapp.io" />
    <meta
      property="og:title"
      content="OneUp: Schedule your Google My Business and social media posts"
    />
    <meta
      property="og:description"
      content="Schedule and automatically repeat your posts on Facebook, Twitter, Instagram, Google My Business, Pinterest, and LinkedIn. Schedule images with our Chrome extension, bulk upload post, and auto-post via RSS feeds"
    />
    <meta property="og:site_name" content="OneUp" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:domain" value="https://www.oneupapp.io" />
    <meta
      name="twitter:title"
      value="OneUp: Schedule your Google My Business and social media posts"
    />
    <meta
      name="twitter:description"
      value="Schedule and automatically repeat your posts on Facebook, Twitter, Instagram, Google My Business, Pinterest, and LinkedIn. Schedule images with our Chrome extension, bulk upload post, and auto-post via RSS feeds"
    />
    <meta name="twitter:image" content="https://www.oneupapp.io/img/hero.png" />
    <meta name="twitter:url" value="https://www.oneupapp.io" />
    <!-- Google Tag Manager -->

    <script>
      (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s),
          dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-5T76958');
    </script>

    <!-- End Google Tag Manager -->
  </head>
  <body style="background-color:#188EF5;">
    <!-- Google Tag Manager (noscript) -->
    <noscript
      ><iframe
        src="https://www.googletagmanager.com/ns.html?id=GTM-5T76958"
        height="0"
        width="0"
        style="display:none;visibility:hidden"
      ></iframe
    ></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="hero">
      <div class="left">
        <a class="brand" href="/">OneUp</a>
        <h1 class="brand-title">
          Schedule and automatically recycle your social media posts.
        </h1>
        <p class="lead">
          Get more traffic by automatically repeating your social media and Google My Business posts.
        </p>
        <div class="ctas">
          <div class="cta-btns">
            <a href="/register" class="button is-warning hero-cta"
              >TRY IT NOW FOR FREE</a
            >
            <a href="/login"
              ><p
                style="font-size:15px;color:#fff;opacity:0.8;margin-top:10px;margin-left:40px;"
              >
                Login to your account
              </p></a
            >
          </div>
          <div class="hero-img-small"><img src="{{URL::asset('img/hero.png')}}" /></div>
        </div>
      </div>
      <div class="right"><img class="hero-img" src="{{URL::asset('img/hero.png')}}" /></div>
    </div>
    <section class="section simple-section">
      <div class="simple-lead-text">
        <h3>It's really simple</h3>
        <ul class="simple-list">
          <li>Add your social media post</li>
          <li>Select the interval and frequency of automatic recycling</li>
          <li>Post it now or schedule for the future</li>
        </ul>
        <img src="img/recycle.gif" class="simple-img section-image" />
      </div>

      <div class="social-icons">
        <img src="{{URL::asset('img/social-icon-facebook-min.png')}}" />
        <img src="{{URL::asset('img/social-icon-twitter-min.png')}}" />
        <img src="{{URL::asset('img/social-icon-instagram-min.png')}}" />
        <img src="{{URL::asset('img/social-icon-linkedin-min.png')}}" />
        <img src="{{URL::asset('img/gmb_test.png')}}" style="width:32px;height:32px;border-radius:32px"/>
        <img src="{{URL::asset('img/social-icon-pinterest-min.png')}}" />
      </div>
    </section>

    <section class="section section-light organize-section">
      <h3>Organize Posts into Categories</h3>
      <p>
        OneUp allows you to create as many categories as you’d like in order to
        organize your library of posts. Customize your schedule based on when
        you’d like to share different types of content.
      </p>
      <img src="{{URL::asset('img/cate.png')}}" class="section-image" />
    </section>

    <section
      class="section section-light boosted-section"
      style="background-color:rgb(245,245,245)"
    >
      <h3>Boosted Calendar View</h3>
      <p>Manage and edit your scheduled posts via your calendar.</p>
      <img src="{{URL::asset('img/calendar.gif')}}" class="boosted-image section-image" />
    </section>

    <section
      class="section section-light section-measure"
      style="background-color:#fff"
    >
      <h3>Schedule Google My Business posts to stand out in search results</h3>
      <p>
        OneUp allows you to schedule and automate your Google My Business posts. Add images, links, and Call-To-Action buttons to your Google My Business posts.
      </p>
      <img src="{{URL::asset('img/gmb_animated.gif')}}" class="section-image-custom" />
    </section>

    <img class="slide-img" src="{{URL::asset('img/background-blue-min.jpg')}}" />

    <section
      class="section section-dark features-section"
      style="background-color:#178EEA"
    >
      <h3>Even more great features</h3>
      <p>
        Here are even more ways OneUp can help in supercharging your social
        media efforts —all from a single dashboard.
      </p>
      <ul class="features-list">
        <li>
          <img src="{{URL::asset('img/upload-26.png')}}" style="width:30px;height:30px" />

          <p style="font-size:1em;color:#fff;margin-top:12px;">Bulk Upload</p>
          <p
            style="font-size:0.875em;color:#fff;opacity:0.8;line-height:1.75em;margin-top:0.5em"
          >
            Effortlessly upload and schedule multiple posts at once - plan for
            an entire week or even an entire month in just a few minutes.
          </p>
        </li>
        <li>
          <img
            src="{{URL::asset('img/features-icon-video-min.png')}}"
            style="width:40px;height:40px"
          />

          <p style="font-size:1em;color:#fff;margin-top:12px;">
            Video & GIF Uploader
          </p>
          <p
            style="font-size:0.875em;color:#fff;opacity:0.8;line-height:1.75em;margin-top:0.5em"
          >
            Natively add video and GIFs to your posts.
          </p>
        </li>

        <li>
          <img src="{{URL::asset('img/features_category.png')}}" style="width:40px;height:40px" />

          <p style="font-size:1em;color:#fff;margin-top:12px;">
            Categorize your content
          </p>
          <p
            style="font-size:0.875em;color:#fff;opacity:0.8;line-height:1.75em;margin-top:0.5em"
          >
            Ditch the spreadsheets and content calendar templates. Categories
            let you manage your different types of content with ease.
          </p>
        </li>

        <li>
          <img
            src="{{URL::asset('img/features-icon-calendar-min.png')}}"
            style="width:40px;height:40px"
          />

          <p style="font-size:1em;color:#fff;margin-top:12px;">
            Content Calendar
          </p>
          <p
            style="font-size:0.875em;color:#fff;opacity:0.8;line-height:1.75em;margin-top:0.5em"
          >
            Schedule your social media in seconds, with categorized queues and a
            drag & drop visual calendar.
          </p>
        </li>

        <li>
          <img src="{{URL::asset('img/businesswhite.png')}}" style="width:50px;height:50px" />

          <p style="font-size:1em;color:#fff;margin-top:12px;">
            Schedule Google My Business posts
          </p>
          <p
            style="font-size:0.875em;color:#fff;opacity:0.8;line-height:1.75em;margin-top:0.5em"
          >
            Stand out in Google searches by scheduling posts to Google My Business.
          </p>
        </li>

        <li>
          <img src="{{URL::asset('img/recycle.png')}}" style="width:40px;height:40px" />

          <p style="font-size:1em;color:#fff;margin-top:12px;">Automate Posts via RSS Feeds</p>
          <p
            style="font-size:0.875em;color:#fff;opacity:0.8;line-height:1.75em;margin-top:0.5em"
          >
            Schedule your posts to be automatically repeated at the interval you choose, or auto-post via RSS feeds.
          </p>
        </li>
      </ul>
    </section>

    <section
      class="section section-light section-testimonials"
      style="background-color:#fff"
    >
      <h3>You're in great company.</h3>
      <p class="testimonial-lead">See what customers say about us</p>
      <p style="font-size:1em;margin-top:60px;text-align:left">
        OneUp helps me get the most out of my blog posts by helping readers see
        articles they may have missed. It saves me hours of time reposting
        across social media accounts.
      </p>
      <p style="font-size:1em;font-weight:bold;text-align:left;margin-top:10px">
        <a
          href="https://www.nirandfar.com/"
          style="color:inherit"
          target="blank"
          >Nir Eyal</a
        >
        —
        <span style="text-decoration:underline">
          <a
            href="https://www.amazon.com/Hooked-How-Build-Habit-Forming-Products/dp/1591847788/?tag=thekerne-20"
            style="color:inherit"
            target="blank"
          >
            Author of Hooked: How to Build Habit-Forming Products
          </a>
        </span>
      </p>

      <p style="font-size:1em;margin-top:40px;text-align:left">
        The web is crowded and noisy, it's difficult to stand above others. Some
        tools let you post regularly but nothing offers the possibility to
        manage your evergreen content the right way. <br />OneUp comes at the
        right for me and my side project. Time is scarce and OneUp helps you
        focus on your features by automating your social strategy. You don't
        need to hesitate, I'm building my strategy right now to have a complete
        month of content on Twitter and Facebook!
      </p>
      <p style="font-size:1em;font-weight:bold;text-align:left;margin-top:10px">
        Jean-Philippe Fong—
        <a href="https://usebookman.com" style="color:inherit" target="blank"
          ><span style="text-decoration:underline"
            >Founder of Bookman
          </span></a
        >
      </p>
    </section>
    <section
      class="section section-light section-centered"
      style="background-color:#fafafa"
    >
      <h3>Still not convinced?</h3>
      <p>Watch our 3 minute video to see how it works:</p>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/m69nnBkNAtc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </section>
    <section
      class="section section-light section-centered"
      style="background-color:#fff"
    >
      <h3>24 Hour Customer Support</h3>
      <p>
        We have a worldwide team of customer advocates on standby, so all your
        questions get an answer, fast.
      </p>
    </section>

    <section class="section section-light" style="background-color:#fafafa">
      <div class="footer-container">
        <div class="on-this-page">
          <h5>ON THIS PAGE</h5>
          <a href="/price"> <b>Pricing</b> </a> <a href="/privacy"> Privacy </a>
          <a href="/terms"> Terms </a>
          <a href="/jobs"> Jobs </a>
          <a href="/team"> Team </a>
        </div>
        <div class="heed-help">
          <h5>NEED HELP?</h5>
          <a href="mailto:support@oneupapp.io" target="_top">
            Contact Support
          </a>
          <a
            href="https://calendly.com/oneupapp/one-to-one-demo"
            target="blank"
          >
            Schedule a Demo
          </a>
          <a href="https://blog.oneupapp.io" target="blank"> Blog </a>

        </div>
        <div class="on-this-page" style="width:300px">
          <h5>RESOURCES</h5>
          <a
            href="/oneup-usecase"
            target="blank"
          >
            <b>Case Study</b>
          </a>
          <a href="/google-my-business"> Google My Business </a>
          <a
            href="/oneup-vs-meet-edgar"
          >
            OneUp vs MeetEdgar
          </a>
          <a
            href="https://docs.google.com/document/d/1e6sVag4QVt4YkSZrs65H5kgt5NUIkgqISVlNV_PiTHw/edit?usp=sharing"
            target="blank"
          >
            Testimonials
          </a>
        </div>
        <div class="footer-links">
          <a href="https://www.facebook.com/myoneup/" target="blank"
            ><img
              src="{{URL::asset('img/footer_facebook.png')}}"
              style="width:30px;height:30px;float:left"
          /></a>
          <a href="https://twitter.com/talk2oneup" target="blank"
            ><img
              src="{{URL::asset('img/footer_twitter.png')}}"
              style="width:30px;height:30px;float:left;margin-left:20px;"
          /></a>

          <a
            class="android-link"
            href="https://www.capterra.com/p/179588/OneUp/"
            target="blank"
            ><img
              src="{{URL::asset('img/capterra.png')}}"
              style="float:left;width:150px;height:100px;margin-left:-12px;margin-top:15px"
          /></a>
        </div>
      </div>
    </section>
  </body>
</html>

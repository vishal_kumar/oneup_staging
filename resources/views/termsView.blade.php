
<!DOCTYPE html><html lang=""><head><meta charSet="utf-8"/><meta http-equiv="x-ua-compatible" content="ie=edge"/><title>Terms - OneUp</title><meta name="description" content="This description shows when you share a link to your page on Facebook or Twitter."/><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/><link rel="canonical" href="https://oneup.landen.co/terms"/><meta name="google-site-verification" content="bAbKJB62p6ujDXw63JnGNEAsaRiqUpx53rvjhnEZlFQ"/>
<link rel="icon" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/><link rel="stylesheet" href="https://cdn.landen.co/6vh800wvro81/assets/main.cf1e5fc5.css"/><style tyle="text/css">body {
    font-family: 'Roboto', Arial, serif;
    font-size: 14px;
    font-weight: 400;
    --title-scale: 1;
  }

  @media only screen and (max-width: 750px) {
    body {
      --title-scale: 0.7;
    }
  } .wr { max-width: 1030px; max-width: var(--wr-max); } .section:not(.dark), .section:not(.dark) .color-2 {
    color: #fff;
  } .section.dark, .section.dark .color-2 {
    color: rgba(17,17,17,1);
  } .section:not(.dark) .color-1 {
    color: #fff;
  } .section.dark .color-1 {
    color: rgba(17,17,17,1);
  } .section:not(.dark) .color-2 {
    color: #fff;
  } .section.dark .color-2 {
    color: rgba(17,17,17,1);
  } a:not(.btn), .link {
    color: #fff;
    --link-hover-bg: rgba(255, 255, 255, 0.05);
    --link-hover: #cccccc;
  } .dark a:not(.btn), .dark .link {
    color: rgba(17,17,17,1);
    --link-hover-bg: rgba(17, 17, 17, 0.05);
    --link-hover: rgb(0, 0, 0);
  } .weight-text { font-weight: 400; } .weight-text-m { font-weight: 500; } .weight-text-h { font-weight: 700; } .weight-title { font-weight: 400; } .weight-title-m { font-weight: 400; } .weight-title-h { font-weight: 400; } .font-title { font-family: 'Varela Round', Arial, serif; }
      a, .link { text-decoration: none; font-weight: 500; }
      a:not(.btn):hover { text-decoration: underline }</style><link href="https://fonts.googleapis.com/css?family=Varela+Round|Roboto:400,500,700" rel="stylesheet"/><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-138336756-1"></script><script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-138336756-1');</script></head><body><header id="header" class="section header dark" style="--wr-max:1030px"><div class="wr color-1"><a id="headerLogo" href="/" class="header__logo" style="font-weight:400">OneUp</a><nav id="headerNav" class="header__nav"><div class="headerNav__links"><a href="https://www.oneupapp.io/price">Pricing</a></div><div class="header__navCtas"><a href="https://www.oneupapp.io/register" class="btn btn--b btn--primary" style="color:#fff;background-color:rgba(17,17,17,1)">Sign Up</a></div></nav><nav id="headerMenu" class="header__nav header__nav--hidden"><div id="headerMenuButton" class="burger">Menu</div></nav></div><nav id="headerDrawer" class="headerMenu col-dark-sec"><div id="headerDrawerBackdrop" class="headerMenu__backdrop"></div><div class="headerMenu__wrapper" style="background-color:rgb(255, 255, 255)"><button id="headerDrawerClose" class="headerMenu__close btn"></button><ul class="headerMenu__links"><li><a href="https://www.oneupapp.io/price" class="drawerLink">Pricing </a></li><li><a href="https://www.oneupapp.io/register" class="drawerLink col-dark">Sign Up <svg width="13" height="12" xmlns="http://www.w3.org/2000/svg"><path d="M9.557 7H1a1 1 0 1 1 0-2h8.586L7.007 2.421a1 1 0 0 1 1.414-1.414l4.243 4.243c.203.202.3.47.292.736a.997.997 0 0 1-.292.735L8.42 10.964A1 1 0 1 1 7.007 9.55L9.557 7z" fill="currentColor"></path></svg></a></li></ul></div></nav><script>(function() {
  var buffer = 10;
  var margin = 60 + 40 + buffer;
  // button
  var button = document.getElementById('headerMenuButton');
  var buttonWrapper = document.getElementById('headerMenu');
  // drawer
  var drawer = document.getElementById('headerDrawer');
  var drawerBackdrop = document.getElementById('headerDrawerBackdrop');
  var drawerClose = document.getElementById('headerDrawerClose');
  // header UI
  var logo = document.getElementById('headerLogo');
  var nav = document.getElementById('headerNav');
  var logoWidth = logo.offsetWidth;
  var navWidth = nav.offsetWidth;

  var determineVisibility = function() {
    var parent = window.innerWidth;
    if (logoWidth + navWidth > parent - margin) {
      nav.classList.add('header__nav--hidden');
      buttonWrapper.classList.add('headerMenu--visible');
    } else {
      nav.classList.remove('header__nav--hidden');
      buttonWrapper.classList.remove('headerMenu--visible');
    }
  };

  var showDrawer = function() {
    drawer.classList.add('headerMenu--visible');
  };

  var hideDrawer = function() {
    drawer.classList.remove('headerMenu--visible');
  };

  button.onclick = showDrawer;
  drawerBackdrop.onclick = hideDrawer;
  drawerClose.onclick = hideDrawer;

  window.addEventListener('resize', determineVisibility);
  determineVisibility();
})();
</script></header><div id="content" class="section section--content section--left dark" style="background-color:rgba(255,255,255,1);padding-bottom:60px;padding-top:130px;--pdx-min-height:0px;--wr-max:1030px"><div class="wr"><div class="color textBody mkd textBody--left textBody--dark"><!-- wp:heading {"level":3} -->
<h3>Terms of&nbsp;Use</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Welcome, and thank you for your interest in OneUp (“<strong>OneUp</strong>,” “<strong>we</strong>,” or “<strong>us</strong>”) and our website at oneupapp.io, along with our related websites, networks, applications, and other services provided by us (collectively, the “Service”). These Terms of Service are a legally binding contract between you and OneUp regarding your use of the Service.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>PLEASE READ THE FOLLOWING TERMS CAREFULLY</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>BY CLICKING “I ACCEPT,” OR BY DOWNLOADING, INSTALLING, OR OTHERWISE ACCESSING OR USING THE SERVICE, YOU AGREE THAT YOU HAVE READ AND UNDERSTOOD, AND, AS A CONDITION TO YOUR USE OF THE SERVICE, YOU AGREE TO BE BOUND BY, THE FOLLOWING TERMS AND CONDITIONS, INCLUDING OneUp’S PRIVACY POLICY AND ANY ADDITIONAL TERMS AND POLICIES OneUp MAY PROVIDE FROM TIME TO TIME (TOGETHER, THESE “TERMS”). If you are not eligible, or do not agree to the Terms, then you do not have our permission to use the Service. YOUR USE OF THE SERVICE, AND OneUp’S PROVISION OF THE SERVICE TO YOU, CONSTITUTES AN AGREEMENT BY OneUp AND BY YOU TO BE BOUND BY THESE TERMS.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Please read our Privacy Policy carefully for information relating to our collection, use, storage, disclosure of your personal information. The Privacy Policy is incorporated by this reference into, and made a part of, these Terms.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>ARBITRATION NOTICE</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Except for certain kinds of disputes described in Section 16, you agree that disputes arising under these Terms will be resolved by binding, individual arbitration, and BY ACCEPTING THESE TERMS, YOU AND ONEUP ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN ANY CLASS ACTION OR REPRESENTATIVE PROCEEDING. YOU AGREE TO GIVE UP YOUR RIGHT TO GO TO COURT to assert or defend your rights under this contract (except for matters that may be taken to small claims court). Your rights will be determined by a NEUTRAL ARBITRATOR and NOT a judge or jury. (See Section 16).</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>1. OneUp Service&nbsp;Overview</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>The Service provides a social media management tool that enables users to release posts on social platforms at a scheduled time, in addition to other design and analytics tools to help bolster users’ social media content.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>2. Eligibility</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>You must be at least [18] years old to use the Service. By agreeing to these Terms, you represent and warrant to us that: (a) you are at least [18] years old; (b) you have not previously been suspended or removed from the Service; and © your registration and your use of the Service is in compliance with any and all applicable laws and regulations. If you are an entity, organization, or company, the individual accepting these Terms on your behalf represents and warrants that they have authority to bind you to these Terms and references to you herein (and all of your obligations hereunder) will refer to such entity and any individual using the Service on such entity’s behalf.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>3. Accounts and Registration</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>To access most features of the Service, you must register for an account. When you register for an account, you may be required to provide us with some information about yourself, such as your name, email address, or other contact information. You agree that the information you provide to us is accurate and that you will keep it accurate and up-to-date at all times. When you register, you will be asked to provide a password. You are solely responsible for maintaining the confidentiality of your account and password, and you accept responsibility for all activities that occur under your account. If you believe that your account is no longer secure, then you must immediately notify us.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>4. General Payment&nbsp;Terms</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Premium features of the Service will require you to pay fees upon registering for the applicable premium service. Before you pay any fees, you will have an opportunity to review and accept the fees that you will be charged. All fees are in U.S. Dollars and are non-refundable. Fees vary based on the plan, with different pricing schemes for individual users and organizations.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>4.1 Price</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>OneUp reserves the right to determine pricing for the Service. OneUp will make reasonable efforts to keep pricing information published on the website up to date. We encourage you to check our website periodically for current pricing information, located here: oneupapp.io/price. OneUp may change the fees for any feature of the Service, including additional fees or charges, if OneUp gives you advance notice of changes before they apply. OneUp, at its sole discretion, may make promotional offers with different features and different pricing to any of OneUp’s customers. These promotional offers, unless made to you, will not apply to your offer or these Terms.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>4.2 Authorization</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>You authorize OneUp to charge all sums for the orders that you make and any level of Service you select as described in these Terms or published by OneUp, to the payment method specified in your account. If you pay any fees with a credit card, OneUp may seek pre-authorization of your credit card account prior to your purchase to verify that the credit card is valid and has the necessary funds or credit available to cover your purchase.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>4.3 Subscription Service and Cancellation Policy</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>The Service may include automatically recurring payments for periodic charges (“<strong>Subscription Service</strong>”). If you activate a Subscription Service, you authorize OneUp to periodically charge, on a going-forward basis and until cancellation of either the recurring payments or your account, all accrued sums on or before the payment due date for the accrued sums. The “<strong>Subscription Billing Date</strong>” is the date when you purchase your first subscription to the Service. For information on the “<strong>Subscription Fee</strong>”, please see our Pricing page. Your account will be charged automatically on the Subscription Billing Date all applicable fees for the next subscription period. The subscription will continue unless and until you cancel your subscription or we terminate it. You must cancel your subscription before it renews in order to avoid billing of the next periodic Subscription Fee to your account. We will bill the periodic Subscription Fee to the payment method you provide to us during registration (or to a different payment method if you change your payment information).</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>4.4 Delinquent Accounts</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>OneUp may suspend or terminate access to the Service for any account for which any amount is due but unpaid. In addition to the amount due for the Service, a delinquent account will be charged with fees or charges that are incidental to any chargeback or collection of any the unpaid amount, including collection fees.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>5. Licenses</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>5.1 Permission to&nbsp;Use</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Subject to your complete and ongoing compliance with these Terms, OneUp grants you limited, non-transferable, non-sublicensable, revocable permission to access and use the Service for your personal, internal use during the Term at the level of service for which you have paid all applicable Fees.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>5.2 Restrictions</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Except and solely to the extent such a restriction is impermissible under applicable law, you may not: (a) reproduce, distribute, publicly display, or publicly perform the Service; (b) make modifications to the Service; or © interfere with or circumvent any feature of the Service, including any security or access control mechanism. If you are prohibited under applicable law from using the Service, you may not use it. You may not use the Service on behalf of any third party, or in a service bureau or similar capacity.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>5.3 Feedback</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If you choose to provide input and suggestions regarding problems with or proposed modifications or improvements to the Service (“<strong>Feedback</strong>”), then you hereby grant OneUp an unrestricted, perpetual, irrevocable, non-exclusive, fully-paid, royalty-free right to exploit the Feedback in any manner and for any purpose, including to improve the Service and create other products and services.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>6. Ownership; Proprietary Rights</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>The Service is owned and operated by OneUp. The visual interfaces, graphics, design, compilation, information, data, computer code (including source code or object code), products, software, services, templates, and all other elements of the Service (“<strong>Materials</strong>”) provided by OneUp are protected by intellectual property and other laws. All Materials included in the Service are the property of OneUp or its third party licensors. Except as expressly authorized by OneUp, you may not make use of the Materials. OneUp reserves all rights to the Materials not granted expressly in these Terms.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>7. Third Party&nbsp;Terms</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>7.1 Third Party Services and Linked&nbsp;Websites</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>OneUp provides tools through the Service that enable you to import and export information, including User Content, to and from third party services, including through features that allow you to link your account on OneUp with an account on a third party social network service, such as Twitter or Facebook. By using one of these tools, you agree that OneUp may transfer that information to and from the applicable third party service. Third party services are not under OneUp’s control, and OneUp is not responsible for any third party service’s use of your exported information. The Service may also contain links to third party websites. Linked websites are not under OneUp’s control, and OneUp is not responsible for their content.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>7.2 Third Party&nbsp;Software</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>The Service may include or incorporate third party software components that are generally available free of charge under licenses granting recipients broad rights to copy, modify, and distribute those components (“<strong>Third Party Components</strong>”). Although the Service is provided to you subject to these Terms, nothing in these Terms prevents, restricts, or is intended to prevent or restrict you from obtaining Third Party Components under the applicable third party licenses or to limit your use of Third Party Components under those third party licenses.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>8. User&nbsp;Content</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>8.1 User Content Generally</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Certain features of the Service may permit users to upload content to the Service, including social media posts and other content which may be comprised of messages, reviews, photos, video, images, data, text, and other types of works (“<strong>User Content</strong>”) and to publish User Content on the Service. <strong>You retain any copyright and other proprietary rights that you may hold in the User Content that you post to the Service</strong>. Nevertheless, we need certain permission from you in order to provide the Service.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>8.2 Limited License Grant to&nbsp;OneUp</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>By providing User Content to or via the Service, you grant OneUp a worldwide, non-exclusive, royalty-free, fully paid right and license (with the right to sublicense) to host, store, transfer, display, perform, reproduce, modify for the purpose of formatting for display, and distribute your User Content, in whole or in part, in any media formats and through any media channels now known or hereafter developed.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>8.3 Limited License Grant to Other&nbsp;Users</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>By providing User Content to or via the Service to other users of the Service, you grant those users a non-exclusive license to access and use that User Content as permitted by these Terms and the functionality of the Service.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>8.4 User Content Representations and Warranties</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>OneUp disclaims any and all liability in connection with User Content. You are solely responsible for your User Content and the consequences of providing User Content via the Service. By providing User Content via the Service, you affirm, represent, and warrant that:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>a) you are the creator and owner of the User Content, or have the necessary licenses, rights, consents, and permissions to authorize OneUp and users of the Service to use and distribute your User Content as necessary to exercise the licenses granted by you in this Section, in the manner contemplated by OneUp, the Service, and these Terms;<br>b) your User Content, and the use of your User Content as contemplated by these Terms, does not and will not: (i) infringe, violate, or misappropriate any third party right, including any copyright, trademark, patent, trade secret, moral right, privacy right, right of publicity, or any other intellectual property or proprietary right; (ii) slander, defame, libel, or invade the right of privacy, publicity or other property rights of any other person; or (iii) cause OneUp to violate any law or regulation; and<br>c) your User Content could not be deemed by a reasonable person to be objectionable, profane, indecent, pornographic, harassing, threatening, embarrassing, hateful, or otherwise inappropriate.<br>d) your User Content does not and will not contain Hateful Content, a Threat of Physical Harm, or Harassment<br>The following serves as a guide to help illustrate generally the types of content that fall within the scope of OneUp’s policy on Hateful Content, Threats of Physical Harm, and Harassment, but is not exhaustive. This Section 8.4(d) does not limit any of our other rights or remedies provided herein. For the avoidance of doubt, your User Content may not include, and OneUp may remove or refuse to publish or promote any User Content that violates the terms or policies of any third party platform with which OneUp’s Services integrate or interoperate.<br>Hateful Content includes:</li><li>Any statement, image, photograph, or other content that in our sole judgment could be reasonably perceived to harm, threaten, demean, promote the harassment of, promote the intimidation of, or promote the abuse of others for any reason, including by reason of race, gender or gender identity, national origin, sexual orientation, religion, or otherwise.</li><li>A Threat of Physical Harm includes:</li><li>Any statement, photograph, advertisement, or other content that in our sole judgment could be reasonably perceived to threaten, advocate, or incite physical harm to or violence against others, including references to current or historical figures or groups that are known for purporting such content, such as the Ku Klux Klan, Nazi Party, and the like.</li><li>Harassment includes:</li><li>Revealing someone’s personal information, also known as “doxxing”.</li><li>Online stalking, and bullying.</li><li>Wishes for physical harm directed at a person or persons.</li><li>Incitement of others to any of the previous items.</li><li><strong>We reserve the right to suspend or terminate accounts and remove individual posts which contain Hateful Content, a Threat of Physical Harm, or Harassment.<br></strong>We also may suspend or terminate your account if we determine, in our sole discretion, that you are either:</li><li>An organization which has publicly stated or acknowledged that its goals, objectives, positions, or founding tenets include statements or principles that could be reasonably perceived to advocate, encourage, or sponsor Hateful Content, Harassment, or A Threat of Physical Harm.</li><li>A person or organization that has acted in such a way as could be reasonably perceived to support, condone, encourage, or represent Hateful Content, Harassment, or A Threat of Physical Harm.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Notwithstanding the foregoing, we reserve the right to screen, remove, edit, or block any User Content we find in violation of the Terms or that we find, in our sole discretion to be otherwise objectionable, at our sole discretion.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>8.5 User Content Disclaimer</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>We are under no obligation to edit or control User Content that you or other users post or publish, and will not be in any way responsible or liable for User Content. OneUp may, however, at any time and without prior notice, screen, remove, edit, or block any User Content that in our sole judgment violates these Terms or is otherwise objectionable. You understand that when using the Service you will be exposed to User Content from a variety of sources and acknowledge that User Content may be inaccurate, offensive, indecent, or objectionable. You agree to waive, and do waive, any legal or equitable right or remedy you have or may have against OneUp with respect to User Content. If notified by a user or content owner that User Content allegedly does not conform to these Terms, we may investigate the allegation and determine in our sole discretion whether to remove the User Content, which we reserve the right to do at any time and without notice. For clarity, OneUp does not permit copyright-infringing activities on the Service.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>8.6 Monitoring Content</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>OneUp does not control and does not have any obligation to monitor: (a) User Content; (ii) any content made available by third parties; or (iii) the use of the Service by its users. You acknowledge and agree that OneUp reserves the right to, and may from time to time, monitor any and all information transmitted or received through the Service for operational and other purposes. If at any time OneUp chooses to monitor the content, OneUp still assumes no responsibility or liability for content or any loss or damage incurred as a result of the use of content. During monitoring, information may be examined, recorded, copied, and used in accordance with our Privacy Policy.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>9. Prohibited Conduct</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>BY USING THE SERVICE YOU AGREE NOT TO:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>use the Service for any illegal purpose or in violation of any local, state, national, or international law;</li><li>violate, or encourage others to violate, any right of a third party, including by infringing or misappropriating any third party intellectual property right;</li><li>interfere with security-related features of the Service, including by: (i) disabling or circumventing features that prevent or limit use or copying of any content; (ii) reverse engineering or otherwise attempting to discover the source code of any portion of the Service except to the extent that the activity is expressly permitted by applicable law; or (iii) hacking, password “mining” or using any other illegitimate means of interference;</li><li>modify or create derivatives of any part of the Service;</li><li>interfere with the operation of the Service or any user’s enjoyment of the Service, including by: (i) uploading or otherwise disseminating any virus, adware, spyware, worm, or other malicious code; (ii) making any unsolicited offer or advertisement to another user of the Service; (iii) collecting personal information about another user or third party without consent; or (iv) interfering with or disrupting any network, equipment, or server connected to or used to provide the Service;</li><li>perform any fraudulent activity including impersonating any person or entity, claiming a false affiliation, accessing any other Service account without permission, or falsifying your age or date of birth;</li><li>take action that imposes an unreasonable or disproportionately large load on the infrastructure of the Service of OneUp’s systems or networks, or any systems or networks connected to the Service or OneUp;</li><li>sell or otherwise transfer the access granted under these Terms or any Materials (as defined in Section 6) or any right or ability to view, access, or use any Materials; or</li><li>attempt to do any of the acts described in this Section 9 or assist or permit any person in engaging in any of the acts described in this Section 9.</li></ul>
<!-- /wp:list -->

<!-- wp:heading {"level":3} -->
<h3>10. Digital Millennium Copyright Act</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>10.1 DMCA Notification</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>We comply with the provisions of the Digital Millennium Copyright Act applicable to Internet service providers (17 U.S.C. §512, as amended).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Any notice alleging that materials hosted by or distributed through the Service infringe intellectual property rights must include the following information:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other right being infringed;</li><li>a description of the copyrighted work or other intellectual property that you claim has been infringed;</li><li>a description of the material that you claim is infringing and where it is located on the Service;</li><li>your address, telephone number, and email address;</li><li>a statement by you that you have a good faith belief that the use of the materials on the Service of which you are complaining is not authorized by the copyright owner, its agent, or the law; and</li><li>a statement by you that the above information in your notice is accurate and that, under penalty of perjury, you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner’s behalf.</li></ul>
<!-- /wp:list -->

<!-- wp:heading {"level":3} -->
<h3>10.2 Repeat Infringers</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>It is OneUp’s policy to promptly terminate the accounts of users that are determined by OneUp to be repeat infringers.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>11. Modification of these&nbsp;Terms</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>We reserve the right to change these Terms on a going-forward basis at any time. Please check these Terms periodically for changes. Modifications will become effective upon the earlier of (a) your acceptance of the modified Terms, (b) your use of the Service with actual knowledge of the modified Terms, or © thirty (30) days following our publication of the modified Terms through the Service. Except as expressly permitted in this Section 11, these Terms may be amended only by a written agreement signed by authorized representatives of the parties to these Terms. Disputes arising under these Terms will be resolved in accordance with the version of these Terms that was in effect at the time the dispute arose.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>12. Term, Termination and Modification of the&nbsp;Service</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>12.1 Term</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>These Terms are effective beginning when you accept the Terms or first download, install, access, or use the Service, and ending when terminated as described in Section 12.2.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>12.2 Termination</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If you violate any provision of these Terms, your authorization to access the Service and these Terms automatically terminate. In addition, OneUp may, at its sole discretion, terminate these Terms or your account on the Service, or suspend or terminate your access to the Service, at any time for any reason or no reason, with or without notice. You may terminate your account and these Terms at any time as provided in Section 4.3 or by contacting customer service at support@oneupapp.io.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>12.3 Effect of Termination</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Upon termination of these Terms: (a) your license rights will terminate and you must immediately cease all use of the Service; (b) you will no longer be authorized to access your account or the Service; © you must pay OneUp any unpaid amount that was due prior to termination; and (d) all payment obligations accrued prior to termination and Sections 5.3, 6, 12.3, 13, 14, 15, 16 and 17 will survive.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>12.4 Modification of the&nbsp;Service</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>OneUp reserves the right to modify or discontinue the Service at any time (including by limiting or discontinuing certain features of the Service), temporarily or permanently, without notice to you. OneUp will have no liability for any change to the Service or any suspension or termination of your access to or use of the Service. Premium service fees are not refundable.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>13. Indemnity</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>You are responsible for your use of the Service, and you will defend and indemnify OneUp and its officers, directors, employees, consultants, affiliates, subsidiaries and agents (together, the “OneUp <strong>Entities</strong>”) from and against every claim brought by a third party, and any related liability, damage, loss, and expense, including reasonable attorneys’ fees and costs, arising out of or connected with: (a) your use of, or misuse of, the Service; (b) your violation of any portion of these Terms, any representation, warranty, or agreement referenced in these Terms, or any applicable law or regulation; © your violation of any third party right, including any intellectual property right or publicity, confidentiality, other property, or privacy right; or (d) any dispute or issue between you and any third party. We reserve the right, at our own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you (without limiting your indemnification obligations with respect to that matter), and in that case, you agree to cooperate with our defense of those claims.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>14. Disclaimers; No Warranties</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>THE SERVICE AND ALL MATERIALS AND CONTENT AVAILABLE THROUGH THE SERVICE ARE PROVIDED “AS IS” AND ON AN “AS AVAILABLE” BASIS. OneUp DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, RELATING TO THE SERVICE AND ALL MATERIALS AND CONTENT AVAILABLE THROUGH THE SERVICE, INCLUDING: (A) ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, QUIET ENJOYMENT, OR NON-INFRINGEMENT; AND (B) ANY WARRANTY ARISING OUT OF COURSE OF DEALING, USAGE, OR TRADE. OneUp DOES NOT WARRANT THAT THE SERVICE OR ANY PORTION OF THE SERVICE, OR ANY MATERIALS OR CONTENT OFFERED THROUGH THE SERVICE, WILL BE UNINTERRUPTED, SECURE, OR FREE OF ERRORS, VIRUSES, OR OTHER HARMFUL COMPONENTS, AND OneUp DOES NOT WARRANT THAT ANY OF THOSE ISSUES WILL BE CORRECTED.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM THE SERVICE OR OneUp ENTITIES OR ANY MATERIALS OR CONTENT AVAILABLE THROUGH THE SERVICE WILL CREATE ANY WARRANTY REGARDING ANY OF THE OneUp ENTITIES OR THE SERVICE THAT IS NOT EXPRESSLY STATED IN THESE TERMS. WE ARE NOT RESPONSIBLE FOR ANY DAMAGE THAT MAY RESULT FROM THE SERVICE AND YOUR DEALING WITH ANY OTHER SERVICE USER. YOU UNDERSTAND AND AGREE THAT YOU USE ANY PORTION OF THE SERVICE AT YOUR OWN DISCRETION AND RISK, AND THAT WE ARE NOT RESPONSIBLE FOR ANY DAMAGE TO YOUR PROPERTY (INCLUDING YOUR COMPUTER SYSTEM OR MOBILE DEVICE USED IN CONNECTION WITH THE SERVICE) OR ANY LOSS OF DATA, INCLUDING USER CONTENT.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>HOWEVER, OneUp DOES NOT DISCLAIM ANY WARRANTY OR OTHER RIGHT THAT OneUp IS PROHIBITED FROM DISCLAIMING UNDER APPLICABLE LAW.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>15. Limitation of Liability</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>TO THE FULLEST EXTENT PERMITTED BY LAW, IN NO EVENT WILL THE OneUp ENTITIES BE LIABLE TO YOU FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES (INCLUDING DAMAGES FOR LOSS OF PROFITS, GOODWILL, OR ANY OTHER INTANGIBLE LOSS) ARISING OUT OF OR RELATING TO YOUR ACCESS TO OR USE OF, OR YOUR INABILITY TO ACCESS OR USE, THE SERVICE OR ANY MATERIALS OR CONTENT ON THE SERVICE, WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), STATUTE, OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT ANY OneUp ENTITY HAS BEEN INFORMED OF THE POSSIBILITY OF DAMAGE.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>EXCEPT AS PROVIDED IN SECTION 16.4(iii) AND TO THE FULLEST EXTENT PERMITTED BY LAW, THE AGGREGATE LIABILITY OF THE OneUp ENTITIES TO YOU FOR ALL CLAIMS ARISING OUT OF OR RELATING TO THE USE OF OR ANY INABILITY TO USE ANY PORTION OF THE SERVICE OR OTHERWISE UNDER THESE TERMS, WHETHER IN CONTRACT, TORT, OR OTHERWISE, IS LIMITED TO THE GREATER OF: (A) THE AMOUNT YOU HAVE PAID TO OneUp FOR ACCESS TO AND USE OF THE SERVICE IN THE 12 MONTHS PRIOR TO THE EVENT OR CIRCUMSTANCE GIVING RISE TO CLAIM; OR (B) $100.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>EACH PROVISION OF THESE TERMS THAT PROVIDES FOR A LIMITATION OF LIABILITY, DISCLAIMER OF WARRANTIES, OR EXCLUSION OF DAMAGES IS INTENDED TO AND DOES ALLOCATE THE RISKS BETWEEN THE PARTIES UNDER THESE TERMS. THIS ALLOCATION IS AN ESSENTIAL ELEMENT OF THE BASIS OF THE BARGAIN BETWEEN THE PARTIES. EACH OF THESE PROVISIONS IS SEVERABLE AND INDEPENDENT OF ALL OTHER PROVISIONS OF THESE TERMS. THE LIMITATIONS IN THIS SECTION 15 WILL APPLY EVEN IF ANY LIMITED REMEDY FAILS OF ITS ESSENTIAL PURPOSE.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>16. Dispute Resolution and Arbitration</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>16.1 Generally</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>In the interest of resolving disputes between you and OneUp in the most expedient and cost effective manner, and except as described in Section 16.2, you and OneUp agree that every dispute arising in connection with these Terms will be resolved by binding arbitration. Arbitration is less formal than a lawsuit in court. Arbitration uses a neutral arbitrator instead of a judge or jury, may allow for more limited discovery than in court, and can be subject to very limited review by courts. Arbitrators can award the same damages and relief that a court can award. This agreement to arbitrate disputes includes all claims arising out of or relating to any aspect of these Terms, whether based in contract, tort, statute, fraud, misrepresentation, or any other legal theory, and regardless of whether a claim arises during or after the termination of these Terms. YOU UNDERSTAND AND AGREE THAT, BY ENTERING INTO THESE TERMS, YOU AND OneUp ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN A CLASS ACTION.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>16.2 Exceptions</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Despite the provisions of Section 16.1, nothing in these Terms will be deemed to waive, preclude, or otherwise limit the right of either party to: (a) bring an individual action in small claims court; (b) pursue an enforcement action through the applicable federal, state, or local agency if that action is available; © seek injunctive relief in a court of law in aid of arbitration; or (d) to file suit in a court of law to address an intellectual property infringement claim.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>16.3 Arbitrator</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Any arbitration between you and OneUp will be settled under the Federal Arbitration Act and administered by the American Arbitration Association (“AAA”) under its Consumer Arbitration Rules (collectively, “AAA Rules”) as modified by these Terms. The AAA Rules and filing forms are available online at <a href="http://www.adr.org/" rel="noreferrer noopener" target="_blank">www.adr.org</a>, by calling the AAA at 1–800–778–7879, or by contacting OneUp. The arbitrator has exclusive authority to resolve any dispute relating to the interpretation, applicability, or enforceability of this binding arbitration agreement.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>16.4 Notice of Arbitration; Process</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>A party who intends to seek arbitration must first send a written notice of the dispute to the other party by certified U.S. Mail or by Federal Express (signature required) or, only if that other party has not provided a current physical address, then by electronic mail (“Notice of Arbitration”). OneUp’s address for Notice is: OneUp, #1179 SECOND FLOOR 21 A CROSS 14 MAIN SECTOR 3 HSR LAYOUT BANGALORE 560102. The Notice of Arbitration must: (a) describe the nature and basis of the claim or dispute; and (b) set forth the specific relief sought (“Demand”). The parties will make good faith efforts to resolve the claim directly, but if the parties do not reach an agreement to do so within 30 days after the Notice of Arbitration is received, you or OneUp may commence an arbitration proceeding. During the arbitration, the amount of any settlement offer made by you or OneUp must not be disclosed to the arbitrator until after the arbitrator makes a final decision and award, if any. If the dispute is finally resolved through arbitration in your favor, OneUp will pay you the highest of the following: (i) the amount awarded by the arbitrator, if any; (ii) the last written settlement amount offered by OneUp in settlement of the dispute prior to the arbitrator’s award; or (iii) $10,000.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>16.5 Fees</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If you commence arbitration in accordance with these Terms, OneUp will reimburse you for your payment of the filing fee, unless your claim is for more than $10,000, in which case the payment of any fees will be decided by the AAA Rules. Any arbitration hearing will take place at a location to be agreed upon in Santa Clara County, California, but if the claim is for $10,000 or less, you may choose whether the arbitration will be conducted: (a) solely on the basis of documents submitted to the arbitrator; (b) through a non-appearance based telephone hearing; or © by an in-person hearing as established by the AAA Rules in the county (or parish) of your billing address. If the arbitrator finds that either the substance of your claim or the relief sought in the Demand is frivolous or brought for an improper purpose (as measured by the standards set forth in Federal Rule of Civil Procedure 11(b)), then the payment of all fees will be governed by the AAA Rules. In that case, you agree to reimburse OneUp for all monies previously disbursed by it that are otherwise your obligation to pay under the AAA Rules. Regardless of the manner in which the arbitration is conducted, the arbitrator must issue a reasoned written decision sufficient to explain the essential findings and conclusions on which the decision and award, if any, are based. The arbitrator may make rulings and resolve disputes as to the payment and reimbursement of fees or expenses at any time during the proceeding and upon request from either party made within 14 days of the arbitrator’s ruling on the merits.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>16.6 No Class&nbsp;Actions</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>YOU AND OneUp AGREE THAT EACH MAY BRING CLAIMS AGAINST THE OTHER ONLY IN YOUR OR ITS INDIVIDUAL CAPACITY AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS OR REPRESENTATIVE PROCEEDING. Further, unless both you and OneUp agree otherwise, the arbitrator may not consolidate more than one person’s claims, and may not otherwise preside over any form of a representative or class proceeding.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>16.7 Modifications to this Arbitration Provision</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If OneUp makes any future change to this arbitration provision, other than a change to OneUp’s address for Notice of Arbitration, you may reject the change by sending us written notice within 30 days of the change to OneUp’s address for Notice of Arbitration, in which case your account with OneUp will be immediately terminated and this arbitration provision, as in effect immediately prior to the changes you rejected will survive.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>16.8 Enforceability</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If Section 16.6 is found to be unenforceable or if the entirety of this Section 16 is found to be unenforceable, then the entirety of this Section 16 will be null and void and, in that case, the parties agree that the exclusive jurisdiction and venue described in Section 17.1 will govern any action arising out of or related to these Terms.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>17. General&nbsp;Terms</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>These Terms, together with the Privacy Policy and any other agreements expressly incorporated by reference into these Terms, are the entire and exclusive understanding and agreement between you and OneUp regarding your use of the Service. You may not assign or transfer these Terms or your rights under these Terms, in whole or in part, by operation of law or otherwise, without our prior written consent. We may assign these Terms at any time without notice or consent. The failure to require performance of any provision will not affect our right to require performance at any other time after that, nor will a waiver by us of any breach or default of these Terms, or any provision of these Terms, be a waiver of any subsequent breach or default or a waiver of the provision itself. Use of section headers in these Terms is for convenience only and will not have any impact on the interpretation of any provision. Throughout these Terms the use of the word “including” means “including but not limited to”. If any part of these Terms is held to be invalid or unenforceable, the unenforceable part will be given effect to the greatest extent possible, and the remaining parts will remain in full force and effect.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>17.1 Governing Law</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>These Terms are governed by the laws of the State of California without regard to conflict of law principles. You and OneUp submit to the personal and exclusive jurisdiction of the state courts and federal courts located within Santa Clara County, California for resolution of any lawsuit or court proceeding permitted under these Terms.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>17.2 Consent to Electronic Communications</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>By using the Service, you consent to receiving certain electronic communications from us as further described in our Privacy Policy. Please read our Privacy Policy to learn more about our electronic communications practices. You agree that any notices, agreements, disclosures, or other communications that we send to you electronically will satisfy any legal communication requirements, including that those communications be in writing.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>17.3 Contact Information</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>The Service is offered by OneUp, Inc. located at #1179 SECOND FLOOR 21 A CROSS 14 MAIN SECTOR 3 HSR LAYOUT BANGALORE 560102. You may contact us by sending correspondence to that address or by emailing us at support@oneupapp.io. You can access a copy of these Terms by clicking here: oneupapp.io/terms</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>17.4 Notice to California Residents</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If you are a California resident, under California Civil Code Section 1789.3, you may contact the Complaint Assistance Unit of the Division of Consumer Services of the California Department of Consumer Affairs in writing at 1625 N. Market Blvd., Suite S-202, Sacramento, California 95834, or by telephone at (800) 952–5210 in order to resolve a complaint regarding the Service or to receive further information regarding use of the Service.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>17.5 International Use</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Access to the Service from countries or territories or by individuals where such access is illegal is prohibited</p>
<!-- /wp:paragraph --></div></div></div><footer id="footer" class="section section--footer section--center dark" style="background-color:rgba(255,255,255,1);padding-bottom:15px;padding-top:15px;--pdx-min-height:0px;--wr-max:1030px"><div class="wr"><div class="footer footer--simple"><div class="footer__primary"><span></span></div></div></div></footer><script>/*! smooth-scroll v14.2.1 | (c) 2018 Chris Ferdinandi | MIT License | http://github.com/cferdinandi/smooth-scroll */
!(function(e,t){"function"==typeof define&&define.amd?define([],(function(){return t(e)})):"object"==typeof exports?module.exports=t(e):e.SmoothScroll=t(e)})("undefined"!=typeof global?global:"undefined"!=typeof window?window:this,(function(e){"use strict";var t={ignore:"[data-scroll-ignore]",header:null,topOnEmptyHash:!0,speed:500,clip:!0,offset:0,easing:"easeInOutCubic",customEasing:null,updateURL:!0,popstate:!0,emitEvents:!0},n=function(){return"querySelector"in document&&"addEventListener"in e&&"requestAnimationFrame"in e&&"closest"in e.Element.prototype},o=function(){for(var e={},t=0;t<arguments.length;t++)!(function(t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])})(arguments[t]);return e},r=function(t){return!!("matchMedia"in e&&e.matchMedia("(prefers-reduced-motion)").matches)},a=function(t){return parseInt(e.getComputedStyle(t).height,10)},i=function(e){var t;try{t=decodeURIComponent(e)}catch(n){t=e}return t},c=function(e){"#"===e.charAt(0)&&(e=e.substr(1));for(var t,n=String(e),o=n.length,r=-1,a="",i=n.charCodeAt(0);++r<o;){if(0===(t=n.charCodeAt(r)))throw new InvalidCharacterError("Invalid character: the input contains U+0000.");t>=1&&t<=31||127==t||0===r&&t>=48&&t<=57||1===r&&t>=48&&t<=57&&45===i?a+="\\"+t.toString(16)+" ":a+=t>=128||45===t||95===t||t>=48&&t<=57||t>=65&&t<=90||t>=97&&t<=122?n.charAt(r):"\\"+n.charAt(r)}var c;try{c=decodeURIComponent("#"+a)}catch(e){c="#"+a}return c},u=function(e,t){var n;return"easeInQuad"===e.easing&&(n=t*t),"easeOutQuad"===e.easing&&(n=t*(2-t)),"easeInOutQuad"===e.easing&&(n=t<.5?2*t*t:(4-2*t)*t-1),"easeInCubic"===e.easing&&(n=t*t*t),"easeOutCubic"===e.easing&&(n=--t*t*t+1),"easeInOutCubic"===e.easing&&(n=t<.5?4*t*t*t:(t-1)*(2*t-2)*(2*t-2)+1),"easeInQuart"===e.easing&&(n=t*t*t*t),"easeOutQuart"===e.easing&&(n=1- --t*t*t*t),"easeInOutQuart"===e.easing&&(n=t<.5?8*t*t*t*t:1-8*--t*t*t*t),"easeInQuint"===e.easing&&(n=t*t*t*t*t),"easeOutQuint"===e.easing&&(n=1+--t*t*t*t*t),"easeInOutQuint"===e.easing&&(n=t<.5?16*t*t*t*t*t:1+16*--t*t*t*t*t),e.customEasing&&(n=e.customEasing(t)),n||t},s=function(){return Math.max(document.body.scrollHeight,document.documentElement.scrollHeight,document.body.offsetHeight,document.documentElement.offsetHeight,document.body.clientHeight,document.documentElement.clientHeight)},l=function(t,n,o,r){var a=0;if(t.offsetParent)do{a+=t.offsetTop,t=t.offsetParent}while(t);return a=Math.max(a-n-o,0),r&&(a=Math.min(a,s()-e.innerHeight)),a},d=function(e){return e?a(e)+e.offsetTop:0},f=function(e,t,n){t||history.pushState&&n.updateURL&&history.pushState({smoothScroll:JSON.stringify(n),anchor:e.id},document.title,e===document.documentElement?"#top":"#"+e.id)},m=function(t,n,o){0===t&&document.body.focus(),o||(t.focus(),document.activeElement!==t&&(t.setAttribute("tabindex","-1"),t.focus(),t.style.outline="none"),e.scrollTo(0,n))},h=function(t,n,o,r){if(n.emitEvents&&"function"==typeof e.CustomEvent){var a=new CustomEvent(t,{bubbles:!0,detail:{anchor:o,toggle:r}});document.dispatchEvent(a)}};return function(a,p){var g,v,y,S,E,b,O,I={};I.cancelScroll=function(e){cancelAnimationFrame(O),O=null,e||h("scrollCancel",g)},I.animateScroll=function(n,r,a){var i=o(g||t,a||{}),c="[object Number]"===Object.prototype.toString.call(n),p=c||!n.tagName?null:n;if(c||p){var v=e.pageYOffset;i.header&&!S&&(S=document.querySelector(i.header)),E||(E=d(S));var y,b,C,w=c?n:l(p,E,parseInt("function"==typeof i.offset?i.offset(n,r):i.offset,10),i.clip),L=w-v,A=s(),H=0,q=function(t,o){var a=e.pageYOffset;if(t==o||a==o||(v<o&&e.innerHeight+a)>=A)return I.cancelScroll(!0),m(n,o,c),h("scrollStop",i,n,r),y=null,O=null,!0},Q=function(t){y||(y=t),H+=t-y,b=H/parseInt(i.speed,10),b=b>1?1:b,C=v+L*u(i,b),e.scrollTo(0,Math.floor(C)),q(C,w)||(O=e.requestAnimationFrame(Q),y=t)};0===e.pageYOffset&&e.scrollTo(0,0),f(n,c,i),h("scrollStart",i,n,r),I.cancelScroll(!0),e.requestAnimationFrame(Q)}};var C=function(t){if(!r()&&0===t.button&&!t.metaKey&&!t.ctrlKey&&"closest"in t.target&&(y=t.target.closest(a))&&"a"===y.tagName.toLowerCase()&&!t.target.closest(g.ignore)&&y.hostname===e.location.hostname&&y.pathname===e.location.pathname&&/#/.test(y.href)){var n=c(i(y.hash)),o=g.topOnEmptyHash&&"#"===n?document.documentElement:document.querySelector(n);o=o||"#top"!==n?o:document.documentElement,o&&(t.preventDefault(),I.animateScroll(o,y))}},w=function(e){if(null!==history.state&&history.state.smoothScroll&&history.state.smoothScroll===JSON.stringify(g)&&history.state.anchor){var t=document.querySelector(c(i(history.state.anchor)));t&&I.animateScroll(t,null,{updateURL:!1})}},L=function(e){b||(b=setTimeout((function(){b=null,E=d(S)}),66))};return I.destroy=function(){g&&(document.removeEventListener("click",C,!1),e.removeEventListener("resize",L,!1),e.removeEventListener("popstate",w,!1),I.cancelScroll(),g=null,v=null,y=null,S=null,E=null,b=null,O=null)},I.init=function(r){if(!n())throw"Smooth Scroll: This browser does not support the required JavaScript methods and browser APIs.";I.destroy(),g=o(t,r||{}),S=g.header?document.querySelector(g.header):null,E=d(S),document.addEventListener("click",C,!1),S&&e.addEventListener("resize",L,!1),g.updateURL&&g.popstate&&e.addEventListener("popstate",w,!1)},I.init(p),I}}));</script><script>;(function() {
  var offset = 0

  if (document.getElementsByClassName('header--fixed').length) {
    offset = document.getElementsByClassName('header--fixed')[0].clientHeight;
  }

  // smooth scrolling to all anchor links
  new window.SmoothScroll('a[href*="#"]', {
    offset: offset
  });
})();
</script></body></html>

<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-78962458-1');
	</script>

	<!-- Hotjar Tracking Code for https://www.oneupapp.io -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:959054,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>


	<title>Getting started with OneUp</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>

	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5T76958');</script>
	<!-- End Google Tag Manager -->
</head>
<meta name="csrf-token" content="{{ csrf_token() }}">

<body>
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5T76958"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	@include('layouts.partials.nav');
    <div class="container" style="margin-top:40px">
				<div class="columns is-centered" style="margin-top:40px;">
						<div class="column is-4">
							<b>To get started:</b>
							<p>1. Connect a social account<br>
								2. Schedule a post</p>
						</div>
				</div>
        <div class="columns is-centered">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/m69nnBkNAtc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div class="columns is-centered is-multiline" style="margin-top:40px;">
            <div class="column is-4 has-text-centered">
                <a class="button is-primary" href="/accounts">Get started by connecting accounts</a>
            </div>
						<br>
        </div>

				<div class="columns is-centered is-multiline" style="margin-top:40px;border-top:1px solid #ccc">
            <div class="column is-4">
              <b> On a mobile device?</b>
							<p>OneUp is optimized for use from a computer. Please login from
								 a laptop or desktop to enable full navigation.<p>
            </div>
						<br>
        </div>
    </div>
</body>
</html>

<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-78962458-1');
	</script>

	<!-- Hotjar Tracking Code for https://www.oneupapp.io -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:959054,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1 , maximum-scale=1.0, user-scalable=no">

		<title>Connected Social Accounts - OneUp App</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
		<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('css/responsive_app.css') }}">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5T76958');</script>
	<!-- End Google Tag Manager -->
</head>
<meta name="csrf-token" content="{{ csrf_token() }}">

<body>
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5T76958"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	@include('layouts.partials.nav' ,['numOfDays' => $numOfDays]);
	<div id="insta_count" style="display:none">{{$insta_account_count}}</div>
	<div id="app" class="container app-container" style="">
		<i class="fa fa-angle-right social-arrow-mbl"></i>
		<div class="columns is-mobile is-multiline-mobile social-icons-columns">
				<div class="column is-2-desktop mybox is-4-mobile" >
					<a href="{{$fb_url}}"><img src={{URL::asset('img/facebook_connect.png')}} style="cursor:pointer;width:155px;height:35px;border-radius:4px"/></a>
				</div>
				<div class="column is-2-desktop mybox is-4-mobile" >
					<a href="{{$twitter_url}}"><img src={{URL::asset('img/twitter_connect.png')}} style="cursor:pointer;width:155px;height:35px;border-radius:4px"/></a>
				</div>
				<div class="column is-2-desktop mybox is-4-mobile" >
					<a class="addPinterestAccount"><img src={{URL::asset('img/pinterest_connect.png')}} style="cursor:pointer;width:155px;height:35px;border-radius:4px"/></a>
				</div>
				<div class="column is-2-desktop mybox is-4-mobile" >
					<a href="{{$linkedin_url}}"><img src={{URL::asset('img/linkedin_connect.png')}} style="cursor:pointer;width:155px;height:35px;border-radius:4px"/></a>
				</div>
				@if($email == 'allowloudstore@gmail.com')
					<div class="column is-2-desktop mybox is-4-mobile" >
						<a class="addInstaAccount"><img src={{URL::asset('img/instagram_connect.png')}} style="cursor:pointer;width:155px;height:35px;border-radius:4px"/></a>
					</div>
				@else
				  @if(true)
						<div class="column is-2-desktop mybox is-4-mobile" >
							<a class="connectInstaPopup"><img src={{URL::asset('img/instagram_connect.png')}} style="cursor:pointer;width:155px;height:35px;border-radius:4px"/></a>
						</div>
					@else
						<div class="column is-2-desktop mybox is-4-mobile" >
							<a class="upgradeAccount"><img src={{URL::asset('img/instagram_connect.png')}} style="cursor:pointer;width:155px;height:35px;border-radius:4px"/></a>
						</div>
					@endif
				@endif
		</div>

		<div class="columns is-multiline google-business-account" >
			<div class="column is-full" style="border-top:1px solid rgb(220,220,220);margin-bottom:10px">
				<p><b>Connect your Google My Business account: </b></p>
				<a href="{{$google_url}}"><img src={{URL::asset('img/connect_google_my_business.jpg')}} style="cursor:pointer;width:155px;height:45px;border-radius:4px;margin-top:30px"/></a>
			</div>
		</div>
		<div class="columns is-multiline" style="margin-top:20px;min-height:40px">
			<div class="column is-full" style="border-top:1px solid rgb(220,220,220);margin-bottom:10px">
				<p><b>Connect your Bitly account: </b></p>
				@if($bitly_username != NULL)
				<div class="column is-3" style="margin-top:20px">
					<div class="card">
					  <header class="card-header">
					    <p class="card-header-title" style="float:left">
					    	{{$bitly_username}}
					    </p>
					  </header>
					  <header class="card-header" style="height:40px">
					  	@if($bitly_enabled == 1)
							<p style="margin-top:5px;margin-left:5px">Link shortening enabled. <a href="/switchbitly">Disable</a></p>
						@else
							<p style="margin-top:5px;margin-left:5px">Link shortening disabled. <a href="/switchbitly">Enable</a></p>
						@endif
					  </header>
					  <footer class="card-footer">
					    <a href="/deletebitly" class="card-footer-item">Delete</a>
					  </footer>
					</div>
				</div>
				@else
				<a href="https://bitly.com/oauth/authorize?client_id=0c4d3211b844f39859c5a1ac8f02f42b51132cdf&redirect_uri=https://www.oneupapp.io/authbitly"><img src={{URL::asset('img/bitly.png')}} style="cursor:pointer;width:165px;height:40px;border-radius:4px;margin-top:30px"/></a>
				@endif
			</div>
		</div>
		<div class="columns is-multiline connected-accounts is-mobile" >
			<div class="column is-7-desktop is-12-mobile" style="border-top:1px solid rgb(220,220,220);margin-bottom:10px">
				<p><b>{{count($accounts)}} Account/s Connected: </b></p>
				@if(count($accounts) == 0)
				  <br>
					<p>Connect at least one social account to get started.</p>
				@else
				  <br/>
					<p class="selectAllAccounts" style="float:left;color:#188EF5;cursor:pointer"><i class="fa fa-long-arrow-down" aria-hidden="true"></i>&nbsp;&nbsp;Select All</p>
					<a class="multipleDeleteModalBtn" style="float:right">Delete selected accounts</a>
				@endif
			</div>

			@for ($i=0; $i < count($accounts);$i++)
			<div class="column is-7-desktop is-12-mobile account-row" style="">
				<div class="columns is-mobile">
					<div class="column is-1">
						  <input type="checkbox" class="social_account_checkbox" id="{{$accounts[$i]['social_account_id']}}">
					</div>
					<div class="column is-2 account-image-column" >
						<div id="mypanel" class="account-image" style="background-image: url({{$accounts[$i]['image_url']}})">
							<img class="sn-icon" style="margin-left:-4px;margin-top:-4px" src="https://www.oneupapp.io/img/{{$accounts[$i]['social_network_type']}}.png" />
						</div>
					</div>
					<div class="column is-5">
						<p class="account-name" >
							<b>{{$accounts[$i]['username']}}</b>
							@if($accounts[$i]['social_network_type']==0 && $accounts[$i]['account_type'] == 0)
	             <a href="https://www.facebook.com"{{$accounts[$i]['social_account_id']}} target="blank">(Profile)</a>
	            @endif
	            @if($accounts[$i]['social_network_type']==0 && $accounts[$i]['account_type'] == 1)
	             <a href="https://www.facebook.com/{{$accounts[$i]['social_account_id']}}" target="blank">(Page)</a>
	            @endif
	            @if($accounts[$i]['social_network_type']==0 && $accounts[$i]['account_type'] == 2)
	             <a href="https://www.facebook.com/{{$accounts[$i]['social_account_id']}}" target="blank">(Group)</a>
	            @endif
						</p>
					</div>
					<div class="column is-1 is-hidden-touch"></div>
					<div class="column is-2" style="margin-top:10px;">
						@if($accounts[$i]['social_network_type']==0)
							<a href="{{$fb_url}}">Refresh</a>
						@endif
						@if($accounts[$i]['social_network_type']==1)
							<a href="{{$twitter_url}}">Refresh</a>
						@endif
						@if($accounts[$i]['social_network_type']==3)
							<a href="{{$google_url}}">Refresh</a>
						@endif
						@if($accounts[$i]['social_network_type']==4)
							<a href="{{$linkedin_url}}">Refresh</a>
						@endif
						@if($accounts[$i]['social_network_type']==5)
							<a class="addPinterestAccount">Refresh</a>
						@endif
						@if($accounts[$i]['social_network_type']==6)
							<a href="{{$google_url}}">Refresh</a>
						@endif
					</div>
					<div class="column is-3 is-hidden-mobile-edit" style="margin-top:10px;">
						<a class="delete_modal" sn-id="{{$accounts[$i]['social_account_id']}}" style="margin-left:-50px;color:#CD5C5C">| Disconnect</a>
					</div>
					<div class="delete-modal-div">
						<a class="delete_modal" sn-id="{{$accounts[$i]['social_account_id']}}" style="color:#CD5C5C;"> Disconnect</a>
					</div>
				</div>
			</div>
			<div class="column is-5"></div>
			@endfor
		</div>
	</div>
	<div class="modal upgradeModal">
			<div class="modal-background"></div>
			<div class="modal-card">
				<section class="modal-card-body">
					Instagram is not available in the free trial. If interested in using Instagram, you can upgrade your account to a paid plan in the settings.<br /> OneUp offers a 7-day money back guarantee if you are not satisfied.
				</section>
				<footer class="modal-card-foot">
					<button class="button cancelbtn">OK</button>
				</footer>
			</div>
	</div>
	<div class="modal extensionModal">
			<div class="modal-background"></div>
			<div class="modal-card">
				<header class="modal-card-head">
					<p class="modal-card-title">Connect your Instagram account</p>
					<button class="delete cancelbtn" aria-label="close"></button>
				</header>
				<section class="modal-card-body">
					To connect your Instagram account, first download the <b>OneUp Account Connect</b> Chrome extension by clicking this icon:
					<br /><br />
					<a href="https://chrome.google.com/webstore/detail/oneup-account-connect/hmedghgcpbbocpgeakpmcnbpblkaefcm" target="blank" style="margin-left:120px"><img src="{{URL::asset('img/ChromeWebStore_1.png')}}" /></a>
					<br /><br />
					After, go to Instagram.com, login to your Instagram account, then press the OneUp Chrome extension icon to connect your Instagram account to OneUp.
					<br/><br/>
					<b><a href="https://oneup.crisp.help/en-us/article/instagram-help-125nk8/" target="_blank">Watch Video Tutorial</a></b>
					<br/><br/>
					<i>Please note this is a different extension from the OneUp image scheduling extension.</i>
				</section>
				<footer class="modal-card-foot">
					<button class="button cancelbtn">Cancel</button>
				</footer>
			</div>
	</div>

	<div class="modal deletemodal">
		  <div class="modal-background"></div>
		  <div class="modal-card">
		    <header class="modal-card-head">
		      <p class="modal-card-title">Confirm</p>
		      <button class="delete cancelbtn" aria-label="close"></button>
		    </header>
		    <section class="modal-card-body">
		      Remove this account? This will also delete all scheduled posts associated with this account.
		    </section>
		    <footer class="modal-card-foot">
		      <button class="button is-danger deletebtn" sn-id="" >Delete</button>
		      <button class="button cancelbtn">Cancel</button>
		    </footer>
		  </div>
	</div>

	<div class="modal bulkDeleteModal">
		  <div class="modal-background"></div>
		  <div class="modal-card">
		    <header class="modal-card-head">
		      <p class="modal-card-title">Confirm</p>
		      <button class="delete cancelbtn" aria-label="close"></button>
		    </header>
		    <section class="modal-card-body bulkDeleteMessage">
		    </section>
		    <footer class="modal-card-foot">
		      <button class="button is-danger bulkDeleteBtn" sn-id="" >Delete</button>
		      <button class="button cancelbtn">Cancel</button>
		    </footer>
		  </div>
	</div>


	<div class="modal refreshmodal">
		  <div class="modal-background"></div>
		  <div class="modal-card">
		    <header class="modal-card-head">
		      <p class="modal-card-title">Confirm</p>
		      <button class="delete cancelbtn" aria-label="close"></button>
		    </header>
		    <section class="modal-card-body">
				<div class="field">
				  <p class="control has-icons-left has-icons-right">
				    <input class="input" id="insta_refresh_username" placeholder="Instagram username">
				    <span class="icon is-small is-left">
				      <i class="fa fa-envelope"></i>
				    </span>
				  </p>
				</div>
				<div class="field">
				  <p class="control has-icons-left">
				    <input class="input" type="password" id="insta_refresh_pass" placeholder="Password">
				    <span class="icon is-small is-left">
				      <i class="fa fa-lock"></i>
				    </span>
				  </p>
				</div>
		    </section>
		    <footer class="modal-card-foot">
		      <button class="button is-primary refreshInstaAccountBtn" sn-id="" >Update Account</button>
		      <button class="button cancelbtn">Cancel</button>
		    </footer>
		  </div>
	</div>
	<div class="insta_limit" style="display:none">{{$insta_limit}}</div>

	<div class="modal verifyinstamodal">
		  <div class="modal-background"></div>
		  <div class="modal-card">
		    <header class="modal-card-head">
		      <p class="modal-card-title">Enter code sent to the email connected to this Instagram account</p>
		      <button class="delete cancelbtn" aria-label="close"></button>
		    </header>
		    <section class="modal-card-body">
				<div class="field">
				  <p class="control has-icons-left">
				    <input class="input" type="password" id="insta_verify_code" placeholder="Enter 6 digit code sent to your email id">
				    <span class="icon is-small is-left">
				      <i class="fa fa-lock"></i>
				    </span>
				  </p>
				</div>
		    </section>
		    <footer class="modal-card-foot">
		      <button class="button is-primary verifyInstaAccountBtn" sn-id="" >Verify Account</button>
		      <button class="button cancelbtn">Cancel</button>
		    </footer>
		  </div>
	</div>

	<div class="modal addInstagramAccountModal">
		  <div class="modal-background"></div>
		  <div class="modal-card">
		    <header class="modal-card-head">
		      <p class="modal-card-title">Confirm</p>
		      <button class="delete cancelbtn" aria-label="close"></button>
		    </header>
		    <section class="modal-card-body">
				<div class="field">
				  <p class="control has-icons-left has-icons-right">
				    <input class="input" id="insta_username" placeholder="Instagram username">
				    <span class="icon is-small is-left">
				      <i class="fa fa-envelope"></i>
				    </span>
				  </p>
				</div>
				<div class="field">
				  <p class="control has-icons-left">
				    <input class="input" type="password" id="insta_pass" placeholder="Password">
				    <span class="icon is-small is-left">
				      <i class="fa fa-lock"></i>
				    </span>
				  </p>
				</div>
				<p> 1) We do not store your password.</p><br>
				<p>2) Make sure you have turned off "Two-factor authentication".</p><br>
				<p>3) Please do not close this window during verification process.</p>
		    </section>
		    <footer class="modal-card-foot">
		      <button class="button is-primary addInstaAccountBtn" sn-id="" >Connect Account</button>
		      <button class="button cancelbtn">Cancel</button>
		    </footer>
		  </div>
	</div>

	<div class="modal addPinterestAccountModal">
		  <div class="modal-background"></div>
		  <div class="modal-card">
		    <header class="modal-card-head">
		      <p class="modal-card-title">Confirm</p>
		      <button class="delete cancelbtn" aria-label="close"></button>
		    </header>
		    <section class="modal-card-body">
				<div class="field">
				  <p class="control has-icons-left has-icons-right">
				    <input class="input" id="pinterest_username" placeholder="Pinterest Email">
				    <span class="icon is-small is-left">
				      <i class="fa fa-envelope"></i>
				    </span>
				  </p>
				</div>
				<div class="field">
				  <p class="control has-icons-left">
				    <input class="input" type="password" id="pinterest_pass" placeholder="Password">
				    <span class="icon is-small is-left">
				      <i class="fa fa-lock"></i>
				    </span>
				  </p>
				</div>
				<p> Note: We do not store your password.</p>
				<br>
				<p>* Make sure that your Pinterest password does not contain any special characters.</p>
				<br>
				<p>* If you are getting "Invalid username and password" response, try to connect the account after 24 hours.</p>

		    </section>
		    <footer class="modal-card-foot">
		      <button class="button is-primary addPinterestBtn" sn-id="" >Connect Account</button>
		      <button class="button cancelbtn">Cancel</button>
		    </footer>
		  </div>
	</div>
</body>
<script src="{{URL::asset('js/app.js')}}"></script>

<script>
$(document).ready(function(e){
	//Bulk Delete of accounts
	let selectedAccounts = [];

	$('.social_account_checkbox').change(function() {
		selectedAccounts = [];
		$(".social_account_checkbox:checked").each(function(){
		   selectedAccounts.push($(this).attr('id'));
		});
		console.log(selectedAccounts);
	});

	$(".selectAllAccounts").click(function(e){
		selectedAccounts = [];
		$(".social_account_checkbox").prop("checked", true);
		$(".social_account_checkbox:checked").each(function(){
		   selectedAccounts.push($(this).attr('id'));
		});
		console.log(selectedAccounts);
	});

	$(".multipleDeleteModalBtn").click(function(e){
		if(selectedAccounts.length == 0){
			alert("You have not chosen any accounts. Select which account(s) you want to delete by clicking the checkbox.");
			return;
		}

		$(".bulkDeleteMessage").text("You are about to delete "+selectedAccounts.length+" account(s). This will also delete all scheduled posts associated with these accounts. Are you sure you want to delete?");
		if($(".bulkDeleteModal").hasClass("is-active")){
			$(".bulkDeleteModal").removeClass("is-active");
		}
		else{
			$(".bulkDeleteModal").addClass("is-active");
		}
	});


	$(".delete_modal").click(function(e){
		if($(".deletemodal").hasClass("is-active")){
			$(".deletemodal").removeClass("is-active");
			$(".deletebtn").attr("sn-id","");
		}
		else{
			$(".deletemodal").addClass("is-active");
			$(".deletebtn").attr("sn-id", $(this).attr("sn-id"));
		}
	});

	$(".refresh_modal").click(function(e){
		if($(".refreshmodal").hasClass("is-active")){
			$(".refreshmodal").removeClass("is-active");
		}
		else{
			$(".refreshmodal").addClass("is-active");
			$("#insta_refresh_username").val($(this).attr("sn-id"));
			//$(".deletebtn").attr("sn-id", $(this).attr("sn-id"));
		}
	});

	$(".deletebtn").click(function(e){
		$(this).addClass("is-loading");
		window.location.href = "/deleteaccount?id="+$(this).attr("sn-id");
	});

	$(".bulkDeleteBtn").click(function(e){
		if(selectedAccounts.length == 0) return;
		$(this).addClass('is-loading');
		$.ajax({
			url: "/bulkdeleteaccounts",
			data: {
				accounts: selectedAccounts,
				_token:$('meta[name="csrf-token"]').attr('content')
			},
		  type: 'POST',
			success: function(result){
				location.reload();
			}
	  });
	});

	$(".cancelbtn").click(function(e){
		$(".deletemodal").removeClass("is-active");
		$(".addInstagramAccountModal").removeClass("is-active");
		$(".refreshmodal").removeClass("is-active");
		$(".verifyinstamodal").removeClass("is-active");
		$(".addPinterestAccountModal").removeClass("is-active");
		$(".bulkDeleteModal").removeClass("is-active");
		$(".extensionModal").removeClass("is-active");
		$(".upgradeModal").removeClass("is-active");

	});

	//Instagram
	$(".addInstaAccount").click(function(e){
		if($(".addInstagramAccountModal").hasClass("is-active")){
			$(".addInstagramAccountModal").removeClass("is-active");
		}
		else{
			$(".addInstagramAccountModal").addClass("is-active");
		}
	});

	$(".upgradeAccount").click(function(e){
		$(".upgradeModal").addClass("is-active");
	});

	$(".connectInstaPopup").click(function(e){
		$(".extensionModal").addClass("is-active");
	});

	$(".addPinterestAccount").click(function(e){

		if($(".addPinterestAccountModal").hasClass("is-active")){
			$(".addPinterestAccountModal").removeClass("is-active");
		}
		else{
			$(".addPinterestAccountModal").addClass("is-active");
		}

	});

	/************* Insta ****************************************/
	$(".addInstaAccountBtn").click(function(e){
		var username = $("#insta_username").val();
		var password = $("#insta_pass").val();
		$(this).addClass('is-loading');
		var that = this;
    	$.ajax({
    		url: "/connectinstagram",
            data: {
              username: username,
              password: password,
              _token:$('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            success: function(result){
            	$(that).removeClass('is-loading');
            	if(result == 0)
            		alert("This account already exists. Please delete first.");
            	if(result == 1){
            		$(".addInstagramAccountModal").removeClass("is-active");
            		location.reload();
            	}
            	if(result == 2)
            		alert("Invalid username or password. Please check credentials and try again.");

            	if(result == 3)
            		alert("We have detected that Instagram asked you to verify this account. Please log in to Instagram and verify your account and try reconnecting again. OneUp will be INACTIVE until you update your Instagram account.");

            	if(result == 5){
            		$(".addInstagramAccountModal").removeClass("is-active");
            		$(".verifyinstamodal").addClass("is-active");
            	}
            	if(result == 10)
            		alert("You have connected maximum number of Instagram accounts allowed under your plan. Please contact support.");

            	if(result == 7)
            		alert("Invalid username or password. If your credentials are correct, please contact support to get it resolved.");
            }
    	});
	});

	$(".refreshInstaAccountBtn").click(function(e){
		var username = $("#insta_refresh_username").val();
		var password = $("#insta_refresh_pass").val();
		$(this).addClass('is-loading');
		var that = this;
    	$.ajax({
    		url: "/refreshinstagram",
            data: {
              username: username,
              password: password,
              _token:$('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            success: function(result){
            	$(that).removeClass('is-loading');

            	if(result == 1){
            		location.reload();
            	}
            	if(result == 2)
            		alert("Invalid username or password. Please check credentials and try again.");

            	if(result == 5){
            		$(".addInstagramAccountModal").removeClass("is-active");
            		$(".verifyinstamodal").addClass("is-active");
            	}
            	if(result == 6){
            		alert("Code entered by you is invalid. Please enter a valid code");
            		return;
            	}
            	if(result == 7){
            		alert("We are facing some issue while connecting your account. Please contact support.");
            		return;
            	}
            }
    	});
	});

	$(".verifyInstaAccountBtn").click(function(e){
		var inputCode = $("#insta_verify_code").val();
		if(inputCode == '' || inputCode.length == 0)
			return;

		$(this).addClass('is-loading');
		var that = this;

		//make ajax call
    	$.ajax({
    		url: "/verifyinstagram",
            data: {
              code: inputCode,
              _token:$('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            success: function(result){
            	$(that).removeClass('is-loading');
            	if(result == 0)
            		alert("This account does not exists. Please add first.");
            	if(result == 1){
            		$(".addInstagramAccountModal").removeClass("is-active");
            		location.reload();
            	}
            	if(result == 2)
            		alert("Invalid username or password. Please check credentials and try again.");

            	if(result == 6)
            		alert("Code entered by you is invalid. Please enter a valid code");
            }
    	});

	});


	/********************** Pinterest Logic *******************************************/

	$(".addPinterestBtn").click(function(e){
		var email = $("#pinterest_username").val();
		var password = $("#pinterest_pass").val();

		if(email == '' || password == '')
		  return;

		//make AJAX call
		$(this).addClass('is-loading');
		var that = this;
    $.ajax({
  		url: "/connectpinterest",
      data: {
        email: email,
        password: password,
        _token:$('meta[name="csrf-token"]').attr('content')
      },
      type: 'POST',
      success: function(result){
      	$(that).removeClass('is-loading');

      	if(result == 0)
      		alert("This account is already connected to other profile. Please delete first.");

      	if(result == 1){
      		location.reload();
      	}

      	if(result == -1)
      		alert("Invalid username or password. Please check credentials and try again.");
      }
    });
	});
});

</script>

</html>

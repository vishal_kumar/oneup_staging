<html>
<head>
	<title>Change/Cancel plan - OneUp App</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>

	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
.navbar { position: fixed !important; top: 0; left: 0; right: 0; z-index: 1030;}
.active{background: #188EF5;color:#fff;}
#mypanel{
    width:60px;
    height:60px;
    border:1px solid #EAEAEA;
    margin-left:15px;
    background-position:center;
    background-size: 100%;
    cursor: pointer
}
.sn-icon{
    display:block;
    float:right;
    width:20px;
    height:20px;
    margin-top: -10px;
    margin-right: -10px;
    background-position:center;
    background-size: 100%;
    background-image:url("{{ asset('img/twitter.png') }}");
}
</style>
<body>
	@include('layouts.partials.nav');
    <div class="container" style="margin-top:80px">
     Your current plan: {{ Auth::user()->plan==''?' Free Trial':Auth::user()->plan}}
     <br><br>
		 <a href="/subscribe?upgrade=true">Click here to upgrade to a higher plan.</a>
		 <br><br>
     <p style="opacity:0.8">To downgrade or cancel your plan, please email us at: davis@oneupapp.io</p>

    </div>
</body>
</html>

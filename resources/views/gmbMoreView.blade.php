
<!DOCTYPE html><html lang=""><head>
  <script
    async
    src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"
  ></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-78962458-1');
  </script>

  <meta charSet="utf-8"/><meta http-equiv="x-ua-compatible" content="ie=edge"/><title>Schedule Google My Business posts for $4/month</title><meta name="description" content="Schedule Google My Business posts with OneUp. OneUp is a Google My Business post scheduler and automates GMB posts."/><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/><link rel="canonical" href="https://oneup.landen.co/google-my-business-scheduler"/><link rel="icon" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/><link rel="stylesheet" href="https://cdn.landen.co/6vh800wvro81/assets/main.48150a57.css"/><style tyle="text/css">body {
      font-family: 'Roboto', Arial, serif;
      font-size: 14px;
      font-weight: 400;
      --color-dark: rgba(17,17,17,1);
      --color-dark-secondary: rgba(17,17,17,1);
      --color-dark-link: undefined;
      --wrapper-width: 1030px;
    }
.section:not(.dark), .section:not(.dark) .color-text { color: #fff; --color-secondary: #fff; }
.section.dark, .section.dark .color-text { color: rgba(17,17,17,1); --color-secondary: rgba(17,17,17,1); }
.section:not(.dark) .color { color: #fff; }
.section.dark .color { color: rgba(17,17,17,1); }
.section:not(.dark) { --color: #fff; --color-link: #fff; }
.section.dark { --color: rgba(17,17,17,1); --color-link: rgba(17,17,17,1); }
.weight-text { font-weight: 400; }
.weight-text-m { font-weight: 500; }
.weight-text-h { font-weight: 700; }
.weight-title { font-weight: 400; }
.weight-title-m { font-weight: 400; }
.weight-title-h { font-weight: 400; }
.font-title { font-family: 'Varela Round', Arial, serif; }
a:not(.btn), .link {
      color: #fff;
      --link-hover-bg: rgba(255, 255, 255, 0.05);
      --link-hover: #cccccc;
    }
.dark a:not(.btn), .dark .link {
      color: rgba(17,17,17,1);
      --link-hover-bg: rgba(17, 17, 17, 0.05);
      --link-hover: rgb(0, 0, 0);
    }

      a, .link { text-decoration: none; font-weight: 500; }
      a:not(.btn):hover { text-decoration: underline }
.wr { max-width: 1030px; max-width: var(--wr-max); }</style><link href="https://fonts.googleapis.com/css?family=Varela+Round|Roboto:400,500,700" rel="stylesheet"/></head><body><header id="header" class="section header" style="--wr-max:1030px"><div class="wr color"><a id="headerLogo" href="/" class="header__logo" style="font-weight:400">OneUp</a><nav id="headerNav" class="header__nav"><div class="headerNav__links"><a href="https://www.oneupapp.io/price">Pricing</a></div><div class="header__navCtas"><a href="https://www.oneupapp.io/register" class="btn btn--b btn--primary" style="color:rgba(17,17,17,1);background-color:#fff">Sign Up</a></div></nav><nav id="headerMenu" class="header__nav header__nav--hidden"><div id="headerMenuButton" class="burger">Menu</div></nav></div><nav id="headerDrawer" class="headerMenu col-dark-sec"><div id="headerDrawerBackdrop" class="headerMenu__backdrop"></div><div class="headerMenu__wrapper" style="background-color:rgb(20, 112, 213)"><button id="headerDrawerClose" class="headerMenu__close btn"></button><ul class="headerMenu__links"><li><a href="https://www.oneupapp.io/price" class="drawerLink">Pricing </a></li><li><a href="https://www.oneupapp.io/register" class="drawerLink col-dark">Sign Up <svg width="13" height="12" xmlns="http://www.w3.org/2000/svg"><path d="M9.557 7H1a1 1 0 1 1 0-2h8.586L7.007 2.421a1 1 0 0 1 1.414-1.414l4.243 4.243c.203.202.3.47.292.736a.997.997 0 0 1-.292.735L8.42 10.964A1 1 0 1 1 7.007 9.55L9.557 7z" fill="currentColor"></path></svg></a></li></ul></div></nav></header><script>(function() {
  var buffer = 10;
  var margin = 60 + 40 + buffer;
  // button
  var button = document.getElementById('headerMenuButton');
  var buttonWrapper = document.getElementById('headerMenu');
  // drawer
  var drawer = document.getElementById('headerDrawer');
  var drawerBackdrop = document.getElementById('headerDrawerBackdrop');
  var drawerClose = document.getElementById('headerDrawerClose');
  // header UI
  var logo = document.getElementById('headerLogo');
  var nav = document.getElementById('headerNav');
  var logoWidth = logo.offsetWidth;
  var navWidth = nav.offsetWidth;

  var determineVisibility = function() {
    var parent = window.innerWidth;
    if (logoWidth + navWidth > parent - margin) {
      nav.classList.add('header__nav--hidden');
      buttonWrapper.classList.add('headerMenu--visible');
    } else {
      nav.classList.remove('header__nav--hidden');
      buttonWrapper.classList.remove('headerMenu--visible');
    }
  };

  var showDrawer = function() {
    drawer.classList.add('headerMenu--visible');
  };

  var hideDrawer = function() {
    drawer.classList.remove('headerMenu--visible');
  };

  button.onclick = showDrawer;
  drawerBackdrop.onclick = hideDrawer;
  drawerClose.onclick = hideDrawer;

  window.addEventListener('resize', determineVisibility);
  determineVisibility();
})();
</script><div id="hero" class="section section--hero section--noPadding section--center" style="background-color:rgba(48,138,236,1);--wr-max:1030px"><div class="ft ft--center ft--isFirst ft--twoBg" style="--pdx-padding-top:1;--pdx-padding-bottom:1"><div class="ft__half"><div class="ft__wrapper" style="max-width:485px"><div class="ft__content"><h1 class="hero__title color weight-title-h font-title">Schedule and automatically repeat your Google My Business posts</h1><h2 class="hero__subtitle weight-text">Automate GMB posts and stand out in Google search results for only $4/month.</h2><div class="hero__ctas"><a href="https://www.oneupapp.io/register" class="btn btn--b btn--primary btn--large" style="color:rgba(17,17,17,1);background-color:#fff">Start free trial</a></div><div class="appStores"></div><div class="hero__ctaInfo">No credit card required</div></div></div></div><div class="ft__half pdx" style="background-color:rgba(255, 255, 255, 1.0);--pdx-maxheight:0.56"><div class="pdxItem pdxItem--img-transparent"><img src="https://landen.imgix.net/6vh800wvro81/assets/kio13n6g.png?w=1200&amp;h=900&amp;fit=max" srcSet="https://landen.imgix.net/6vh800wvro81/assets/kio13n6g.png?w=1200&amp;h=900&amp;fit=max 2x" alt="google-my-business-logo-800x450.png"/></div></div></div></div><div id="zigzag" class="section section--zigzag section--center dark" style="background-color:rgba(255,255,255,1);padding-bottom:60px;padding-top:60px;--wr-max:1030px"><div class="wr"><h2 class="color weight-title-h font-title section__title center standalone">The process is simple</h2><div class="zigzag"><div class="zigzagItem zigzagItem--right"><div class="zigzagItem__content"><span class="zigzagItem__number">#1</span><h3 class="zigzagItem__title color weight-text-m">Step one</h3><div class="zigzagItem__text"><p>Connect your Google My Business accounts in OneUp.</p></div></div><div class="zigzagItem__graphic"><img src="https://landen.imgix.net/6vh800wvro81/assets/rub240mf.png?w=350&amp;h=350" class="media media--image" srcSet="https://landen.imgix.net/6vh800wvro81/assets/rub240mf.png?w=700&amp;h=700 2x"/></div></div><div class="zigzagItem zigzagItem--left"><div class="zigzagItem__graphic"><img src="https://landen.imgix.net/6vh800wvro81/assets/gm1bsbhi.png?w=350&amp;h=350" class="media media--image" srcSet="https://landen.imgix.net/6vh800wvro81/assets/gm1bsbhi.png?w=700&amp;h=700 2x"/></div><div class="zigzagItem__content"><span class="zigzagItem__number">#2</span><h3 class="zigzagItem__title color weight-text-m">Step two</h3><div class="zigzagItem__text"><p>Create your GMB posts with any text, image, link, and Call-To-Action button you want. </p></div></div></div><div class="zigzagItem zigzagItem--right"><div class="zigzagItem__content"><span class="zigzagItem__number">#3</span><h3 class="zigzagItem__title color weight-text-m">Step three</h3><div class="zigzagItem__text"><p>Choose if you want to post it once, or have it automatically repeat at set intervals.</p></div></div><div class="zigzagItem__graphic"><img src="https://landen.imgix.net/6vh800wvro81/assets/lk69jjku.png?w=350&amp;h=350" class="media media--image" srcSet="https://landen.imgix.net/6vh800wvro81/assets/lk69jjku.png?w=700&amp;h=700 2x"/></div></div><div class="zigzagItem zigzagItem--left"><div class="zigzagItem__graphic"><img src="https://landen.imgix.net/6vh800wvro81/assets/tjhl49wh.png?w=350&amp;h=350" class="media media--image" srcSet="https://landen.imgix.net/6vh800wvro81/assets/tjhl49wh.png?w=700&amp;h=700 2x"/></div><div class="zigzagItem__content"><span class="zigzagItem__number">#4</span><h3 class="zigzagItem__title color weight-text-m">What it looks like in a Google search</h3><div class="zigzagItem__text"><p>Now you have a fine looking Google My Business post, automated to whatever level you please.</p></div></div></div></div></div></div><div id="hero" class="section section--hero section--noPadding section--center" style="background-color:rgba(48,138,236,1);--wr-max:1030px"><div class="ft ft--center ft--twoBg" style="--pdx-padding-top:1;--pdx-padding-bottom:1"><div class="ft__half"><div class="ft__wrapper" style="max-width:485px"><div class="ft__content"><h1 class="hero__title color weight-title-h font-title">Schedule and automatically repeat your Google My Business posts</h1><h2 class="hero__subtitle weight-text">Stand out in Google search results for only $4/month.</h2><div class="hero__ctas"><a href="https://www.oneupapp.io/register" class="btn btn--b btn--primary btn--large" style="color:rgba(17,17,17,1);background-color:#fff">Start free trial</a></div><div class="appStores"></div><div class="hero__ctaInfo">No credit card required</div></div></div></div><div class="ft__half pdx" style="background-color:rgba(255, 255, 255, 1.0);--pdx-maxheight:0.56"><div class="pdxItem pdxItem--img-transparent"><img src="https://landen.imgix.net/6vh800wvro81/assets/kio13n6g.png?w=1200&amp;h=900&amp;fit=max" srcSet="https://landen.imgix.net/6vh800wvro81/assets/kio13n6g.png?w=1200&amp;h=900&amp;fit=max 2x" alt="google-my-business-logo-800x450.png"/></div></div></div></div><div id="media" class="section section--grid section--left dark" style="background-color:rgba(255,255,255,1);padding-bottom:60px;padding-top:60px;--wr-max:1030px"><div class="wr"><h2 class="color weight-title-h font-title section__title left standalone">Watch this video to see how it works:</h2><iframe width="860" height="415" src="https://www.youtube.com/embed/E9vSzjd88Fg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div><div id="content" class="section section--content section--left dark section--znncirc6mv" style="background-color:rgba(255,255,255,1);padding-bottom:60px;padding-top:60px;--wr-max:1030px;font-size:16px"><div class="wr"><h2 class="color weight-title-h font-title section__title left standalone">Unfamiliar with Google My Business?</h2><div class="color textBody mkd textBody--left textBody--dark"><p><em>This post walks through what Google My Business is, the benefits of using it, how to post, and some of the other features. If you are already familiar with Google My Business, jump down to the <strong>How to schedule Google My Business posts</strong> section.</em></p>
<hr>
<p>One of the newest ways to market your business is through <a href="https://www.google.com/business/">Google My Business</a>, sometimes referred to as GMB.</p>
<p>Google My Business allows you to stand out in Google search results by sharing photos, announcing sales or specials, posting what’s new, managing reviews, and keeping your contact information and open hours current.</p>
<p>You have probably seen Google My Business results in your Google searches, even though you may not have realized it.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*-fGfJJecnjmXMZ7iAUXcWg.png" alt="Image"></p>
<p>This is what a Google My Business post looks like on mobile.</p>
<hr>
<h4 id="should-i-be-using-google-my-business-">Should I be using Google My Business?</h4>
<br/>

<p>In short, yes. Google My Business is a free way to stand out in Google search results.</p>
<p>Because Google My Business is relatively new, most businesses are still not using it yet. For those who start using GMB early, you can get a huge leg up on your competition.</p>
<br/>


<blockquote>
<p>You are most likely already posting on social media, right?</p>
</blockquote>
<br/>

<p>Well unless you are paying to boost your posts, your audience will probably be limited to only people who already follow you.</p>
<p>However, with Google My Business, your posts have the opportunity to reach people outside of your existing audience.</p>
<hr>
<h4 id="does-posting-on-google-my-business-impact-my-search-rankings-">Does posting on Google My Business impact my search rankings?</h4>
<br/>

<p>According to <a href="https://searchengineland.com/google-posts-impact-ranking-case-study-285668">this study by Search Engine Journal</a>, posting on Google My Business has a positive impact on your Google search results ranking.</p>
<p>In addition to helping with Google search rankings, updating your Google My Business listing can lead to people choosing you over a competitor.</p>
<br/>

<p>Let’s take a look at a hypothetical example:</p>
<blockquote>
<p>Suppose you own a restaurant. It’s Monday and the weather is bad, so you know business is going to be slow this evening. You decide to run an extended happy hour deal from 4pm until close. You announce the deal on your Facebook and Instagram pages. Now you wait for all the customers to flock over. Right?</p>
</blockquote>
<br/>

<p><em>But what if someone who doesn’t follow you on social media is Googling a few different restaurants, trying to figure out where to eat tonight?</em></p>
<br/>

<p>When they Google your restaurant name, they have no idea about your special happy hour deal. They end up eating and drinking at another restaurant 😢</p>
<p>That would be bad… but good thing you actually posted the extended happy hour deal on Google My Business as well. Those people end of seeing the deal when they Google your business’s name, and end up choosing your restaurant because of it ! 😀</p>
<hr>
<h4 id="how-to-post-on-google-my-business">How to post on Google My Business</h4>
<br/>

<p>So we’ve established that posting on Google My Business is important. Now let’s look at how to post on Google My Business.</p>
<ol>
<li>Sign up — You can do so here:<a href="https://www.google.com/business/">https://www.google.com/business/</a></li>
<li>Verify your business — Google will send you a physical postcard with a unique verification number to ensure that your address is legitimate.</li>
<li>Post</li>
</ol>
<br/>

<p>When creating a new post, you will have 4 different posting options:</p>
<ul>
<li>What’s new posts: Posts that provide general information about your business. You can include a photo/video, link, Call-to-Action (CTA) button, and other information. For example, a restaurant could make a post to promote a new menu item.</li>
</ul>
<br/>

<ul>
<li>Event posts: Posts that promote an event your business is involved with or supports. Event posts require a title, start/end dates and time, and may include a photo/video, CTA button, and other information. For example, a bookstore may advertise a local author’s book signing or a book club meeting.</li>
</ul>
<br/>

<ul>
<li>Offer posts: Posts that provide promotional sales or offers from your business. Offer posts require a title and start/end dates and times. A “View offer” CTA button is automatically added to the post. You can also include a photo/video, coupon code, link, and terms and conditions with the post. For example, a pizza parlor may advertise a 20% off large pizzas for a week.</li>
</ul>
<br/>

<ul>
<li>Product posts: Posts that emphasize a specific product your business sells. Product posts require a title and photo or video. You can also include a CTA button and other information. For example, an electronics store may promote a new phone for sale.</li>
</ul>
<p><img src="https://cdn-images-1.medium.com/max/800/1*J-5k6KQFqHTUUY5S7D7cxw.png" alt="Image"></p>
<br/>

<p>You can also add a button to your post if you wish, which you can link to whatever you’d like.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*Ij9TZaVdfJmGIkZe6yqCBQ.png" alt="Image"></p>
<br/>

<p>And add a link to your button.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*a27uZWhXiVpOJelL6X1o5w.png" alt="Image"></p>
<p>Here are a couple example posts that used the Order Online button:</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*T1M1HH_q-KeVX75dc308Fg.png" alt="Image"></p>
<p>Overall, it’s a fairly simple process for posting on Google My Business.</p>
<p>Here is what your Google My Business post will look like on the Google search results page:</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*CKUl_quxMxCA8xAvSxLTOw.png" alt="Image"></p>
<br/>

<hr>
<h4 id="ideas-on-what-to-post">Ideas on what to post</h4>
<br/>

<p>So now that you know how to post on Google My Business, the question is what to post. Here are some ideas:</p>
<ul>
<li>High-quality images of your product, food, location, service, etc.</li>
<li>Offer a special discount for searchers</li>
<li>Sales and promotions</li>
<li>Updated hours for holidays</li>
<li>Promote an upcoming event</li>
<li>Share a link to a new blog post</li>
<li>Share a customer testimonial</li>
<li>Call attention to an ad campaign you’re promoting elsewhere (print, billboard, radio)</li>
</ul>
<p>Here are some examples of GMB posts provided by Google:</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*ZvGH9STlJwPoHw4SFmoYYA.png" alt="Image"></p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*dN6HTfe-mv81UPdjRktXug.png" alt="Image"></p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*IZUC6xHp3hoOOCIvyMsl-w.png" alt="Image"></p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*gm8ABWwxN-5f1IH_Y77HXw.png" alt="Image"></p>
<br/>

<hr>
<h4 id="5-keys-to-writing-a-post">5 keys to writing a post</h4>
<br/>

<ol>
<li>Choose a suitable post type: Is your post about an event? An offer or time-sensitive deal? Do you have news to share? Consider what you want your post to do — whether to encourage customers to visit your store, sell something, or announce a new feature.</li>
<li>Photos: Take a high-resolution photo reinforcing your message so your post stands out. Photos should be simple, direct, in-focus and well-lit, with bright, vibrant colors. Photos should have a minimum resolution of 400px wide by 300px tall, in JPG or PNG format.</li>
<li>Title (if your post is an event): Describe your event in 4–5 words. You have up to 58 characters for your title.</li>
<li>More details: Be clear about your offer/event. You have up to 1,500 characters for the description of your post, but the ideal length is between 150–300 characters.</li>
<li>Call to action: Include instructions such as “Buy,” “Book online,” “Learn more,” “Call,” or “Visit.”</li>
</ol>
<hr>
<h4 id="writing-a-google-post-do-s-and-don-ts">Writing a Google Post: Do’s and Don’ts</h4>
<h5 id="what-to-do">What to do</h5>
<ul>
<li>Be precise: What are the 3 things your customer needs to know? What do you want them to remember, for how much, and when?
<em>“Happy Hour! Half-price milkshakes from 5–6 PM every Friday.”</em></li>
</ul>
<br/>


<ul>
<li>Be personal: Show what your business values:<br><em>“We love families at Mike’s and to show our appreciation, kids eat free this weekend!”</em></li>
</ul>
<br/>


<ul>
<li>Tell your customers what they can do. Are you selling a product? Tell them how they can buy.<br><em>“Tickets range from $60-$160, and are available for purchase at the front desk starting at 12 PM EST today.”</em></li>
</ul>
<br/>

<ul>
<li>Highlight what makes your business, product, or offer unique. Large selection? Free shipping? Tell people:<br><em>“Free shipping on orders over $50.”</em></li>
</ul>
<br/>


<ul>
<li>Be timely <em>— _use a key selling point or popular item as the hook for your post:<br>_“Spring is here. All flip flops now 30% off.”</em></li>
</ul>
<br/>

<ul>
<li>Be sure to include any redemption instructions, unique codes, or restrictions on offers or sales:<br><em>“$10 off purchase of $50 or more. 50% off select women’s clothing.”</em></li>
</ul>
<br/>


<ul>
<li>Use abbreviations for days and months, and don’t use periods, to allow more space for your post:<br><em>Jan, Feb, Mar… Mon, Tue, Wed…</em></li>
</ul>
<br/>


<ul>
<li>Abbreviate hours in this way:<br><em>9 AM, 5 PM, 12 PM, 12 AM</em></li>
</ul>
<br/>

<h5 id="what-not-to-do">What NOT to do</h5>
<ul>
<li>Don’t use commercial slang:<br><em>“BOGO: 50% off men’s sneakers.”</em></li>
</ul>
<br/>


<ul>
<li>Don’t use excessive exclamation marks or all caps:<br><em>“Crazy SALE today!!!”</em></li>
</ul>
<br/>

<ul>
<li>Don’t include more than one theme or offer in one communication:<br><em>“Half price coffee and tea from 3–6 PM on Fridays, and buy 6, get one free donut on weekdays.”</em></li>
</ul>
<br/>

<ul>
<li>Don’t craft a deal with too many exclusions:<br><em>“10% off new seasonal sandwiches. Not valid on lunch specials.”</em></li>
</ul>
<br/>

<hr>
<h4 id="be-aware-that-google-my-business-posts-expire-after-7-days">Be aware that Google My Business posts expire after 7 days</h4>
<br/>

<p>When you post something on Google My Business, it will show in search results for only 7 days.</p>
<p>The only exception to this is Event posts, which stay until the date of the event passes.</p>
<p>When your post is about to expire, you will get an automated email from Google My Business.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*8VfhTqE6ZN7TUqKxGdpFiQ.png" alt="Image"></p>
<br/>


<hr>
<h4 id="insights">Insights</h4>
<p>On each Google My Business post, you can see the views and clicks that the post has received.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/0*L0wqgqY93lBgEwZ-.png" alt="Image"></p>
<p>Post Insights help you better understand how your posts perform with potential customers. From the Posts tab, you can view insights for:</p>
<ul>
<li>An individual post</li>
<li>All posts from the last week</li>
<li>All posts from the last month</li>
</ul>
<h5 id="queries">Queries</h5>
<p>In addition, Google My Business shows you the actual search terms people are using to find your business. This is an extremely valuable insight.</p>
<p><img src="https://cdn-images-1.medium.com/max/1000/1*fF57JV8klsGFX9CIze-GCA.png" alt="Image"></p>
<p>Search queries show the terms that your customers used to find your business on Local Search and Maps.</p>
<p>You can filter the search queries by the last week, month, or quarter.</p>
<hr>
<h4 id="other-features">Other Features</h4>
<br/>

<p><a href="http://blog.oneupapp.io/schedule-google-my-business-posts/">Google My Business</a> has so many other features that might be of value to businesses that it is hard to cover everything in just one post.</p>
<p>Here is a quick review of some of GMB’s other features:</p>
<h5 id="messaging">Messaging</h5>
<p>If enabled, Google My Business allows customers who are viewing your listing to message you directly. This can significantly minimize the number of phone calls or emails received.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*Xign_MQicGDWzvoR2S3Nlg.png" alt="Image"></p>
<h5 id="website">Website</h5>
<p>Google My Business wants to get everyone online, including businesses that cannot afford a website, or aren’t tech-savvy enough to build a website.</p>
<p>So they created a simple website builder within GMB.</p>
<p><img src="https://cdn-images-1.medium.com/max/1000/1*KtQV7_o2MIsqcolk19VPUw.png" alt="Image"></p>
<br/>

<br/>

<blockquote>
<p><em>But I already have a website. Do I need a Google My Business website?</em></p>
</blockquote>
<br/>

<p>Good question. While it’s definitely not necessary, <a href="https://onlinemarketinginct.com/2018/09/07/why-you-should-create-a-gmb-site-even-if-you-already-have-a-website/">many people believe that having a GMB website helps</a>. Google likes when you use Google products.</p>
<p>Here is what <a href="https://www.vpmarketinggroup.com/google-my-business-websites/">Vantage Point Marketing</a> concluded about using a Google My Businesses website:</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*cvGMa592s0iwrpIbWFe0_g.png" alt="Image"></p>
<br/>

<h5 id="create-ads">Create Ads</h5>
<p>You can create ads directly in Google My Business.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*yegiPBGB8jYpfWNUwu3DBg.png" alt="Image"></p>
<br/>

<h5 id="add-users">Add Users</h5>
<p>Do you have multiple people on your team? If so, you can add multiple users and assign different permission levels on GMB.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*ND6KfSxAfQsEJBOng7y4lQ.png" alt="Image"></p>
<br/>

<h5 id="add-locations">Add locations</h5>
<p>For businesses with multiple locations, Google My Business allows you to add up to 10 locations manually.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/1*pucrprMYxH31umcA0rebFw.png" alt="Image"></p>
<p>If you have more than 10 locations, you can request bulk verification by following <a href="https://support.google.com/business/answer/4490296?hl=en&amp;authuser=0">these steps</a>.</p>
<br/>

<hr>
<h4 id="how-to-schedule-google-my-business-posts">How to schedule Google My Business posts</h4>
<br/>

<p>The fact that your Google My Business posts expire after 7 days means that you have to constantly go back into GMB and post something week after week, again and again.</p>
<p>The solution: schedule Google My Business posts weeks or even months ahead of time.</p>
<p>The issue, however, is that very few social media schedulers work with Google My Business, and the ones that do work charge upwards of $50/month, which is a big hit to take for many businesses.</p>
<br/>

<h4 id="oneup">OneUp</h4>
<p>Using <a href="https://www.oneupapp.io/?utm_source=HowtoschedulepoststoGoogleMyBusiness&amp;utm_medium=blog">OneUp</a>, you can schedule Google My Business posts for only $4/month.</p>
<p>The process is simple. Just connect your Google My Business account on the Accounts page.</p>
<p><img src="https://cdn-images-1.medium.com/max/1000/1*mDmtByQsDcRzQQ0_hvMxPw.png" alt="Image"></p>
<br/>

<p>Then schedule your Google My Business post, optionally adding any link or image to the post.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/0*PGpozLDwgMXzrhbk" alt="Image"></p>
<br/>

<p>Then add your Call-To-Action button and a link.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/0*s4w8g3qCn2m2GjTe" alt="Image"></p>
<br/>

<p>In addition to just scheduling Google My Business posts, OneUp allows you to set GMB posts to automatically repeat at set intervals — such as once a week or once a month.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/0*hnYgHxiSASM6h6TJ" alt="Image"></p>
<br/>

<p>This allows you to reap the benefits of being active on Google My Business, without the hassle of constantly going back in every week to manually post something once your post expires.</p>
<p>Then choose to post it now, or schedule it for a date in the future.</p>
<p><img src="https://cdn-images-1.medium.com/max/800/0*MRvLSYUcYKE_raw6" alt="Image"></p>
<br/>

<p>Here is what the post looks like inside your Google My Business account:</p>
<p><img src="https://cdn-images-1.medium.com/max/800/0*ydFC9h3uks2jxQZq" alt="Image"></p>
<br/>

<p>And here is what the post looks like on the Google search results page:</p>
<p><img src="https://cdn-images-1.medium.com/max/800/0*FZCIkqNOjllnWFR4" alt="Image"></p>
</div></div></div><div id="hero" class="section section--hero section--noPadding section--center" style="background-color:rgba(48,138,236,1);--wr-max:1030px"><div class="ft ft--center ft--noPdx" style="--pdx-padding-top:1;--pdx-padding-bottom:1"><div class="ft__half"><div class="ft__wrapper" style="max-width:485px"><div class="ft__content"><h1 class="hero__title color weight-title-h font-title">Schedule and automatically repeat your Google My Business posts</h1><h2 class="hero__subtitle weight-text">Only $4/month for up to 10 accounts.</h2><div class="hero__ctas"><a href="https://www.oneupapp.io/register" class="btn btn--b btn--primary btn--large" style="color:rgba(17,17,17,1);background-color:#fff">Start free trial</a><a href="https://www.oneupapp.io/price" class="btn btn--b btn--secondary btn--large" style="color:#fff">Pricing</a></div><div class="hero__ctaInfo">No credit card required</div></div></div></div></div></div><footer id="footer" class="section section--footer section--center dark" style="background-color:rgba(255,255,255,1);padding-bottom:15px;padding-top:15px;--wr-max:1030px"><div class="wr"><div class="footer footer--simple"><div class="footer__primary"><span>© 2019 OneUp App</span></div></div></div></footer><script>/*! smooth-scroll v14.2.1 | (c) 2018 Chris Ferdinandi | MIT License | http://github.com/cferdinandi/smooth-scroll */
!(function(e,t){"function"==typeof define&&define.amd?define([],(function(){return t(e)})):"object"==typeof exports?module.exports=t(e):e.SmoothScroll=t(e)})("undefined"!=typeof global?global:"undefined"!=typeof window?window:this,(function(e){"use strict";var t={ignore:"[data-scroll-ignore]",header:null,topOnEmptyHash:!0,speed:500,clip:!0,offset:0,easing:"easeInOutCubic",customEasing:null,updateURL:!0,popstate:!0,emitEvents:!0},n=function(){return"querySelector"in document&&"addEventListener"in e&&"requestAnimationFrame"in e&&"closest"in e.Element.prototype},o=function(){for(var e={},t=0;t<arguments.length;t++)!(function(t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])})(arguments[t]);return e},r=function(t){return!!("matchMedia"in e&&e.matchMedia("(prefers-reduced-motion)").matches)},a=function(t){return parseInt(e.getComputedStyle(t).height,10)},i=function(e){var t;try{t=decodeURIComponent(e)}catch(n){t=e}return t},c=function(e){"#"===e.charAt(0)&&(e=e.substr(1));for(var t,n=String(e),o=n.length,r=-1,a="",i=n.charCodeAt(0);++r<o;){if(0===(t=n.charCodeAt(r)))throw new InvalidCharacterError("Invalid character: the input contains U+0000.");t>=1&&t<=31||127==t||0===r&&t>=48&&t<=57||1===r&&t>=48&&t<=57&&45===i?a+="\\"+t.toString(16)+" ":a+=t>=128||45===t||95===t||t>=48&&t<=57||t>=65&&t<=90||t>=97&&t<=122?n.charAt(r):"\\"+n.charAt(r)}var c;try{c=decodeURIComponent("#"+a)}catch(e){c="#"+a}return c},u=function(e,t){var n;return"easeInQuad"===e.easing&&(n=t*t),"easeOutQuad"===e.easing&&(n=t*(2-t)),"easeInOutQuad"===e.easing&&(n=t<.5?2*t*t:(4-2*t)*t-1),"easeInCubic"===e.easing&&(n=t*t*t),"easeOutCubic"===e.easing&&(n=--t*t*t+1),"easeInOutCubic"===e.easing&&(n=t<.5?4*t*t*t:(t-1)*(2*t-2)*(2*t-2)+1),"easeInQuart"===e.easing&&(n=t*t*t*t),"easeOutQuart"===e.easing&&(n=1- --t*t*t*t),"easeInOutQuart"===e.easing&&(n=t<.5?8*t*t*t*t:1-8*--t*t*t*t),"easeInQuint"===e.easing&&(n=t*t*t*t*t),"easeOutQuint"===e.easing&&(n=1+--t*t*t*t*t),"easeInOutQuint"===e.easing&&(n=t<.5?16*t*t*t*t*t:1+16*--t*t*t*t*t),e.customEasing&&(n=e.customEasing(t)),n||t},s=function(){return Math.max(document.body.scrollHeight,document.documentElement.scrollHeight,document.body.offsetHeight,document.documentElement.offsetHeight,document.body.clientHeight,document.documentElement.clientHeight)},l=function(t,n,o,r){var a=0;if(t.offsetParent)do{a+=t.offsetTop,t=t.offsetParent}while(t);return a=Math.max(a-n-o,0),r&&(a=Math.min(a,s()-e.innerHeight)),a},d=function(e){return e?a(e)+e.offsetTop:0},f=function(e,t,n){t||history.pushState&&n.updateURL&&history.pushState({smoothScroll:JSON.stringify(n),anchor:e.id},document.title,e===document.documentElement?"#top":"#"+e.id)},m=function(t,n,o){0===t&&document.body.focus(),o||(t.focus(),document.activeElement!==t&&(t.setAttribute("tabindex","-1"),t.focus(),t.style.outline="none"),e.scrollTo(0,n))},h=function(t,n,o,r){if(n.emitEvents&&"function"==typeof e.CustomEvent){var a=new CustomEvent(t,{bubbles:!0,detail:{anchor:o,toggle:r}});document.dispatchEvent(a)}};return function(a,p){var g,v,y,S,E,b,O,I={};I.cancelScroll=function(e){cancelAnimationFrame(O),O=null,e||h("scrollCancel",g)},I.animateScroll=function(n,r,a){var i=o(g||t,a||{}),c="[object Number]"===Object.prototype.toString.call(n),p=c||!n.tagName?null:n;if(c||p){var v=e.pageYOffset;i.header&&!S&&(S=document.querySelector(i.header)),E||(E=d(S));var y,b,C,w=c?n:l(p,E,parseInt("function"==typeof i.offset?i.offset(n,r):i.offset,10),i.clip),L=w-v,A=s(),H=0,q=function(t,o){var a=e.pageYOffset;if(t==o||a==o||(v<o&&e.innerHeight+a)>=A)return I.cancelScroll(!0),m(n,o,c),h("scrollStop",i,n,r),y=null,O=null,!0},Q=function(t){y||(y=t),H+=t-y,b=H/parseInt(i.speed,10),b=b>1?1:b,C=v+L*u(i,b),e.scrollTo(0,Math.floor(C)),q(C,w)||(O=e.requestAnimationFrame(Q),y=t)};0===e.pageYOffset&&e.scrollTo(0,0),f(n,c,i),h("scrollStart",i,n,r),I.cancelScroll(!0),e.requestAnimationFrame(Q)}};var C=function(t){if(!r()&&0===t.button&&!t.metaKey&&!t.ctrlKey&&"closest"in t.target&&(y=t.target.closest(a))&&"a"===y.tagName.toLowerCase()&&!t.target.closest(g.ignore)&&y.hostname===e.location.hostname&&y.pathname===e.location.pathname&&/#/.test(y.href)){var n=c(i(y.hash)),o=g.topOnEmptyHash&&"#"===n?document.documentElement:document.querySelector(n);o=o||"#top"!==n?o:document.documentElement,o&&(t.preventDefault(),I.animateScroll(o,y))}},w=function(e){if(null!==history.state&&history.state.smoothScroll&&history.state.smoothScroll===JSON.stringify(g)&&history.state.anchor){var t=document.querySelector(c(i(history.state.anchor)));t&&I.animateScroll(t,null,{updateURL:!1})}},L=function(e){b||(b=setTimeout((function(){b=null,E=d(S)}),66))};return I.destroy=function(){g&&(document.removeEventListener("click",C,!1),e.removeEventListener("resize",L,!1),e.removeEventListener("popstate",w,!1),I.cancelScroll(),g=null,v=null,y=null,S=null,E=null,b=null,O=null)},I.init=function(r){if(!n())throw"Smooth Scroll: This browser does not support the required JavaScript methods and browser APIs.";I.destroy(),g=o(t,r||{}),S=g.header?document.querySelector(g.header):null,E=d(S),document.addEventListener("click",C,!1),S&&e.addEventListener("resize",L,!1),g.updateURL&&g.popstate&&e.addEventListener("popstate",w,!1)},I.init(p),I}}));</script><script>;(function() {
  var offset = 0

  if (document.getElementsByClassName('header--fixed').length) {
    offset = document.getElementsByClassName('header--fixed')[0].clientHeight;
  }

  // smooth scrolling to all anchor links
  new window.SmoothScroll('a[href*="#"]', {
    offset: offset
  });
})();
</script></body></html>

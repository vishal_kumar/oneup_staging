<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-78962458-1');
      </script>
            
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>OneUp App</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #000;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $(document).ready(function(e){
           $(".btn").click(function(e){
                var obj = {};
                var all_data = [];
                obj.content = $("#desc").val();
                obj.source_url = $("#image_url").val();
                obj.image_url = $("#source_url").val();
                all_data.push(obj);
                $.ajax({
                  url: "/insertpost",
                  headers: {'X-CSRF-TOKEN': $('#token').val()},
                  data: {posts:all_data},
                  type: 'POST',
                  success: function(result){
                  }
                });

           });
        });
    </script>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">


                <div class="col-md-6">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input id="desc" placeholder="Description" />
                    <input id="image_url" placeholder="image_url" />
                    <input id="source_url" placeholder="source_url" />
                    <button class="btn btn-primary">
                        Add Post
                    </button>
                </div>
            </div>
        </div>
    </body>
</html>

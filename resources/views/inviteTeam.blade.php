<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-78962458-1');
	</script>

	<!-- Hotjar Tracking Code for https://www.oneupapp.io -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:959054,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

	<title>Invite Team Members - OneUp App</title>
  <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5T76958');</script>
	<!-- End Google Tag Manager -->
</head>
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
.navbar { position: fixed !important; top: 0; left: 0; right: 0; z-index: 1030;}
.active{background: #188EF5;color:#fff;}
</style>

<body>
  @include('layouts.partials.nav');
	@if($isAuthorised === false)
	<div id="app" class="container" style="margin-top:80px;margin-left:55px;min-height:600px;">
		<p>You are not authorised to access this page. Only Admins can access this page.</p>
	</div>
	@else
  <div id="app" class="container" style="margin-top:80px;margin-left:55px;min-height:600px;">
		<div class="columns is-centered is-multiline">
      <div class="column is-3" style="height:40px;">
        <input class="input is-rounded" id="teamEmail" type="text" placeholder="Enter Team Member's Email Address...">
      </div>
      <div class="column is-2">
        <a class="button is-info" id="sendInviteBtn">Send Invite</a>
      </div>
      <div class="column is-10" style="border-bottom:1px solid #ccc"></div>
      <div class="column is-8" style="margin-top:5px;display:flex;align-items:center; justify-content:center">
        <b>Current Members</b>
      </div>
      <div class="column is-8">
        @if(count($teamMembers) !=0)
          <table class="table is-fullwidth">
            <thead>
              <tr>
                <th>Email</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @for ($i=0; $i < count($teamMembers);$i++)
              <tr>
                <td>{{$teamMembers[$i]['invitedTo']}}</td>
                <td>{{$teamMembers[$i]['status']}}</td>
                <td><a class="button is-danger removeTeamMemberBtn" data-id="{{$teamMembers[$i]['invitedTo']}}">Remove</a></td>
              </tr>
              @endfor
            </tbody>
          </table>
        @else
          <p>You have not invited any team members yet.</p>
        @endif
      </div>
    </div>
  </div>
	@endif
</body>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
  $(document).ready(function(e){
    $("#sendInviteBtn").click(function(e){
      var teamEmail = $("#teamEmail").val();

      if(teamEmail.trim() == '') return;
      $("#sendInviteBtn").addClass("is-loading");
      $.ajax({
        url: "/addteammember",
        data: {email: teamEmail},
        type: 'POST',
        success: function(result){
          $("#sendInviteBtn").removeClass("is-loading");

          if(result == -1){
            alert("This account is already connected. Please check the email.");
            return;
          }
          location.reload();
        }
      });
    });

    $(".removeTeamMemberBtn").click(function(e){
      var email = $(this).attr('data-id');
      $(this).addClass("is-loading");

      $.ajax({
        url: "/removeteammember",
        data: {email: email},
        type: 'POST',
        success: function(result){
          location.reload();
        }
      });
    });
  });
</script>

<html>
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-78962458-1');
  </script>

  <title>OneUp - Jobs</title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <link rel="stylesheet" type="text/css" href="css/semantic.css">
  <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>

</head>
<body>

  <div class="ui grid">
      <div class="three wide column" style="height:80px; background-color:#188EF5;border-bottom: 2px solid #c8c8c8">
          <a href="/">
            <img class="ui circular image" src="img/oneup.jpeg" style="width:55px; height:55px;margin-top:5px;margin-left:15%">
          </a>
      </div>
      <div class="thirteen wide column" style="height:80px; background-color:#188EF5;border-bottom: 2px solid #c8c8c8">
          <a href="/"><p style="float:right;margin-right:2%; color:#fff;line-height:60px;">Go Back</p></a>
      </div>
  </div>
  <div class="ui grid centered">
    <p style="font-size:32px;margin-top:40px"><b>Jobs</b></p>
  </div>
  <div class="ui grid centered">
    <p style="font-size:16px;margin-top:20px"><b>We work in a fast-paced environment with a lot of talented and creative people. <br>If you’re interested in joining our growing team, check out these opportunities:</b>
    </p>
  </div>
  <div class="ui grid centered">
       <p style="margin-top:25px;font-size:20px;color:#188EF5">Content Writer</p>
  </div>
  <div class="ui grid centered">
       <p style="margin-top:5px;font-size:20px;color:#188EF5">Full Stack Developer</p>
  </div>
  <div class="ui grid centered">
       <p style="margin-top:5px;font-size:20px;color:#188EF5">Backend Developer (Ruby on Rails)</p>
  </div>

  <div class="ui grid centered">
       <p style="margin-top:35px;font-size:16px">If you’d like to apply, <a href="mailto:support@oneupapp.io" target="_top">send us an email</a>, including links to your CV/résumé.
  </div>

  <div class="ui grid centered">
       <p style="margin-top:10px;color:#6e6e6e">Excited to work with us, but don’t see your role listed? We’re open to speculative applications, so get in touch and pitch yourself to us!</p>
  </div>
</body>

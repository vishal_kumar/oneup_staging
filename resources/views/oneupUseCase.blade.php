
<!DOCTYPE html><html lang=""><head><meta charSet="utf-8"/><meta http-equiv="x-ua-compatible" content="ie=edge"/><title>OneUp Case Study</title><meta name="description" content="At $49/month, Meet Edgar is not affordable for most small businesses. Recycle your social media posts with OneUp for $4/month."/><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/><link rel="canonical" href="https://www.oneupapp.io/oneup-vs-meet-edgar"/><link rel="icon" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/><link rel="stylesheet" href="https://cdn.landen.co/6vh800wvro81/assets/main.f6819c1f.css"/><style tyle="text/css">body {
      font-family: 'Roboto', Arial, serif;
      font-size: 14px;
      font-weight: 400;
      --color-dark: rgba(17,17,17,1);
      --color-dark-secondary: rgba(17,17,17,1);
      --color-dark-link: undefined;
      --wrapper-width: 1030px;
    }
.section:not(.dark), .section:not(.dark) .color-text { color: #fff; --color-secondary: #fff; }
.section.dark, .section.dark .color-text { color: rgba(17,17,17,1); --color-secondary: rgba(17,17,17,1); }
.section:not(.dark) .color { color: #fff; }
.section.dark .color { color: rgba(17,17,17,1); }
.section:not(.dark) { --color: #fff; --color-link: #fff; }
.section.dark { --color: rgba(17,17,17,1); --color-link: rgba(17,17,17,1); }
.weight-text { font-weight: 400; }
.weight-text-m { font-weight: 500; }
.weight-text-h { font-weight: 700; }
.weight-title { font-weight: 400; }
.weight-title-m { font-weight: 400; }
.weight-title-h { font-weight: 400; }
.font-title { font-family: 'Varela Round', Arial, serif; }
a:not(.btn), .link {
      color: #fff;
      --link-hover-bg: rgba(255, 255, 255, 0.05);
      --link-hover: #cccccc;
    }
.dark a:not(.btn), .dark .link {
      color: rgba(17,17,17,1);
      --link-hover-bg: rgba(17, 17, 17, 0.05);
      --link-hover: rgb(0, 0, 0);
    }

      a, .link { text-decoration: none; font-weight: 500; }
      a:not(.btn):hover { text-decoration: underline }
.wr { max-width: 1030px; max-width: var(--wr-max); }</style><link href="https://fonts.googleapis.com/css?family=Varela+Round|Roboto:400,500,700" rel="stylesheet"/><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-138336756-1"></script><script>window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-138336756-1');</script></head><body><header id="header" class="section header" style="--wr-max:1030px"><div class="wr color"><a id="headerLogo" href="/" class="header__logo" style="font-weight:400">OneUp</a><nav id="headerNav" class="header__nav"><div class="headerNav__links"><a href="https://www.oneupapp.io/price">Pricing</a></div><div class="header__navCtas"><a href="https://www.oneupapp.io/register" class="btn btn--b btn--primary" style="color:rgba(17,17,17,1);background-color:#fff">Sign Up</a></div></nav><nav id="headerMenu" class="header__nav header__nav--hidden"><div id="headerMenuButton" class="burger">Menu</div></nav></div><nav id="headerDrawer" class="headerMenu col-dark-sec"><div id="headerDrawerBackdrop" class="headerMenu__backdrop"></div><div class="headerMenu__wrapper" style="background-color:rgb(20, 112, 213)"><button id="headerDrawerClose" class="headerMenu__close btn"></button><ul class="headerMenu__links"><li><a href="https://www.oneupapp.io/price" class="drawerLink">Pricing </a></li><li><a href="https://www.oneupapp.io/register" class="drawerLink col-dark">Sign Up <svg width="13" height="12" xmlns="http://www.w3.org/2000/svg"><path d="M9.557 7H1a1 1 0 1 1 0-2h8.586L7.007 2.421a1 1 0 0 1 1.414-1.414l4.243 4.243c.203.202.3.47.292.736a.997.997 0 0 1-.292.735L8.42 10.964A1 1 0 1 1 7.007 9.55L9.557 7z" fill="currentColor"></path></svg></a></li></ul></div></nav><script>(function() {
  var buffer = 10;
  var margin = 60 + 40 + buffer;
  // button
  var button = document.getElementById('headerMenuButton');
  var buttonWrapper = document.getElementById('headerMenu');
  // drawer
  var drawer = document.getElementById('headerDrawer');
  var drawerBackdrop = document.getElementById('headerDrawerBackdrop');
  var drawerClose = document.getElementById('headerDrawerClose');
  // header UI
  var logo = document.getElementById('headerLogo');
  var nav = document.getElementById('headerNav');
  var logoWidth = logo.offsetWidth;
  var navWidth = nav.offsetWidth;

  var determineVisibility = function() {
    var parent = window.innerWidth;
    if (logoWidth + navWidth > parent - margin) {
      nav.classList.add('header__nav--hidden');
      buttonWrapper.classList.add('headerMenu--visible');
    } else {
      nav.classList.remove('header__nav--hidden');
      buttonWrapper.classList.remove('headerMenu--visible');
    }
  };

  var showDrawer = function() {
    drawer.classList.add('headerMenu--visible');
  };

  var hideDrawer = function() {
    drawer.classList.remove('headerMenu--visible');
  };

  button.onclick = showDrawer;
  drawerBackdrop.onclick = hideDrawer;
  drawerClose.onclick = hideDrawer;

  window.addEventListener('resize', determineVisibility);
  determineVisibility();
})();
</script></header><div id="hero" class="section section--hero section--noPadding section--bottom section--izlaco5vi9o" style="background-color:rgba(48,138,236,1);--pdx-min-height:undefinedpx;--wr-max:1030px;font-size:14px"><div class="divider divider--wave"><svg viewBox="0 0 1695 57" preserveAspectRatio="none"><path d="M0 23c135.4 19 289.6 28.5 462.5 28.5C721.9 51.5 936.7 1 1212.2 1 1395.8.9 1556.7 8.3 1695 23v34H0V23z" fill="rgba(255,255,255,1)" fill-rule="evenodd"></path></svg></div><div class="ft ft--bottom ft--isFirst" style="--pdx-padding-top:1.45;--pdx-padding-bottom:1"><div class="ft__half pdx pdxParent--img-transparent" style="--pdx-maxheight:1"><div class="pdxItem pdxItem--img-transparent"></div></div><div class="ft__half"><div class="ft__wrapper" style="max-width:485px"><div class="ft__content"><h1 class="hero__title color weight-title-h font-title">OneUp Case Study</h1><h2 class="hero__subtitle weight-text">See how this user was able to rank multiple clients at the top of Google by automating Google My Business posts with OneUp.</h2><div class="hero__ctas"></div></div></div></div></div></div><div id="content" class="section section--content section--left dark" style="background-color:rgba(255,255,255,1);padding-bottom:60px;padding-top:60px;--pdx-min-height:0px;--wr-max:1030px"><div class="wr"><h3 class="weight-text section__subtitle left standalone">Kyle runs an SEO agency, and was able to get his client ranked in multiple cities in just a short time by automating Google My Business posts through OneUp. This has led to a 54.94% increase in organic traffic for his client compared to the previous period.</h3><div class="color textBody mkd textBody--left textBody--dark"></div></div></div><div id="media" class="section section--grid section--center dark" style="background-color:rgba(255,255,255,1);padding-bottom:60px;padding-top:60px;--pdx-min-height:0px;--wr-max:1030px"><div class="wr"><h2 class="color weight-title-h font-title section__title center">Watch how Kyle did it here:</h2><h3 class="weight-text section__subtitle center"><iframe width="560" height="315" src="https://www.youtube.com/embed/BT6Yf8Z6kCI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></h3></div></div><div id="hero" class="section section--hero section--noPadding section--bottom section--o34toikqpml" style="background-color:rgba(48,138,236,1);--pdx-min-height:700px;--wr-max:1030px;font-size:14px"><div class="divider divider--tallWave divider--back"><svg viewBox="0 0 1695 563" preserveAspectRatio="none"><path d="M603.6 456C869 387.4 1019 248.1 1242.5 155A3601.5 3601.5 0 0 1 1695 .2V563H0v-5c225.5-22.2 426.7-56.2 603.6-102z" fill="rgba(255,255,255,1)" fill-rule="evenodd"></path></svg></div><div class="ft ft--bottom" style="--pdx-padding-top:1;--pdx-padding-bottom:1"><div class="ft__half pdx pdxParent--undefined" style="--pdx-maxheight:1"><div class="pdxItem pdxItem--undefined" style="border-radius:5px;box-shadow:0 2px 5px 0 rgba(0,0,0,0.15)"></div></div><div class="ft__half"><div class="ft__wrapper" style="max-width:485px"><div class="ft__content"><h1 class="hero__title color weight-title-h font-title">Try out a free 7-day trial of OneUp</h1><h2 class="hero__subtitle weight-text"></h2><div class="hero__ctas"><a href="https://www.oneupapp.io/register" class="btn btn--b btn--primary btn--large" style="color:rgba(17,17,17,1);background-color:#fff">Sign Up</a><a href="https://www.oneupapp.io/price" class="btn btn--b btn--secondary btn--large" style="color:#fff">Pricing</a></div></div></div></div></div></div><footer id="footer" class="section section--footer section--center dark" style="background-color:rgba(255,255,255,1);padding-bottom:15px;padding-top:15px;--pdx-min-height:0px;--wr-max:1030px"><div class="wr"><div class="footer footer--simple"><div class="footer__primary"><span></span></div></div></div></footer><script>/*! smooth-scroll v14.2.1 | (c) 2018 Chris Ferdinandi | MIT License | http://github.com/cferdinandi/smooth-scroll */
!(function(e,t){"function"==typeof define&&define.amd?define([],(function(){return t(e)})):"object"==typeof exports?module.exports=t(e):e.SmoothScroll=t(e)})("undefined"!=typeof global?global:"undefined"!=typeof window?window:this,(function(e){"use strict";var t={ignore:"[data-scroll-ignore]",header:null,topOnEmptyHash:!0,speed:500,clip:!0,offset:0,easing:"easeInOutCubic",customEasing:null,updateURL:!0,popstate:!0,emitEvents:!0},n=function(){return"querySelector"in document&&"addEventListener"in e&&"requestAnimationFrame"in e&&"closest"in e.Element.prototype},o=function(){for(var e={},t=0;t<arguments.length;t++)!(function(t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])})(arguments[t]);return e},r=function(t){return!!("matchMedia"in e&&e.matchMedia("(prefers-reduced-motion)").matches)},a=function(t){return parseInt(e.getComputedStyle(t).height,10)},i=function(e){var t;try{t=decodeURIComponent(e)}catch(n){t=e}return t},c=function(e){"#"===e.charAt(0)&&(e=e.substr(1));for(var t,n=String(e),o=n.length,r=-1,a="",i=n.charCodeAt(0);++r<o;){if(0===(t=n.charCodeAt(r)))throw new InvalidCharacterError("Invalid character: the input contains U+0000.");t>=1&&t<=31||127==t||0===r&&t>=48&&t<=57||1===r&&t>=48&&t<=57&&45===i?a+="\\"+t.toString(16)+" ":a+=t>=128||45===t||95===t||t>=48&&t<=57||t>=65&&t<=90||t>=97&&t<=122?n.charAt(r):"\\"+n.charAt(r)}var c;try{c=decodeURIComponent("#"+a)}catch(e){c="#"+a}return c},u=function(e,t){var n;return"easeInQuad"===e.easing&&(n=t*t),"easeOutQuad"===e.easing&&(n=t*(2-t)),"easeInOutQuad"===e.easing&&(n=t<.5?2*t*t:(4-2*t)*t-1),"easeInCubic"===e.easing&&(n=t*t*t),"easeOutCubic"===e.easing&&(n=--t*t*t+1),"easeInOutCubic"===e.easing&&(n=t<.5?4*t*t*t:(t-1)*(2*t-2)*(2*t-2)+1),"easeInQuart"===e.easing&&(n=t*t*t*t),"easeOutQuart"===e.easing&&(n=1- --t*t*t*t),"easeInOutQuart"===e.easing&&(n=t<.5?8*t*t*t*t:1-8*--t*t*t*t),"easeInQuint"===e.easing&&(n=t*t*t*t*t),"easeOutQuint"===e.easing&&(n=1+--t*t*t*t*t),"easeInOutQuint"===e.easing&&(n=t<.5?16*t*t*t*t*t:1+16*--t*t*t*t*t),e.customEasing&&(n=e.customEasing(t)),n||t},s=function(){return Math.max(document.body.scrollHeight,document.documentElement.scrollHeight,document.body.offsetHeight,document.documentElement.offsetHeight,document.body.clientHeight,document.documentElement.clientHeight)},l=function(t,n,o,r){var a=0;if(t.offsetParent)do{a+=t.offsetTop,t=t.offsetParent}while(t);return a=Math.max(a-n-o,0),r&&(a=Math.min(a,s()-e.innerHeight)),a},d=function(e){return e?a(e)+e.offsetTop:0},f=function(e,t,n){t||history.pushState&&n.updateURL&&history.pushState({smoothScroll:JSON.stringify(n),anchor:e.id},document.title,e===document.documentElement?"#top":"#"+e.id)},m=function(t,n,o){0===t&&document.body.focus(),o||(t.focus(),document.activeElement!==t&&(t.setAttribute("tabindex","-1"),t.focus(),t.style.outline="none"),e.scrollTo(0,n))},h=function(t,n,o,r){if(n.emitEvents&&"function"==typeof e.CustomEvent){var a=new CustomEvent(t,{bubbles:!0,detail:{anchor:o,toggle:r}});document.dispatchEvent(a)}};return function(a,p){var g,v,y,S,E,b,O,I={};I.cancelScroll=function(e){cancelAnimationFrame(O),O=null,e||h("scrollCancel",g)},I.animateScroll=function(n,r,a){var i=o(g||t,a||{}),c="[object Number]"===Object.prototype.toString.call(n),p=c||!n.tagName?null:n;if(c||p){var v=e.pageYOffset;i.header&&!S&&(S=document.querySelector(i.header)),E||(E=d(S));var y,b,C,w=c?n:l(p,E,parseInt("function"==typeof i.offset?i.offset(n,r):i.offset,10),i.clip),L=w-v,A=s(),H=0,q=function(t,o){var a=e.pageYOffset;if(t==o||a==o||(v<o&&e.innerHeight+a)>=A)return I.cancelScroll(!0),m(n,o,c),h("scrollStop",i,n,r),y=null,O=null,!0},Q=function(t){y||(y=t),H+=t-y,b=H/parseInt(i.speed,10),b=b>1?1:b,C=v+L*u(i,b),e.scrollTo(0,Math.floor(C)),q(C,w)||(O=e.requestAnimationFrame(Q),y=t)};0===e.pageYOffset&&e.scrollTo(0,0),f(n,c,i),h("scrollStart",i,n,r),I.cancelScroll(!0),e.requestAnimationFrame(Q)}};var C=function(t){if(!r()&&0===t.button&&!t.metaKey&&!t.ctrlKey&&"closest"in t.target&&(y=t.target.closest(a))&&"a"===y.tagName.toLowerCase()&&!t.target.closest(g.ignore)&&y.hostname===e.location.hostname&&y.pathname===e.location.pathname&&/#/.test(y.href)){var n=c(i(y.hash)),o=g.topOnEmptyHash&&"#"===n?document.documentElement:document.querySelector(n);o=o||"#top"!==n?o:document.documentElement,o&&(t.preventDefault(),I.animateScroll(o,y))}},w=function(e){if(null!==history.state&&history.state.smoothScroll&&history.state.smoothScroll===JSON.stringify(g)&&history.state.anchor){var t=document.querySelector(c(i(history.state.anchor)));t&&I.animateScroll(t,null,{updateURL:!1})}},L=function(e){b||(b=setTimeout((function(){b=null,E=d(S)}),66))};return I.destroy=function(){g&&(document.removeEventListener("click",C,!1),e.removeEventListener("resize",L,!1),e.removeEventListener("popstate",w,!1),I.cancelScroll(),g=null,v=null,y=null,S=null,E=null,b=null,O=null)},I.init=function(r){if(!n())throw"Smooth Scroll: This browser does not support the required JavaScript methods and browser APIs.";I.destroy(),g=o(t,r||{}),S=g.header?document.querySelector(g.header):null,E=d(S),document.addEventListener("click",C,!1),S&&e.addEventListener("resize",L,!1),g.updateURL&&g.popstate&&e.addEventListener("popstate",w,!1)},I.init(p),I}}));</script><script>;(function() {
  var offset = 0

  if (document.getElementsByClassName('header--fixed').length) {
    offset = document.getElementsByClassName('header--fixed')[0].clientHeight;
  }

  // smooth scrolling to all anchor links
  new window.SmoothScroll('a[href*="#"]', {
    offset: offset
  });
})();
</script></body></html>

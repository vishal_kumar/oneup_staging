<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-78962458-1');
	</script>
	<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="ea53bead-ca28-42ee-9b50-908fe4f414cb";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
	<!-- Hotjar Tracking Code for https://www.oneupapp.io -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:959054,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

	<title>Queue - OneUp App</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1 , maximum-scale=1.0, user-scalable=no">
  <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>

	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('css/responsive_app.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5T76958');</script>
	<!-- End Google Tag Manager -->
</head>
<style>
.navbar { position: fixed !important; top: 0; left: 0; right: 0; z-index: 1030;}
.active{background: #188EF5;color:#fff;}
#mypanel{
    width:40px;
    height:40px;
    border:1px solid #EAEAEA;
		margin-top: 20px;
    margin-right:25px;
		background-position:center;
		background-size: 100%;
    cursor: pointer
}
#sn-icon{
    display:block;
    float:right;
    width:20px;
    height:20px;
    margin-top: -10px;
    margin-right: -10px;
    background-position:center;
    background-size: 100%;
}
</style>
<body>
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5T76958"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	@include('layouts.partials.nav');
	<div id="app" class="container" style="margin-top:80px">
		<queue usertz="{{$timezone}}"></queue>
	</div>
</body>

<script src="{{URL::asset('js/app.js?v=1.3.5')}}"></script>
</script>
</html>

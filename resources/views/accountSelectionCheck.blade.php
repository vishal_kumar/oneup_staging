<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-78962458-1');
	</script>

	<!-- Hotjar Tracking Code for https://www.oneupapp.io -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:959054,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

	<title>Select Accounts - OneUp App</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5T76958');</script>
	<!-- End Google Tag Manager -->
</head>
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
.navbar { position: fixed !important; top: 0; left: 0; right: 0; z-index: 1030;}
.active{background: #188EF5;color:#fff;}
#mypanel{
    width:50px;
    height:50px;
    border:1px solid #EAEAEA;
    margin-left:15px;
    background-position:center;
    background-size: 100%;
    cursor: pointer
}
#sn-icon{
    display:block;
    float:right;
    width:20px;
    height:20px;
		margin-top: -10px;
    margin-right: -10px;
    background-position:center;
    background-size: 100%;
}
.mybox:hover{
	opacity: 0.8
}
input[type="checkbox"] {
  zoom: 1.7;
}
</style>

<body>
  <div id="app" class="container" style="margin-top:80px;margin-left:55px;min-height:600px;">
    <label style="margin-left:-15px"><b>Select Accounts you want to connect to OneUp: </b></label>
		<br><br>
		<input type="checkbox" class="social_account_select_all">
    <div class="columns is-multiline" style="margin-top:30px;height:600px;overflow:scroll;border:2px solid #ccc">
      @for ($i=0; $i < count($accounts);$i++)
      <div class="column is-12 accountsRow" style="height:75px;border-bottom:1px solid #ccc;margin-bottom:25px;margin-top:10px">
				<div class="columns">
					<div class="column is-1">
						  <input type="checkbox" name="{{$accounts[$i]['social_network_id']}}" {{$accounts[$i]['isAlreadyConnected']?"checked":""}} class="social_account_checkbox">
					</div>
          <div class="column is-2" >
						<div id="mypanel" style="background-image: url({{$accounts[$i]['image_url']}})">
							<img class="sn-icon" style="margin-left:-4px;margin-top:-4px" src="https://www.oneupapp.io/img/{{$accounts[$i]['social_network_type']}}.png" />
						</div>
					</div>
          <div class="column is-5">
            <p style="float:left;margin-top:10px;margin-left:-40px">
              <b>{{$accounts[$i]['accountName']}} (Profile)</b>
            </p>
          </div>
        </div>
      </div>
      @endfor
    </div>
    <button class="button is-primary connectAccounts" style="margin-left:-15px;margin-bottom: 30px">Connect Selected Account(s)</button>
  </div>
</body>
<script src="{{URL::asset('js/app.js')}}"></script>

<script>
$(document).ready(function(e){
	let accountsToBeDeleted = [];
	let self = this;

	$(".social_account_select_all").click(function(e){
		$(".social_account_checkbox").prop("checked", $(this).prop("checked"));
	});

  $(".connectAccounts").click(function(e){
		accountsToBeDeleted = [];
    $('input[type="checkbox"]').each(function() {
      if (!$(this).is(':checked')) {
				accountsToBeDeleted.push($(this).attr('name'));
      }
    });

		$(this).addClass('is-loading');
		$.ajax({
			url: "/bulkdeleteaccounts",
			data: {
				accounts: accountsToBeDeleted,
				_token:$('meta[name="csrf-token"]').attr('content')
			},
			type: 'POST',
			success: function(result){
				window.location="/accounts";
			}
		});
  });
});
</script>

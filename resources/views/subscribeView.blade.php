<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/semantic.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <title>OneUp: Subscribe to continue using</title>

    <style>
    /* START COMPARISON TABLE STYLES */
#comparetable {width: 100%; table-layout: fixed; text-align: center; margin: 4em 0; border-collapse: collapse; }
#comparetable tr {background: transparent!important;}
#comparetable td,
#comparetable th {padding: 20px; text-align: center;}
#comparetable td.rowTitle {text-align: left;}
.blank {background: none!important; border: none!important;}
 .blueshine th {background-color: #b8cee2; font-size: 22px; color: #0c3053; text-align: center; font-weight: 600; text-shadow: 0 1px 0 #e0ecf7; border: 1px solid #9fb6c8;}
 .blueshine td {background-color: #f0f1f1; border: 1px solid #c8d6e2;}
 .blueshine td.rowTitle {border-left: 4px solid #333}
/* END COMPARISON TABLE STYLES */
</style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5T76958');</script>
<!-- End Google Tag Manager -->
</head>
    <script src="js/jquery.min.js"></script>
    <script src="js/semantic.js"></script>
    <script>
        $(document).ready(function(e){
           $(".annual").click(function(e){
                $(".monthly").removeClass("blue");
                $(this).addClass("blue");
                $(".monthly_block").css("display", "none");
                $(".annual_block").css("display", "block");
           });

           $(".monthly").click(function(e){
                $(".annual").removeClass("blue");
                $(this).addClass("blue");
                $(".annual_block").css("display", "none");
                $(".monthly_block").css("display", "block");
           });
        });
    </script>
<body>
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5T76958"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
     <div class="ui grid">
     <div class="three wide column" style="height:80px; background-color:#188EF5;border-bottom: 2px solid #c8c8c8">
         <a href="/">
          <img class="ui circular image" src="img/oneup.jpeg" style="width:55px; height:55px;margin-top:5px;margin-left:15%">
         </a>
     </div>
     <div class="thirteen wide column" style="height:80px; background-color:#188EF5;border-bottom: 2px solid #c8c8c8">
     </div>
   </div>

   @if(!isset($_GET['upgrade']))
   <div class="ui grid centered">
      <div class="seven wide column" style="text-align:center">
        <p style="font-size: 32px"> Oops! Your subscription has expired.</p>
        <p style="color:#5F6A7D; font-size:20px;"> Don't worry, all your data is saved for the next 5 days.<br>Subscribe now to get instant access. </p>
      </div>
   </div>
   @endif
   <br><br>
   <div class="ui grid centered">
      <div class="tweleve wide column" style="text-align: center">
  <div class="ui large buttons">
    <button class="ui button blue annual">Annual</button>
    <div class="or"></div>
    <button class="ui button monthly">Monthly</button>
  </div>
        <br><br>
        <p style="font-size: 15px;">Save BIG with Annual plans</p>
      </div>
   </div>

   <!-- Annual block -->
   <div class="ui grid centered annual_block">
  <div class="third four wide column" style="height:680px; border:1px solid #979ba0; text-align: center; margin-right:2%;">
     <h3 style="color:#979ba0">STARTER</h3>
           <div class="ui grid">
         <div class="sixteen wide column" style="height:60px;background-color: #fafafa;">
       <h1>$4 / mo</h1>
         </div>
     </div>
           <br>
           <a href="https://sites.fastspring.com/oneupapp/product/starterannualplan48year?referrer={{ Auth::user()->email}}"><button class="ui button blue">Subscribe</button></a>
           <br>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="eight wide column" style="height:72px;">
             <p style="font-size:20px">3 social profiles</p>
             <b style="font-size:20px">150 Scheduled Posts</b>
           </div>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="tweleve wide column">
               <p style="font-size:18px;text-align:left"><i class="fa fa-check"></i> Facebook, Instagram, Twitter, Pinterest, LinkedIn, and Google My Business  <br /><br />Maximum of 1 Instagram account</p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i> Set posts to repeat automatically </p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i>Chrome extension for easy image scheduling  </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i> Bulk uploading </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Import posts via CSV file </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Automate posts from RSS feeds (1 feed max)</p>
           </div>
        </div>


  <div class="four wide column" style="height:680px; border:1px solid #979ba0; margin-right:2%; text-align: center">
     <h3 style="color:#979ba0">INDIVIDUAL</h3>
           <div class="ui grid">
         <div class="sixteen wide column" style="height:60px;background-color: #fafafa;">
       <h1>$10 / mo</h1>
         </div>
     </div>
           <br>
           <a href="http://sites.fastspring.com/oneupapp/product/oneupindividualplanannual120year?referrer={{ Auth::user()->email}}"><button class="ui button blue">Subscribe</button></a>
           <br>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="eight wide column">
             <p style="font-size:20px">1 additional team members</p>
             <p style="font-size:20px">10 social profiles</p>
             <b style="font-size:20px">1000 Scheduled Posts</b>
           </div>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="tweleve wide column">
               <p style="font-size:18px;text-align:left"><i class="fa fa-check"></i> Facebook, Instagram, Twitter, Pinterest, LinkedIn, and Google My Business  <br /><br />Maximum of 3 Instagram accounts</p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i> Set posts to repeat automatically </p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i>Chrome extension for easy image scheduling  </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i> Bulk uploading </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Import posts via CSV file </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Automate posts from RSS feeds (3 feeds max)</p>
           </div>
  </div>
  <div class="second four wide column" style="height:680px; border:1px solid #979ba0; margin-right: 2%;text-align: center">
     <h3 style="color:#979ba0">GROWTH</h3>
           <div class="ui grid">
         <div class="sixteen wide column" style="height:60px;background-color: #fafafa;">
       <h1>$20 / mo</h1>
         </div>
     </div>
           <br>
           <a href="https://sites.fastspring.com/oneupapp/product/growthannualplan240year?referrer={{ Auth::user()->email}}"><button class="ui button blue">Subscribe</button></a>
           <br>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="eight wide column">
             <p style="font-size:20px">2 additional team members</p>
             <p style="font-size:20px">30 social profiles</p>
             <b style="font-size:20px">2000 Scheduled Posts</b>
           </div>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="tweleve wide column">
               <p style="font-size:18px;text-align:left"><i class="fa fa-check"></i> Facebook, Instagram, Twitter, Pinterest, LinkedIn, and Google My Business  <br /><br />Maximum of 6 Instagram accounts</p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i> Set posts to repeat automatically </p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i>Chrome extension for easy image scheduling  </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i> Bulk uploading </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Import posts via CSV file </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Automate posts from RSS feeds (5 feeds max)</p>
           </div>
        </div>
   </div>


   <!-- Monthly Block -->

   <div class="ui grid centered monthly_block" style="display:none">
  <div class="third four wide column" style="height:680px; border:1px solid #979ba0; text-align: center; margin-right:2%;">
     <h3 style="color:#979ba0">STARTER</h3>
           <div class="ui grid">
         <div class="sixteen wide column" style="height:60px;background-color: #fafafa;">
       <h1>$8 / mo</h1>
         </div>
     </div>
           <br>
           <a href="https://sites.fastspring.com/oneupapp/product/startermonthly8month?referrer={{ Auth::user()->email}}"><button class="ui button blue">Subscribe</button></a>
           <br>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="eight wide column" style="height:72px;">
             <p style="font-size:20px">3 social profiles</p>
             <b style="font-size:20px">150 Scheduled Posts</b>
           </div>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="tweleve wide column">
               <p style="font-size:18px;text-align:left"><i class="fa fa-check"></i> Facebook, Instagram, Twitter, Pinterest, LinkedIn, and Google My Business <br /><br />Maximum of 1 Instagram account</p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i> Set posts to repeat automatically </p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i>Chrome extension for easy image scheduling  </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i> Bulk uploading </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Import posts via CSV file </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Automate posts from RSS feeds (1 feed max)</p>
           </div>
        </div>

  <div class="four wide column" style="height:680px; border:1px solid #979ba0; margin-right:2%; text-align: center">
     <h3 style="color:#979ba0">INDIVIDUAL</h3>
           <div class="ui grid">
         <div class="sixteen wide column" style="height:60px;background-color: #fafafa;">
       <h1>$15 / mo</h1>
         </div>
     </div>
           <br>
           <a href="https://sites.fastspring.com/oneupapp/product/oneupindividualplan15month?referrer={{ Auth::user()->email}}"><button class="ui button blue">Subscribe</button></a>
           <br>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="eight wide column">
             <p style="font-size:20px">1 additional team members</p>
             <p style="font-size:20px">10 social profiles</p>
             <b style="font-size:20px">1000 Scheduled Posts</b>
           </div>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="tweleve wide column">
               <p style="font-size:18px;text-align:left"><i class="fa fa-check"></i> Facebook, Instagram, Twitter, Pinterest, LinkedIn, and Google My Business  <br /><br />Maximum of 3 Instagram accounts</p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i> Set posts to repeat automatically </p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i>Chrome extension for easy image scheduling  </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i> Bulk uploading </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Import posts via CSV file </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i> Automate posts from RSS feeds (3 feeds max)</p>
           </div>
  </div>
  <div class="second four wide column" style="height:680px; border:1px solid #979ba0; margin-right: 2%;text-align: center">
     <h3 style="color:#979ba0">GROWTH</h3>
           <div class="ui grid">
         <div class="sixteen wide column" style="height:60px;background-color: #fafafa;">
       <h1>$30 / mo</h1>
         </div>
     </div>
           <br>
           <a href="https://sites.fastspring.com/oneupapp/product/growthmonthlyplan30month?referrer={{ Auth::user()->email}}"><button class="ui button blue">Subscribe</button></a>
           <br>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="eight wide column">
             <p style="font-size:20px">3 additional team members</p>
             <p style="font-size:20px">30 social profiles</p>
             <b style="font-size:20px">2000 Scheduled Posts</b>
           </div>
           <div class="ten wide column">
             <div class="ui divider"></div>
           </div>
           <div class="tweleve wide column">
               <p style="font-size:18px;text-align:left"><i class="fa fa-check"></i> Facebook, Instagram, Twitter, Pinterest, LinkedIn, and Google My Business  <br /><br />Maximum of 6 Instagram accounts</p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i> Set posts to repeat automatically </p>
               <p style="font-size:18px; text-align:left"><i class="fa fa-check"></i>Chrome extension for easy image scheduling  </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i> Bulk uploading </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i>Import posts via CSV file </p>
               <p style="font-size:18px; text-align:left;"><i class="fa fa-check"></i> Automate posts from RSS feeds (5 feeds max)</p>
           </div>
        </div>
   </div>

   <div class="annual_block">
     <div class="ui grid centered" style="margin-top:30px">
       <div style="width:80%;height:1px;background-color:#000"></div>
       <div style="width:80%;min-height:70px;display:flex; align-items:center; justify-content:center;margin-top:10px;margin-bottom:10px">
         <p><b>Need more? Try our Business plan - $79/month</b> | 100 social profiles | Unlimited scheduled posts | 10 Instagram accounts | 4 additional users <a href="https://sites.fastspring.com/oneupapp/product/businessannualplan948year?referrer={{ Auth::user()->email}}"
           style="margin-left:10px"><button class="ui button blue">Subscribe</button></a
         ></p>
       </div>
       <div style="width:80%;height:1px;background-color:#000;"></div>
   </div>
 </div>

  <div class="monthly_block" style="display: none">
     <div class="ui grid centered" style="margin-top:30px">
       <div style="width:80%;height:1px;background-color:#000"></div>
       <div style="width:80%;min-height:70px;display:flex; align-items:center; justify-content:center;margin-top:10px;margin-bottom:10px">
         <p><b>Need more? Try our Business plan - $99/month</b> | 100 social profiles | Unlimited scheduled posts | 10 Instagram accounts | 4 additional users <a href="http://sites.fastspring.com/oneupapp/product/businessmonthlyplan99month?referrer={{ Auth::user()->email}}"
           style="margin-left:10px"><button class="ui button blue">Subscribe</button></a
         ></p>
       </div>
       <div style="width:80%;height:1px;background-color:#000;"></div>
     </div>
  </div>

   <div class="ui grid centered" style="margin-top:30px">
       <div class="fourteen wide column" style="text-align:center">
       <h1>Still not convinced?</h1>
       <p style="font-size:18px; color:#5b6675"><b>Check out how OneUp compares to the other social scheduling tools.</b></p>
          <table id="comparetable" class="blueshine">
           <tr>
            <td class="blank"> </td>
                                      <th>OneUp</th>
                                        <th>Buffer</th>
                                        <th>MeetEdgar</th>
                                     </tr>

                                    <tr>
                <td class="rowTitle" style="font-size:16px">Schedule posts to be recycled at specific time intervals</a></td>

                                                  <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                  <td><img src="{{URL::asset('/img/addRedX.png')}}" alt='icon' /></td>
                                                  <td><img src="{{URL::asset('/img/addRedX.png')}}" alt='icon' /></td>
                                                </tr>
                                    <tr>
                <td class="rowTitle" style="font-size:16px">Facebook, Twitter, & LinkedIn </td>

                                                  <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                </tr>
                                       <tr>
             <td class="rowTitle" style="font-size:16px">Instagram</td>

                                              <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                 <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                 <td><img src="{{URL::asset('/img/addRedX.png')}}" alt='icon' /></td>
                                             </tr>
                           <tr>
                <td class="rowTitle" style="font-size:16px">Pinterest</td>

                                                 <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td>Only on Buffer’s $15/month and higher plans</td>
                                                    <td><img src="{{URL::asset('/img/addRedX.png')}}" alt='icon' /></td>
                                                </tr>

                                         <tr>
                  <td class="rowTitle" style="font-size:16px">Google My Business</td>

                                                   <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addRedX.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addRedX.png')}}" alt='icon' /></td>
                                                  </tr>
                                         <tr>
                                       <tr>
                <td class="rowTitle" style="font-size:16px">Ability to create a custom posting schedule</td>

                                                 <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addRedX.png')}}" alt='icon' /></td>
                                                </tr>

                <td class="rowTitle" style="font-size:16px">Chrome browser extension for easy scheduling while browsing the web</td>

                                                 <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                </tr>
                                       <tr>
                <td class="rowTitle" style="font-size:16px">Ability to sort updates into categories</td>

                                                 <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addRedX.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                </tr>
                                       <tr>
                <td class="rowTitle" style="font-size:16px">Emoji support</td>

                                                 <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addcheck.png')}}" alt='icon' /></td>
                                                    <td><img src="{{URL::asset('/img/addRedX.png')}}" alt='icon' /></td>
                                                </tr>




          </table>
    </div>

   </div>
   <div class="ui grid centered">
       <div class="fourteen wide column" style="text-align:center">
     <h1>Frequently Asked Questions</h1>
       </div>
       <div class="eight wide column" style="text-align:left">
         <p style="font-size:22px; color:#323a44;line-height:18px;">Do you support Instagram and Instagram Stories? </p>
         <p style="font-size:18px; color:#5b6675"><b>Yes, OneUp supports Instagram and Instagram Stories with direct publishing. No need for an app or push notifications.</b></p>
         <br><br>
         <p style="font-size:22px; color:#323a44;line-height:18px;">Does OneUp support scheduled posts to Google My Business? </p>
         <p style="font-size:18px; color:#5b6675"><b>Yes. You can also add Call-To-Action buttons in your Google My Business posts when scheduling.</b></p>
         <br><br>
         <p style="font-size:22px; color:#323a44;">Can OneUp schedule posts to Facebook Groups?</p>
         <p style="font-size:18px; color:#5b6675"><b>Yes, but only if you are the admin of that group.</b></p>
         <br /><br />
         <p style="font-size:22px; color:#323a44;line-height:18px;">What does "number of scheduled posts" mean?</p>
         <p style="font-size:18px; color:#5b6675"><b>Depending on your plan, you'll be able to schedule a certain number of posts in advance.
    This is not a weekly or monthly limit, it's the number of posts you can store in your queue at any one time.<br><br> Let’s say you are on the Starter Plan and already have 150 posts in your queue. You will not have the option to add any more posts. However, as soon as one of your posts gets published, you will be able to top your queue back up to 150.</b></p>

         <br><br>
         <p style="font-size:22px; color:#323a44;line-height:18px;">What counts as a social profile? </p>
         <p style="font-size:18px; color:#5b6675">
           <b>Each Facebook Page, Facebook Group, LinkedIn Profile, LinkedIn Page, Twitter account, Pinterest account, Instagram account, and Google My Business location that are connected to OneUp count as 1 social profile.
             <br><br>
             For example, if you connected 3 Facebook Pages and 2 Twitter accounts, and a Google My Business account with 5 locations, that would count as 10 total social profiles. Each Google My Business location counts as a social profile, even if all of them are connected to the same GMB account.</b>
         </p>

         <br><br>
         <p style="font-size:22px; color:#323a44;line-height:18px;"> Do you offer any enterprise plan? </p>
         <p style="font-size:18px; color:#5b6675"><b>Yes, if you need more than 100 accounts, 10 Instagram accounts, or 4 team members, contact us in the chat or at <span style="color:#563c7d">support@oneupapp.io</span> for a custom plan. </b></p>

         <br><br>
         <p style="font-size:22px; color:#323a44;line-height:18px;"> What currency will I be billed in? </p>
         <p style="font-size:18px; color:#5b6675"><b>OneUp bills in USD. For foreign transactions, the currency conversion will happen after the charge and will be performed by your credit card provider.</b></p>

        <br><br>

         <p style="font-size:22px; color:#323a44;line-height:18px;">Do you offer a free trial?</p>
         <p style="font-size:18px; color:#5b6675"><b>All of our paid plans include a 7 day free trial - no credit card necessary.</b></p>

         <br><br>

         <p style="font-size:22px; color:#323a44;line-height:18px;">What's your refund policy?</p>
         <p style="font-size:18px; color:#5b6675"><b>Click <a href="https://oneup.crisp.help/en-us/article/what-is-oneups-cancellation-and-refund-policy-1a5a79f/?1555419261656" target="blank">here</a> to learn about our refund policy.</b></p>

       </div>
   </div>
</body>

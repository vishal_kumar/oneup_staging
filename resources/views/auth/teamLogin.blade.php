<html>
<head>
    <title>Team Login - OneUp</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5T76958');</script>
    <!-- End Google Tag Manager -->
</head>
<body style="background-color:#188EF5;height:100%">
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5T76958"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <div class="container">
        <div class="columns is-centered is-multilined">
            <div class="column is-6" style="text-align:center;background-color:#fff;height:400px;border-radius:5px;box-shadow:0 8px 0 rgba(0, 0, 0, 0.2);margin-top:100px">
                <div class="columns is-centered">
                    <div class="column is-10" style="text-align:left;margin-top:70px">
                       <p style="font-size:20px;text-decoration:underline;margin-top:-20px;margin-bottom:20px;font-weight:bold">Team Member Login </p>
                       <form class="form-horizontal" method="POST" action="/teammemberlogin">
                        {{ csrf_field() }}

                        <div class="field">
                          <label class="label">Login Email</label>
                          <div class="control has-icons-left has-icons-right">
                            <input class="input" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
                            <span class="icon is-small is-left">
                              <i class="fa fa-envelope"></i>
                            </span>
                          </div>
                          @if (isset($error))
                            <p class="help is-danger">{{$error}}</p>
                           @endif
                        </div>
                        <div class="field" style="margin-top:30px">
                          <label class="label">Password</label>
                          <p class="control has-icons-left">
                            <input class="input" id="password" type="password" class="form-control" name="password" required placeholder="Password">
                            <span class="icon is-small is-left">
                              <i class="fa fa-lock"></i>
                            </span>
                          </p>
                          @if ($errors->has('password'))
                            <p class="help is-danger">{{ $errors->first('password') }}</p>
                           @endif
                        </div>
                         <div class="field">
                            <label class="checkbox">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : 'checked' }}>
                                Remember me
                            </label>
                        </div>
                        <br>
                         <button class="button is-primary" type="submit" class="button">
                            Login
                        </button>
                        </form>
                    </div>
                </div>
                <a href="/"><p style="color:#fff;margin-top:40px;cursor:pointer">OneUp Homepage</p></a>
            </div>
        </div>
    </div>

</body>
</html>

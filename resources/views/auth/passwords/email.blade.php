<html>
<head>
    <title>OneUp - Forgot your password</title>
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
</head>
<body style="background-color:#188EF5;height:100%">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-6" style="text-align:center;background-color:#fff;height:400px;border-radius:5px;box-shadow:0 8px 0 rgba(0, 0, 0, 0.2);margin-top:100px">
                <div class="columns is-centered">
                    <div class="column is-10" style="text-align:left;margin-top:70px">
                       <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="field">
                          <label class="label">E-Mail Address</label>
                          <div class="control">
                            <input id="email" type="email" class="input form-control" name="email" value="{{ old('email') }}" required>
                          </div>
                          @if ($errors->has('email'))
                            <p class="help is-danger">{{ $errors->first('email') }}</p>
                           @endif  
                        </div> 
                        <br>
                         <button class="button is-primary" type="submit" class="button">
                            Send Password Reset Link
                        </button>  
                        </form>    
                        <a href="/login"><p style="color:#000;cursor:pointer;opacity:0.8;font-size:16px">Login to your account.</p></a>                                                                
                    </div>
                </div>
                <a href="/password/reset"><p style="color:#fff;margin-top:40px;cursor:pointer">Forgot Password ?</p></a>                
                <a href="/register"><p style="color:#fff;margin-top:10px;cursor:pointer">Not a member? Sign up here.</p></a>
            </div>
        </div>
    </div>    

</body>
</html>
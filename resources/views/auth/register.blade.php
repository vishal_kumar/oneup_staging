<html>
<head>
    <title>OneUp - Create a new account</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5T76958');</script>
    <!-- End Google Tag Manager -->
</head>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.js"></script>
<script>
  $(document).ready(function(e){
     timezone = jstz.determine();
     $("#timezone").val(timezone.name());
  });
</script>
<body style="background-color:#188EF5;height:100%">
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5T76958"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-6" style="text-align:center;background-color:#fff;height:530px;border-radius:5px;box-shadow:0 8px 0 rgba(0, 0, 0, 0.2);margin-top:60px">
                <div class="columns is-centered">
                    <div class="column is-10" style="text-align:left;margin-top:20px">
                       <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="field">
                          <label class="label">Name</label>
                          <div class="control">
                            <input class="input" id="name" class="form-control" name="name" value="{{ old('name') }}" required autofocus required autofocus placeholder="Name">
                          </div>
                          @if ($errors->has('name'))
                            <p class="help is-danger">{{ $errors->first('name') }}</p>
                           @endif
                        </div>

                        <div class="field" style="margin-top:30px">
                          <label class="label">E-Mail Address</label>
                          <div class="control">
                            <input id="email" type="email" class="input form-control" name="email" value="{{ old('email') }}" required placeholder="Email address">
                          </div>
                          @if ($errors->has('email'))
                            <p class="help is-danger">{{ $errors->first('email') }}</p>
                           @endif
                        </div>


                        <div class="field" style="margin-top:30px">
                          <label class="label">Password</label>
                          <p class="control">
                            <input class="input" id="password" type="password" class="form-control" name="password" required placeholder="Password">
                          </p>
                          @if ($errors->has('password'))
                            <p class="help is-danger">{{ $errors->first('password') }}</p>
                           @endif
                        </div>

                        <div class="field" style="margin-top:30px">
                          <label class="label">Confirm Password</label>
                          <p class="control">
                            <input id="password-confirm" type="password" class="input form-control" name="password_confirmation" required>
                          </p>
                        </div>
                         <input type="hidden" name="timezone" id="timezone" value="">
                        <br>
                         <button class="button is-primary" type="submit" class="button">
                            Register
                        </button>
                        </form>
                    </div>
                </div>
                <a href="/login"><p style="color:#000;cursor:pointer;opacity:0.8">Already registered? Login here.</p></a>
            </div>
        </div>
    </div>

</body>
</html>

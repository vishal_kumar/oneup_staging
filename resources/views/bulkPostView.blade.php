<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78962458-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-78962458-1');
	</script>
	<!-- Hotjar Tracking Code for https://www.oneupapp.io -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:959054,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

	<title>Bulk Upload post - OneUp</title>
    <link rel="shortcut icon" type="image/png" href="https://res.cloudinary.com/dgkqns6fw/image/upload/c_scale,h_16,w_16/v1518969814/bb357f3f82584890a0474474ca4cfe79_gl3sxy.png"/>
	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5T76958');</script>
	<!-- End Google Tag Manager -->
</head>
<style>
.navbar { position: fixed !important; top: 0; left: 0; right: 0; z-index: 1030;}
.active{background: #188EF5}
#mypanel{
    width:60px;
    height:60px;
    border:1px solid #EAEAEA;
    margin-left:15px;
    background-position:center;
    background-size: 100%;
    cursor: pointer;
    margin-bottom: 20px;
}
#smallpanel{
    width:50px;
    height:50px;
    border:1px solid #EAEAEA;
    margin-left:8px;
    background-position:center;
    background-size: 100%;
    cursor: pointer;
    margin-bottom: 20px;
    float: left
}
#sn-icon-small{
    display:block;
    float:right;
    width:26px;
    height:26px;
    margin-top: -10px;
    margin-right: -10px;
    background-repeat: no-repeat;
    background-position: center;
    background-image:url("{{ asset('img/twitter.png') }}");
}
#sn-icon{
    display:block;
    float:right;
    width:24px;
    height:24px;
    margin-top: -10px;
    margin-right: -10px;
    background-repeat: no-repeat;
    background-position: center;
    background-image:url("{{ asset('img/twitter.png') }}");
}
.evergreenBlock{
   width:40px;
   height:40px;
   background-image:url("{{ asset('img/evergreen.png') }}");
   background-position:center;
   background-size: 100%;
}
</style>
<body>
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5T76958"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	@include('layouts.partials.nav');
	<div id="app" class="container" style="margin-top:80px">
		<multiplepost></multiplepost>
	</div>
</body>
<script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>
<script src="{{URL::asset('js/app.js?v=4.1.0')}}"></script>

</html>

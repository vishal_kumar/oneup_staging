
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('category', require('./components/Category.vue'));
Vue.component('singlepost', require('./components/SinglePost.vue'));
Vue.component('rsspost', require('./components/RssPost.vue'));
Vue.component('multiplepost', require('./components/MultiplePost.vue'));
Vue.component('discovercontent', require('./components/DiscoverContent.vue'));
Vue.component('schedule', require('./components/Schedule.vue'));
Vue.component('queue', require('./components/Queue.vue'));
Vue.component('calendarview', require('./components/CalendarView.vue'));
Vue.component('pausequeue', require('./components/PauseQueue.vue'));
Vue.component('analytics', require('./components/Analytics.vue'));
Vue.component('timezone', require('./components/Timezone.vue'));
Vue.component('googleanalytics', require('./components/GoogleAnalytics.vue'));
Vue.component('socialutm', require('./components/SocialUtm.vue'));
Vue.component('instagramstory', require('./components/InstaStory.vue'));


const app = new Vue({
    el: '#app'
});

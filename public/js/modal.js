function ONPHstart(t) {
    var o= {
        init:function(t) {
            this.check(t);
        }
        ,
        check:function(t) {
            var e=document.referrer;
            this.build(t, "on");
            //"producthunt"!=o.getUrlParameter("ref")&&!e.match(/^https?: \/\/([^\/]+\.)?producthunt\.com(\/|$)/i)||1!=t.display_from||null!=o.getCookie("onph-x")?"producthunt"==o.getUrlParameter("ref")&&e.match(/^https?: \/\/([^\/]+\.)?producthunt\.com(\/|$)/i)||null!=o.getCookie("onph-x")||this.build(t, "on"): this.build(t, "from")
        }
        ,
        build:function(t, o) {
            var e,
            i,
            n,
            a=document.createElement("div"),
            r=document.getElementsByTagName("body")[0];
            a.setAttribute("id", "onph-wrap"),
            "bottom-left"==t.position?(a.classList.add("onph-bottom"), a.classList.add("onph-left")): "bottom-right"==t.position?(a.classList.add("onph-bottom"), a.classList.add("onph-right")): "top-left"==t.position?(a.classList.add("onph-top"), a.classList.add("onph-left")): "top-right"==t.position&&(a.classList.add("onph-top"), a.classList.add("onph-right")), "on"==o?(e='<div class="onph-title">'+t.on_title+"</div>", i='<div class="onph-description">'+t.on_description+"</div>", n='<div class="onph-button"><a href="'+t.link+'" target="_blank">'+t.button_text+"</a></div>"): "from"==o&&(e='<div class="onph-title">'+t.from_title+"</div>", i='<div class="onph-description">'+t.from_description+"</div>", n=""!=t.from_discount?'<div class="onph-discount"><span>'+t.from_discount+"</span></div>": ""), a.innerHTML='<div class="onph-container"><div class="onph-logo"></div><div class="onph-content">'+e+i+"</div>"+n+'<div class="onph-close"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg></div>', r.appendChild(a), this.style(t)
        }
        ,
        style:function(t) {
            function e(t, o) {
                return(" "+t.className+" ").indexOf(" "+o+" ")>-1
            }
            var z= document.getElementsByClassName("onph-button");
            if(z[0].addEventListener("click", function() {
              o.setCookie("onph-x", "y"), e(n, "onph-right")?n.style.marginRight="-400px": e(n, "onph-left")&&(n.style.marginLeft="-400px")
            }));
            var i=document.getElementsByClassName("onph-close"), n=document.getElementById("onph-wrap");
            if(i[0].addEventListener("click", function() {
                var win = window.open(t.link, '_blank');
                win.focus();
                o.setCookie("onph-x", "y"), e(n, "onph-right")?n.style.marginRight="-400px": e(n, "onph-left")&&(n.style.marginLeft="-400px")
            }
            ), ""==t.from_discount&&"kitty"==t.design) {
                var a=document.getElementsByClassName("onph-powered");
                a[0].style.right="2px",
                a[0].style.left="auto",
                a[0].style.bottom="-20px"
            }
            var r="@import url(https://fonts.googleapis.com/css?family=Work+Sans:400,600);.onph-button a,.onph-button a:active,.onph-button a:hover,.onph-button a:visited,.onph-powered a,.onph-powered a:active,.onph-powered a:hover,.onph-powered a:visited{-webkit-animation:none;animation:none;-webkit-animation-delay:0;animation-delay:0;-webkit-animation-direction:normal;animation-direction:normal;-webkit-animation-duration:0;animation-duration:0;-webkit-animation-fill-mode:none;animation-fill-mode:none;-webkit-animation-iteration-count:1;animation-iteration-count:1;-webkit-animation-name:none;animation-name:none;-webkit-animation-play-state:running;animation-play-state:running;-webkit-animation-timing-function:ease;animation-timing-function:ease;-webkit-backface-visibility:visible;backface-visibility:visible;background:0;background-clip:border-box;background-origin:padding-box;background-position-x:0;background-position-y:0;background-size:auto auto;border:0;border-width:medium;border-color:inherit;border-bottom:0;border-bottom-color:inherit;border-collapse:separate;-webkit-border-image:none;-o-border-image:none;border-image:none;border-left:0;border-left-color:inherit;border-right:0;border-right-color:inherit;border-spacing:0;border-top:0;border-top-color:inherit;border-radius:0;bottom:auto;-webkit-box-shadow:none;box-shadow:none;-webkit-box-sizing:content-box;box-sizing:content-box;caption-side:top;clear:none;clip:auto;-webkit-columns:auto;columns:auto;-webkit-column-count:auto;column-count:auto;-webkit-column-fill:balance;column-fill:balance;-webkit-column-gap:normal;column-gap:normal;-webkit-column-rule:medium none currentColor;column-rule:medium none currentColor;-webkit-column-rule-color:currentColor;column-rule-color:currentColor;-webkit-column-rule-style:none;column-rule-style:none;-webkit-column-rule-width:none;column-rule-width:none;-webkit-column-span:1;column-span:1;-webkit-column-width:auto;column-width:auto;content:normal;counter-increment:none;counter-reset:none;cursor:auto;color:initial;direction:ltr;display:inline;empty-cells:show;float:none;font:400;font-family:inherit;font-size:medium;font-style:normal;font-variant:normal;font-weight:400;height:auto;-webkit-hyphens:none;-ms-hyphens:none;hyphens:none;left:auto;letter-spacing:normal;line-height:normal;list-style:disc;margin:0;max-height:none;max-width:none;min-height:0;min-width:0;opacity:1;orphans:0;outline:0;outline-width:medium;overflow:visible;overflow-x:visible;overflow-y:visible;padding:0;page-break-after:auto;page-break-before:auto;page-break-inside:auto;-webkit-perspective:none;perspective:none;-webkit-perspective-origin:50% 50%;perspective-origin:50% 50%;position:static;right:auto;-moz-tab-size:8;-o-tab-size:8;tab-size:8;table-layout:auto;text-align:inherit;text-align-last:auto;text-decoration:none;-webkit-text-decoration-color:inherit;text-decoration-color:inherit;-webkit-text-decoration-line:none;text-decoration-line:none;-webkit-text-decoration-style:solid;text-decoration-style:solid;text-indent:0;text-shadow:none;text-transform:none;top:auto;-webkit-transform:none;-ms-transform:none;transform:none;-webkit-transform-style:flat;transform-style:flat;-webkit-transition:none;-o-transition:none;transition:none;-webkit-transition-delay:0s;-o-transition-delay:0s;transition-delay:0s;-webkit-transition-duration:0s;-o-transition-duration:0s;transition-duration:0s;-webkit-transition-property:none;-o-transition-property:none;transition-property:none;-webkit-transition-timing-function:ease;-o-transition-timing-function:ease;transition-timing-function:ease;unicode-bidi:normal;vertical-align:baseline;visibility:visible;white-space:normal;widows:0;width:auto;word-spacing:normal;z-index:auto}@-o-keyframes slideRight{100%{right:20px}}@-moz-keyframes slideRight{100%{right:20px}}@-webkit-keyframes slideRight{100%{right:20px}}@keyframes slideRight{100%{right:20px}}@-o-keyframes slideLeft{100%{left:40px}}@-moz-keyframes slideLeft{100%{left:40px}}@-webkit-keyframes slideLeft{100%{left:40px}}@keyframes slideLeft{100%{left:40px}}@-o-keyframes slideLeft2{100%{left:20px}}@-moz-keyframes slideLeft2{100%{left:20px}}@-webkit-keyframes slideLeft2{100%{left:20px}}@keyframes slideLeft2{100%{left:20px}}#onph-wrap,#onph-wrap *{-webkit-box-sizing:border-box!important;-moz-box-sizing:border-box!important;box-sizing:border-box!important}",
            s=document.head||document.getElementsByTagName("head")[0],
            p=document.createElement("style");
            if("kitty"==t.design)var l=r+"#onph-wrap{max-width:335px;position:fixed;z-index:99999;font-family:'Work Sans',sans-serif;background:#fff;border:1px solid #E0E0E0;border-radius:3px;padding:10px;-webkit-box-shadow:2px 2px 70px rgba(0,0,0,.1);box-shadow:2px 2px 70px rgba(0,0,0,.1)}#onph-wrap.onph-top{top:20%}#onph-wrap.onph-bottom{bottom:20px}#onph-wrap.onph-right{right:-370px;-moz-animation:slideRight .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;-webkit-animation:slideRight .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;animation:slideRight .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;-o-transition:.5s margin-right ease-out;-moz-transition:.5s margin-right ease-out;-webkit-transition:.5s margin-right ease-out;transition:.5s margin-right ease-out}#onph-wrap.onph-left{left:-370px;right:auto;-moz-animation:slideLeft .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;-webkit-animation:slideLeft .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;animation:slideLeft .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;-o-transition:.5s margin-left ease-out;-moz-transition:.5s margin-left ease-out;-webkit-transition:.5s margin-left ease-out;transition:.5s margin-left ease-out}.onph-close{width:8px;height:8px;position:absolute;top:3px;right:3px;cursor:pointer}.onph-close svg{display:block;fill:rgba(0,0,0,.15)}.onph-logo{position:absolute;top:-10px;left:-30px;width:100px;height:100px;background-image:url(http://res.cloudinary.com/dgkqns6fw/image/upload/v1538079499/nhz1l6xrzxqkllnz2dnl.png);background-size:100px auto;background-repeat:no-repeat}.onph-content{width:235px;margin-left:78px;color:#555}.onph-title{font-weight:600;font-size:16px;line-height:1.2;margin-bottom:3px}.onph-description{font-size:12px;line-height:1.4}.onph-button,.onph-discount{margin-top:10px;text-align:right}.onph-button a,.onph-button a:active,.onph-button a:hover,.onph-button a:visited,.onph-discount span{padding:15px;border-radius:3px;font-size:14px;font-weight:600;letter-spacing:1px;display:inline-block;width:235px;text-align:center}.onph-button a,.onph-button a:active,.onph-button a:hover,.onph-button a:visited{background:#da552f;color:#fff;cursor:pointer}.onph-button a:active,.onph-button a:hover,.onph-button a:visited{background:#d04a25;color:#fff!important}.onph-button svg{fill:#fff;margin-right:10px}.onph-discount span{color:#da552f;background-color:#f8f8f8;background-image:url(\"data:image/svg+xml,%3Csvg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23da552f' fill-opacity='0.15' fill-rule='evenodd'%3E%3Cpath d='M5 0h1L0 6V5zM6 5v1H5z'/%3E%3C/g%3E%3C/svg%3E\")}.onph-powered{position:absolute;left:7px;bottom:5px}.onph-powered a,.onph-powered a:active,.onph-powered a:hover,.onph-powered a:visited{font-size:10px;cursor:pointer;color:#BDBDBD!important}.onph-powered svg{vertical-align:middle;fill:#BDBDBD}@media (max-width:390px){#onph-wrap{max-width:272px}.onph-logo{width:80px;height:80px;background-size:80px auto}.onph-content{width:200px;margin-left:50px}.onph-title{font-size:15px}.onph-description{font-size:11px}.onph-button a,.onph-button a:active,.onph-button a:hover,.onph-button a:visited,.onph-discount span{width:200px;padding:10px}.onph-powered{position:absolute;left:1px;bottom:5px}@-o-keyframes slideRight{100%{right:10px}}@-moz-keyframes slideRight{100%{right:10px}}@-webkit-keyframes slideRight{100%{right:10px}}@keyframes slideRight{100%{right:10px}}@-o-keyframes slideLeft{100%{left:35px}}@-moz-keyframes slideLeft{100%{left:35px}}@-webkit-keyframes slideLeft{100%{left:35px}}@keyframes slideLeft{100%{left:35px}}}";
            else if("classic"==t.design)l=r+"#onph-wrap{max-width:280px;position:fixed;z-index:99999;font-family:'Work Sans',sans-serif;background:#F8F8F8;border:1px solid #E0E0E0;border-radius:3px;padding:10px;-webkit-box-shadow:2px 2px 70px rgba(0,0,0,.1);box-shadow:2px 2px 70px rgba(0,0,0,.1)}#onph-wrap.onph-top{top:20%}#onph-wrap.onph-bottom{bottom:20px}#onph-wrap.onph-right{right:-300px;-moz-animation:slideRight .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;-webkit-animation:slideRight .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;animation:slideRight .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;-o-transition:.5s margin-right ease-out;-moz-transition:.5s margin-right ease-out;-webkit-transition:.5s margin-right ease-out;transition:.5s margin-right ease-out}#onph-wrap.onph-left{left:-300px;right:auto;-moz-animation:slideLeft2 .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;-webkit-animation:slideLeft2 .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;animation:slideLeft2 .8s forwards cubic-bezier(.68,-.55,.265,1.55) 50ms;-o-transition:.5s margin-left ease-out;-moz-transition:.5s margin-left ease-out;-webkit-transition:.5s margin-left ease-out;transition:.5s margin-left ease-out}.onph-close{width:8px;height:8px;position:absolute;top:4px;right:4px;cursor:pointer}.onph-close svg{display:block;fill:rgba(0,0,0,.15)}.onph-logo{width:60px;height:60px;margin:-30px auto 10px;background-image:url(https://hypeok.com/onph/ph.png);background-size:60px 60px}.onph-content{text-align:center;color:#555}.onph-title{font-weight:600;font-size:16px;line-height:1.2;margin-bottom:3px}.onph-description{font-size:12px;line-height:1.4}.onph-button a,.onph-button a:active,.onph-button a:hover,.onph-button a:visited,.onph-discount span{padding:15px 25px;border-radius:3px;font-size:14px;font-weight:600;letter-spacing:1px;display:inline-block;text-align:center}.onph-button,.onph-discount{margin-top:15px;text-align:center}.onph-button a,.onph-button a:active,.onph-button a:hover,.onph-button a:visited{background:#da552f;color:#fff;cursor:pointer}.onph-button a:active,.onph-button a:hover,.onph-button a:visited{background:#d04a25;color:#fff!important}.onph-button svg{fill:#fff;margin-right:10px}.onph-discount span{color:#da552f;background-color:#f0f0f0;background-image:url(\"data:image/svg+xml,%3Csvg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23da552f' fill-opacity='0.15' fill-rule='evenodd'%3E%3Cpath d='M5 0h1L0 6V5zM6 5v1H5z'/%3E%3C/g%3E%3C/svg%3E\")}.onph-powered{text-align:center;margin-bottom:-5px;margin-top:10px}.onph-powered a,.onph-powered a:active,.onph-powered a:hover,.onph-powered a:visited{font-size:10px;cursor:pointer;color:#BDBDBD!important}.onph-powered svg{vertical-align:middle;fill:#BDBDBD}";
            p.type="text/css",
            p.styleSheet?p.styleSheet.cssText=l:p.appendChild(document.createTextNode(l)),
            s.appendChild(p)
        }
        ,
        setCookie:function(t, o, e) {
            if(e) {
                var i=new Date;
                i.setTime(i.getTime()+24*e*60*60*1e3);
                var n="; expires="+i.toUTCString()
            }
            else n="";
            document.cookie=t+"="+o+n+"; path=/"
        }
        ,
        getCookie:function(t) {
            for(var o=t+"=", e=document.cookie.split(";"), i=0;
            i<e.length;
            i++) {
                for(var n=e[i];
                " "==n.charAt(0);
                )n=n.substring(1, n.length);
                if(0==n.indexOf(o))return n.substring(o.length, n.length)
            }
            return null
        }
        ,
        getUrlParameter:function(t) {
            t=t.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var o=new RegExp("[\\?&]"+t+"=([^&#]*)").exec(location.search);
            return null===o?"": decodeURIComponent(o[1].replace(/\+/g, " "))
        }
    }
    ;
    o.init(t)
}

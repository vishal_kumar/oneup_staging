var request = require('request'); 
var http = require('http');
var mongodb = require('mongodb');
var Db = mongodb.Db;
var Server = mongodb.Server;
var BSON = mongodb.BSONPure;
var crypto = require('crypto');

var db = new Db('pingraphy_prod', new Server("ds057647-a1.mongolab.com",57647,{auto_reconnect : true}),{safe: true});

db.open(function(err, client) {
  if(err) console.log("Could not open database" + err);
  client.authenticate('pingraphy','pingraphy',function(err, success) {
      if(err) console.log("Could not authenitcate to database" + err);
      init();
  });
});

var init = function() {
    getFailedPinsAndUpload('brainpowerboy@gmail.com',function(){});
};


var getFailedPinsAndUpload = function(email, callback) {
  db.collection('scheduledpins', function(error, scheduledpins) {
        var timestamp = Math.round(new Date().getTime() / 1000) ;
        scheduledpins.find({'email':email,'isUploaded': 3}).each(function(err, doc) {
            if(err || doc == null) {
              //
            } else {
              var o_id = new BSON.ObjectID(''+doc['_id']);

                scheduledpins.update({"_id": o_id}, {$set:{'timestamp':timestamp,'isUploaded':0}}, 
                    function(err, res) {
                      console.log("Updated");
                    }
                );
                timestamp += 2*60*60;
            } 
        });
  });
};